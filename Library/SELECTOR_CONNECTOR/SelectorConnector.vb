Imports System.ComponentModel
Imports isr.Core.ControlExtensions

''' <summary>
''' A user interface for selecting and connecting to an item from a list of names.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
''' created.
''' </history>
<Description("Named Resource Connector Control"), _
    DefaultEvent("Selected"), _
    System.Drawing.ToolboxBitmap(GetType(SelectorConnector))> _
Public Class SelectorConnector

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  Public Sub New()
    MyBase.New()

    ' Initialize user components that might be affected by resize or paint actions
    'onInitialize()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call
    'onInstantiate()
    Me._connectToggleButton.Enabled = False
    Me._clearButton.Enabled = False
    Me._findButton.Enabled = True

  End Sub

#End Region

#Region " TYPES "

  ''' <summary>Enumerates the image indices.
  ''' </summary>
  Private Enum ImageIndex
    None
    Clear
    Connect
    Connected
    Search
  End Enum

#End Region

#Region " BROWSABLE PROPERTIES "

  Private _clearable As Boolean
  ''' <summary>
  ''' Gets or sets the value indicating if the clear button is visible and can be enabled.
  ''' An item can be cleared only if it is connected.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  <Category("Appearance"), Description("Shows or hides the Clear button"), _
    Browsable(True), _
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
    DefaultValue(True)> _
  Public Property Clearable() As Boolean
    Get
      Return _clearable
    End Get
    Set(ByVal value As Boolean)
      _clearable = value
      Me._clearButton.Visible = value
    End Set
  End Property

  Private _connectible As Boolean
  ''' <summary>
  ''' Gets or sets the value indicating if the connect button is visible and can be enabled.
  ''' An item can be connected only if it is selected.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  <Category("Appearance"), Description("Shows or hides the Connect button"), _
    Browsable(True), _
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
    RefreshProperties(RefreshProperties.All), _
    DefaultValue(True)> _
  Public Property Connectible() As Boolean
    Get
      Return _connectible
    End Get
    Set(ByVal value As Boolean)
      _connectible = value
      _connectToggleButton.Visible = value
      If Not value Then
        ' cannot clear a device if we do not connect to it.
        Me.Clearable = False
      End If
    End Set
  End Property

  Private _searchable As Boolean
  ''' <summary>
  ''' Gets or sets the condition determining if the control can be searchable.
  ''' The elements can be searched only if not connected.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  <Category("Appearance"), Description("Shows or hides the Search (Find) button"), _
    Browsable(True), _
    DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
    DefaultValue(True)> _
  Public Property Searchable() As Boolean
    Get
      Return _searchable
    End Get
    Set(ByVal value As Boolean)
      _searchable = value
      Me._findButton.Visible = value
    End Set
  End Property

#End Region

#Region " METHODS and PROPERTIES "

  ''' <summary>Gets or sets the connected status and enables the clear button.
  ''' </summary>
  <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
  Public Property IsConnected() As Boolean
    Get
      Return Me._connectToggleButton.Checked
    End Get
    Set(ByVal Value As Boolean)
      If Me._connectToggleButton.Checked <> Value Then
        Me._connectToggleButton.SilentCheckedSetter(Value)
      End If

      ' enable or disable based on the connection status.
      Me._namesComboBox.Enabled = Not Value
      Me._clearButton.Enabled = Value
      Me._findButton.Enabled = Not Value

    End Set
  End Property

  Private _names() As String
  ''' <summary>
  ''' Displays the names stored in names.
  ''' </summary>
  Private Sub DisplayNames()

    ' clear the interface names
    _namesComboBox.DataSource = Nothing
    _namesComboBox.Items.Clear()

    If _names IsNot Nothing Then

      ' set the list of available names
      _namesComboBox.DataSource = _names

      ' enable the connect toggle
      Me._connectToggleButton.Enabled = Me.Connectible AndAlso Not String.IsNullOrEmpty(_namesComboBox.Text.Trim)

      If _names.Length = 0 Then

        Dim tip As String = "No resources to select from"
        _toolTip.SetToolTip(Me._namesComboBox, tip)

      Else

        Dim tip As String = "Select a name from the list"
        _toolTip.SetToolTip(Me._namesComboBox, tip)

      End If

    End If

  End Sub

  ''' <summary>
  ''' Displays resource names in the list box.
  ''' </summary>
  ''' <param name="names">Specifies the names to display.</param>
  ''' <remarks></remarks>
  Public Sub DisplayNames(ByVal names() As String)

    If names Is Nothing Then
      Return
    End If
    ' save the names
    _names = names

    ' display the names
    Me.DisplayNames()

  End Sub

  ''' <summary>Returns the selected name.</summary>
  <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
  Public Property SelectedName() As String
    Get
      Return _namesComboBox.Text
    End Get
    Set(ByVal Value As String)
      _namesComboBox.Text = Value
      Me._connectToggleButton.Enabled = Me.Connectible AndAlso Not String.IsNullOrEmpty(_namesComboBox.Text.Trim)
    End Set
  End Property

#End Region

#Region " CONTROL EVENT HANDLERS "

  ''' <summary>
  ''' Updates the connectibility of the control.
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub _namesComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _namesComboBox.TextChanged

    Me._connectToggleButton.Enabled = Me.Connectible AndAlso Not String.IsNullOrEmpty(_namesComboBox.Text.Trim)

  End Sub

  ''' <summary>Selects an interface.</summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="ok")> _
  Private Sub _namesComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _namesComboBox.SelectedIndexChanged

    If _namesComboBox.SelectedItem IsNot Nothing Then

      OnSelected(New EventArgs)

    End If
    Me._connectToggleButton.Enabled = Me.Connectible AndAlso Not String.IsNullOrEmpty(_namesComboBox.Text.Trim)

  End Sub

  ''' <summary>
  ''' Invokes <see cref="OnClear">clear</see> to clear resources or whatever else 
  ''' needs clearing.
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="ok")> _
  Private Sub clearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _clearButton.Click
    OnClear(New EventArgs)
  End Sub

  ''' <summary>
  ''' Invokes <see cref="OnConnect">connecting</see> or <see cref="OnDisconnect">disconnecting</see>
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="ok")> _
  Private Sub _connectToggleButton_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _connectToggleButton.CheckStateChanged

    If _connectToggleButton.Enabled Then

      If _connectToggleButton.Checked Then
        Me.OnConnect(New EventArgs)
      Else
        Me.OnDisconnect(New EventArgs)
      End If

    End If

    If _connectToggleButton.Checked Then
      Me._toolTip.SetToolTip(_connectToggleButton, "Release to disconnect")
      _connectToggleButton.ImageIndex = SelectorConnector.ImageIndex.Connected - 1
    Else
      Me._toolTip.SetToolTip(_connectToggleButton, "Press to connect")
      _connectToggleButton.ImageIndex = SelectorConnector.ImageIndex.Connect - 1
    End If

  End Sub

  ''' <summary>Get the container to update the resource names.</summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification:="ok")> _
  Private Sub _findButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _findButton.Click

    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
    Try
      OnFindNames(New EventArgs)
    Catch
      Throw
    Finally
      Me.Cursor = System.Windows.Forms.Cursors.Default
    End Try

  End Sub

#End Region

#Region " EVENT HANDLERS "

  ''' <summary>Occurs when the clear button is clicked.</summary>
  Public Event Clear As EventHandler(Of EventArgs)

  ''' <summary>Raises the clear.</summary>
  ''' <param name="e"></param>
  Protected Sub OnClear(ByVal e As EventArgs)

    Dim previousCursor As Windows.Forms.Cursor = Me.Cursor
    Try

      ' Turn off the form hourglass cursor
      Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

      If ClearEvent IsNot Nothing Then ClearEvent(Me, e)

    Catch

      Throw

    Finally

      ' restore the previous cursor
      Me.Cursor = previousCursor

    End Try

  End Sub

  ''' <summary>Occurs when the connect button is depressed.</summary>
  Public Event Connect As EventHandler(Of EventArgs)

  ''' <summary>Raises the Connect event.</summary>
  ''' <param name="e"></param>
  Protected Sub OnConnect(ByVal e As EventArgs)

    Dim previousCursor As Windows.Forms.Cursor = Me.Cursor
    Try

      ' Turn off the form hourglass cursor
      Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

      If ConnectEvent IsNot Nothing Then ConnectEvent(Me, e)

    Catch

      Throw

    Finally

      ' restore the previous cursor
      Me.Cursor = previousCursor

    End Try

  End Sub

  ''' <summary>Occurs when the connect button is release.</summary>
  Public Event Disconnect As EventHandler(Of EventArgs)

  ''' <summary>Raises the Disconnect event.</summary>
  ''' <param name="e"></param>
  Protected Sub OnDisconnect(ByVal e As EventArgs)

    If DisconnectEvent IsNot Nothing Then DisconnectEvent(Me, e)

  End Sub

  ''' <summary>Occurs when the find button is clicked.</summary>
  Public Event FindNames As EventHandler(Of EventArgs)

  ''' <summary>Raises the FindNames event.</summary>
  ''' <param name="e"></param>
  Protected Sub OnFindNames(ByVal e As EventArgs)

    Dim previousCursor As Windows.Forms.Cursor = Me.Cursor
    Try

      ' Turn off the form hourglass cursor
      Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

      If FindNamesEvent IsNot Nothing Then FindNamesEvent(Me, e)

    Catch

      Throw

    Finally

      ' restore the previous cursor
      Me.Cursor = previousCursor

    End Try

  End Sub

  ''' <summary>Occurs when a resource is selected.</summary>
  Public Event Selected As EventHandler(Of EventArgs)

  ''' <summary>Raises the Selected event.</summary>
  ''' <param name="e"></param>
  Protected Sub OnSelected(ByVal e As EventArgs)

    If SelectedEvent IsNot Nothing Then SelectedEvent(Me, e)

  End Sub

#End Region

End Class

