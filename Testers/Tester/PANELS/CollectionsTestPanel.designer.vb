<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CollectionsTestPanel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.itemSelectedLabel = New System.Windows.Forms.Label
        Me.coloCheckedListBox = New System.Windows.Forms.CheckedListBox
        Me.colorListBox = New System.Windows.Forms.ListBox
        Me.colorCombo = New System.Windows.Forms.ComboBox
        Me.SuspendLayout()
        '
        'itemSelectedLabel
        '
        Me.itemSelectedLabel.Location = New System.Drawing.Point(5, 10)
        Me.itemSelectedLabel.Name = "itemSelectedLabel"
        Me.itemSelectedLabel.Size = New System.Drawing.Size(120, 16)
        Me.itemSelectedLabel.TabIndex = 10
        Me.itemSelectedLabel.Text = "Items Selected"
        '
        'coloCheckedListBox
        '
        Me.coloCheckedListBox.Location = New System.Drawing.Point(268, 10)
        Me.coloCheckedListBox.Name = "coloCheckedListBox"
        Me.coloCheckedListBox.Size = New System.Drawing.Size(120, 94)
        Me.coloCheckedListBox.TabIndex = 9
        '
        'colorListBox
        '
        Me.colorListBox.Location = New System.Drawing.Point(137, 10)
        Me.colorListBox.Name = "colorListBox"
        Me.colorListBox.Size = New System.Drawing.Size(120, 95)
        Me.colorListBox.TabIndex = 8
        '
        'colorCombo
        '
        Me.colorCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.colorCombo.Location = New System.Drawing.Point(5, 50)
        Me.colorCombo.Name = "colorCombo"
        Me.colorCombo.Size = New System.Drawing.Size(121, 21)
        Me.colorCombo.TabIndex = 7
        '
        'CollectionsTestPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(400, 114)
        Me.Controls.Add(Me.itemSelectedLabel)
        Me.Controls.Add(Me.coloCheckedListBox)
        Me.Controls.Add(Me.colorListBox)
        Me.Controls.Add(Me.colorCombo)
        Me.Name = "CollectionsTestPanel"
        Me.Text = "TestPanel1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents itemSelectedLabel As System.Windows.Forms.Label
    Friend WithEvents coloCheckedListBox As System.Windows.Forms.CheckedListBox
    Friend WithEvents colorListBox As System.Windows.Forms.ListBox
    Friend WithEvents colorCombo As System.Windows.Forms.ComboBox
End Class
