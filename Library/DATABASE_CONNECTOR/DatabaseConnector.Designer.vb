﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DatabaseConnector
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me._toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._connectionStringTextBox = New System.Windows.Forms.TextBox
        Me._applyButton = New System.Windows.Forms.Button
        Me._connectionStringPanel = New System.Windows.Forms.Panel
        Me._connectionStringLayout = New System.Windows.Forms.TableLayoutPanel
        Me._connectionStatusLed = New isr.Controls.FlashLed
        Me._connectionStringPanel.SuspendLayout()
        Me._connectionStringLayout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_connectionStringTextBox
        '
        Me._connectionStringTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._connectionStringTextBox.Location = New System.Drawing.Point(0, 3)
        Me._connectionStringTextBox.Name = "_connectionStringTextBox"
        Me._connectionStringTextBox.Size = New System.Drawing.Size(692, 20)
        Me._connectionStringTextBox.TabIndex = 5
        Me._toolTip.SetToolTip(Me._connectionStringTextBox, "Connection string")
        '
        '_applyButton
        '
        Me._applyButton.Location = New System.Drawing.Point(732, 9)
        Me._applyButton.Name = "_applyButton"
        Me._applyButton.Size = New System.Drawing.Size(71, 25)
        Me._applyButton.TabIndex = 0
        Me._applyButton.Text = "&Apply"
        Me._toolTip.SetToolTip(Me._applyButton, "Test the connection to the database")
        Me._applyButton.UseVisualStyleBackColor = True
        '
        '_connectionStringPanel
        '
        Me._connectionStringPanel.Controls.Add(Me._connectionStringTextBox)
        Me._connectionStringPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._connectionStringPanel.Location = New System.Drawing.Point(3, 9)
        Me._connectionStringPanel.Name = "_connectionStringPanel"
        Me._connectionStringPanel.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me._connectionStringPanel.Size = New System.Drawing.Size(692, 25)
        Me._connectionStringPanel.TabIndex = 6
        '
        '_connectionStringLayout
        '
        Me._connectionStringLayout.ColumnCount = 3
        Me._connectionStringLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._connectionStringLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._connectionStringLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._connectionStringLayout.Controls.Add(Me._applyButton, 2, 1)
        Me._connectionStringLayout.Controls.Add(Me._connectionStringPanel, 0, 1)
        Me._connectionStringLayout.Controls.Add(Me._connectionStatusLed, 1, 1)
        Me._connectionStringLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._connectionStringLayout.Location = New System.Drawing.Point(0, 0)
        Me._connectionStringLayout.Name = "_connectionStringLayout"
        Me._connectionStringLayout.RowCount = 3
        Me._connectionStringLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._connectionStringLayout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._connectionStringLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._connectionStringLayout.Size = New System.Drawing.Size(806, 43)
        Me._connectionStringLayout.TabIndex = 7
        '
        '_connectionStatusLed
        '
        Me._connectionStatusLed.BackColor = System.Drawing.Color.Transparent
        Me._connectionStatusLed.FlashColor = System.Drawing.Color.Red
        Me._connectionStatusLed.Location = New System.Drawing.Point(701, 9)
        Me._connectionStatusLed.Name = "_connectionStatusLed"
        Me._connectionStatusLed.OffColor = System.Drawing.SystemColors.Control
        Me._connectionStatusLed.OnColor = System.Drawing.Color.Red
        Me._connectionStatusLed.Size = New System.Drawing.Size(25, 25)
        Me._connectionStatusLed.TabIndex = 7
        '
        'DatabaseConnector
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._connectionStringLayout)
        Me.Name = "DatabaseConnector"
        Me.Size = New System.Drawing.Size(806, 43)
        Me._connectionStringPanel.ResumeLayout(False)
        Me._connectionStringPanel.PerformLayout()
        Me._connectionStringLayout.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents _toolTip As System.Windows.Forms.ToolTip
    Friend WithEvents _connectionStringTextBox As System.Windows.Forms.TextBox
    Friend WithEvents _connectionStringPanel As System.Windows.Forms.Panel
    Friend WithEvents _connectionStringLayout As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents _connectionStatusLed As isr.Controls.FlashLed
    Friend WithEvents _applyButton As System.Windows.Forms.Button

End Class
