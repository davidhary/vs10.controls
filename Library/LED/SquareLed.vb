''' <summary>Defines a simple LED display with a caption
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="09/05/2006" by="David Hary" revision="2.0.2439.x">
''' created.
''' </history>
<System.ComponentModel.Description("Squared Led Control"), _
    System.Drawing.ToolboxBitmap(GetType(SquareLed))> _
Public Class SquareLed
    Inherits UserControlBase

#Region " APPEARANCE "

    Private _backgroundColor As Integer
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Background Color.")> _
    Public Property BackgroundColor() As Integer
        Get

            Return _backgroundColor

        End Get
        Set(ByVal Value As Integer)

            If Value <> _backgroundColor Then
                _backgroundColor = Value
                BackColor = System.Drawing.ColorTranslator.FromOle(Value)
            End If

        End Set
    End Property

    Private _caption As String
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Caption.")> _
    Public Property Caption() As String
        Get
            Return _caption
        End Get
        Set(ByVal Value As String)

            If Value <> _caption Then
                _caption = Value
                captionLabel.Text = Caption
            End If

        End Set
    End Property

    Private _indicatorColor As Integer
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Indicator color.")> _
    Public Property IndicatorColor() As Integer
        Get

            Return _indicatorColor

        End Get
        Set(ByVal Value As Integer)

            If Value <> _indicatorColor Then
                _indicatorColor = Value
                indicatorLabel.BackColor = System.Drawing.ColorTranslator.FromOle(_indicatorColor)
            End If

        End Set
    End Property

    Private _state As IndicatorState
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Is On.")> _
    Public Property IsOn() As Boolean
        Get
            Return _state = IndicatorState.On
        End Get
        Set(ByVal Value As Boolean)

            If Value <> Me.IsOn Then

                If Value Then
                    Me.State = IndicatorState.On
                Else
                    Me.State = IndicatorState.Off
                End If

            End If

        End Set
    End Property

    Private _noColor As Integer
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("No Color.")> _
    Public Property NoColor() As Integer
        Get

            Return _noColor

        End Get
        Set(ByVal Value As Integer)

            _noColor = Value

        End Set
    End Property

    Private _offColor As Integer
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Off Color.")> _
    Public Property OffColor() As Integer
        Get

            Return _offColor

        End Get
        Set(ByVal Value As Integer)

            _offColor = Value

        End Set
    End Property

    Private _onColor As Integer
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("On Color.")> _
    Public Property OnColor() As Integer
        Get

            Return _onColor

        End Get
        Set(ByVal Value As Integer)

            If Value <> _onColor Then
                _onColor = Value
            End If

        End Set
    End Property

    ''' <summary>Sets the indicator color based on the state.
    ''' </summary>
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Current State.")> _
    Public Property State() As IndicatorState
        Get
            Return _state
        End Get
        Set(ByVal Value As IndicatorState)

            If Value <> _state Then
                _state = Value
                If Me.Visible Then

                    Select Case True
                        Case Value = IndicatorState.None
                            Me.IndicatorColor = Me.NoColor
                        Case Value = IndicatorState.On
                            Me.IndicatorColor = Me.OnColor
                        Case Value = IndicatorState.Off
                            Me.IndicatorColor = Me.OffColor
                    End Select

                End If

            End If

        End Set
    End Property

#End Region

#Region " EVENTS HANDLERS "

    ''' <summary>Occurs when the user clicks the indicator.
    ''' </summary>
    Public Shadows Event Click As EventHandler(Of EventArgs)

    ''' <summary>
    ''' Raises the click event.
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub indicatorLabel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles indicatorLabel.Click

        If ClickEvent IsNot Nothing Then ClickEvent(Me, System.EventArgs.Empty)

    End Sub

    ''' <summary>
    ''' Position the controls.
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub SquareLed_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize

        captionLabel.SetBounds((Width - captionLabel.Width) \ 2, 0, Width, captionLabel.Height, Windows.Forms.BoundsSpecified.All)
        indicatorLabel.SetBounds((Width - indicatorLabel.Width) \ 2, Height - indicatorLabel.Height, 0, 0, Windows.Forms.BoundsSpecified.X Or Windows.Forms.BoundsSpecified.Y)

    End Sub

#End Region

End Class

''' <summary>Enumerates the indicator states.
''' </summary>
Public Enum IndicatorState
  <System.ComponentModel.Description("None")> None
  <System.ComponentModel.Description("On")> [On]
  <System.ComponentModel.Description("Off")> [Off]
End Enum

