<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FlatToggleButton
    Inherits UserControlBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents flatButton1 As FlatButton
    Friend WithEvents flatButton2 As FlatButton

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
        Me.SuspendLayout()
        '
        'flatButton1
        '
        flatButton1 = New FlatButton()
        flatButton1.Location = New System.Drawing.Point(1, 0)
        '
        'flatButton2
        '
        flatButton2 = New FlatButton()
        flatButton2.Location = New System.Drawing.Point(59, 0)

        ' Me.Controls.AddRange(New System.Windows.Forms.Control() {flatButton2, flatButton1})
        Me.Controls.Add(flatButton1)
        Me.Controls.Add(flatButton2)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Size = New System.Drawing.Size(119, 20)
        Me.ResumeLayout(False)

    End Sub

End Class
