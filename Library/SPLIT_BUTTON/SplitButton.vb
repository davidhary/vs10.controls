Imports System.ComponentModel
Partial Public Class SplitButton
    Inherits Button

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructor.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

#End Region

#Region " PROPERTIES "

    Private _doubleClickedEnabled As Boolean
    ''' <summary>
    ''' Gets or sets whether the double click event is raised on the SplitButton.
    ''' </summary>
    <Category("Behavior"), Description("Indicates whether the double click event is raised on the SplitButton"), _
      DefaultValue(False)> _
    Public Property DoubleClickedEnabled() As Boolean
        Get
            Return _doubleClickedEnabled
        End Get
        Set(ByVal value As Boolean)
            _doubleClickedEnabled = value
        End Set
    End Property

    Private _alwaysDropDown As Boolean
    ''' <summary>
    ''' Gets or sests whether the SplitButton always shows the drop down menu even if the button part of the SplitButton is clicked.
    ''' </summary>
    <Category("Split Button"), Description("Indicates whether the SplitButton always shows the drop down menu even if the button part of the SplitButton is clicked."), _
      DefaultValue(False)> _
    Public Property AlwaysDropDown() As Boolean
        Get
            Return _alwaysDropDown
        End Get
        Set(ByVal value As Boolean)
            _alwaysDropDown = value
        End Set
    End Property

    Private _alwaysHoverChange As Boolean
    ''' <summary>
    ''' Gets or sets whether the SplitButton always shows the Hover image status in the split part even if the button part of the SplitButton is hovered.
    ''' </summary>
    <Category("Split Button"), Description("Indicates whether the SplitButton always shows the Hover image status in the split part even if the button part of the SplitButton is hovered."), _
      DefaultValue(False)> _
    Public Property AlwaysHoverChange() As Boolean
        Get
            Return _alwaysHoverChange
        End Get
        Set(ByVal value As Boolean)
            _alwaysHoverChange = value
        End Set
    End Property

    Private _calculateSplitRect As Boolean = True
    ''' <summary>
    ''' Gets or sets whether the split rectange must be calculated (basing on Split image size).
    ''' </summary>
    <Category("Split Button"), Description("Indicates whether the split rectange must be calculated (basing on Split image size)."), _
        DefaultValue(True)> _
    Public Property CalculateSplitRect() As Boolean
        Get
            Return _calculateSplitRect
        End Get
        Set(ByVal value As Boolean)
            Dim flag1 As Boolean = _calculateSplitRect

            _calculateSplitRect = value

            If flag1 <> _calculateSplitRect Then
                If _splitWidth > 0 AndAlso _splitHeight > 0 Then
                    initDefaultSplitImages(True)
                End If
            End If
        End Set
    End Property

    Private _fillSplitHeight As Boolean = True
    ''' <summary>
    ''' Gets or sets a value indicating whether [fill split height].
    ''' </summary>
    ''' <value><c>true</c> if [fill split height]; otherwise, <c>false</c>.</value>
    <Category("Split Button"), Description("Indicates whether the split height must be filled to the button height even if the split image height is lower."), _
      DefaultValue(True)> _
    Public Property FillSplitHeight() As Boolean
        Get
            Return _fillSplitHeight
        End Get
        Set(ByVal value As Boolean)
            _fillSplitHeight = value
        End Set
    End Property

    Private _splitHeight As Integer
    ''' <summary>
    ''' Gets or sets the split height (ignored if CalculateSplitRect is setted to true).
    ''' </summary>
    <Category("Split Button"), Description("The split height (ignored if CalculateSplitRect is setted to true)."), _
        DefaultValue(0)> _
    Public Property SplitHeight() As Integer
        Get
            Return _splitHeight
        End Get
        Set(ByVal value As Integer)
            _splitHeight = value

            If (Not _calculateSplitRect) Then
                If _splitWidth > 0 AndAlso _splitHeight > 0 Then
                    initDefaultSplitImages(True)
                End If
            End If
        End Set
    End Property

    Private _splitWidth As Integer
    ''' <summary>
    ''' Gets or sets the split width (ignored if CalculateSplitRect is setted to true).
    ''' </summary>
    <Category("Split Button"), Description("The split width (ignored if CalculateSplitRect is setted to true)."), _
        DefaultValue(0)> _
    Public Property SplitWidth() As Integer
        Get
            Return _splitWidth
        End Get
        Set(ByVal value As Integer)
            _splitWidth = value

            If (Not _calculateSplitRect) Then
                If _splitWidth > 0 AndAlso _splitHeight > 0 Then
                    initDefaultSplitImages(True)
                End If
            End If
        End Set
    End Property

    Private _normalImage As String
    ''' <summary>
    ''' Gets or sets the Normal status image name in the ImageList.
    ''' </summary>
    ''' <remarks>Add an ImageList to your form (or control or whatever) and set the ImageList property 
    ''' of the SplitButton to this ImageList. Add images you want to the ImageList for the SplitButton 
    ''' splitter side statuses: <see cref="NormalImage">Normal</see>, <see cref="HoverImage">Hover</see>, 
    ''' <see cref="ClickedImage">Clicked</see>, <see cref="DisabledImage">Disabled</see>, 
    ''' <see cref="FocusedImage">Focused</see>. Now, have a look at the property windows for the 
    ''' SplitButton, go to "Split Button Images" category, and simply select an image for each status 
    ''' from the list box of images (the ones in the ImageList of the SplitButton). Now, you can set 
    ''' some suggested options: TextAlign to MiddleLeft, ImageAlign to MiddleRight, and 
    ''' TextImageRelation to TextBeforeImage. You're done. 
    ''' </remarks>
    <Category("Split Button Images"), Description("The Normal status image name in the ImageList."), _
      DefaultValue(""), Editor("System.Windows.Forms.Design.ImageIndexEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a_", _
        GetType(Drawing.Design.UITypeEditor)), Localizable(True), RefreshProperties(RefreshProperties.Repaint), TypeConverter(GetType(ImageKeyConverter))> _
    Public Property NormalImage() As String
        Get
            Return _normalImage
        End Get
        Set(ByVal value As String)
            _normalImage = value
        End Set
    End Property

    Private _hoverImage As String
    ''' <summary>
    ''' Gets or sets the status image name in the ImageList.
    ''' </summary>
    <Category("Split Button Images"), Description("The Hover status image name in the ImageList."), _
      DefaultValue(""), Editor("System.Windows.Forms.Design.ImageIndexEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", _
      GetType(Drawing.Design.UITypeEditor)), Localizable(True), RefreshProperties(RefreshProperties.Repaint), TypeConverter(GetType(ImageKeyConverter))> _
    Public Property HoverImage() As String
        Get
            Return _hoverImage
        End Get
        Set(ByVal value As String)
            _hoverImage = value
        End Set
    End Property

    Private _clickedImage As String
    ''' <summary>
    ''' Gets or sets the Clicked status image name in the ImageList.
    ''' </summary>
    <Category("Split Button Images"), Description("The Clicked status image name in the ImageList."), _
        DefaultValue(""), Editor("System.Windows.Forms.Design.ImageIndexEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", _
      GetType(Drawing.Design.UITypeEditor)), Localizable(True), RefreshProperties(RefreshProperties.Repaint), TypeConverter(GetType(ImageKeyConverter))> _
    Public Property ClickedImage() As String
        Get
            Return _clickedImage
        End Get
        Set(ByVal value As String)
            _clickedImage = value
        End Set
    End Property

    Private _disabledImage As String
    ''' <summary>
    ''' Gets or sets the Disabled status image name in the ImageList.
    ''' </summary>
    <Category("Split Button Images"), Description("The Disabled status image name in the ImageList."), _
        DefaultValue(""), Editor("System.Windows.Forms.Design.ImageIndexEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", _
        GetType(Drawing.Design.UITypeEditor)), Localizable(True), RefreshProperties(RefreshProperties.Repaint), TypeConverter(GetType(ImageKeyConverter))> _
    Public Property DisabledImage() As String
        Get
            Return _disabledImage
        End Get
        Set(ByVal value As String)
            _disabledImage = value
        End Set
    End Property

    Private _focusedImage As String
    ''' <summary>
    ''' Gets or sets the Focused status image name in the ImageList.
    ''' </summary>
    <Category("Split Button Images"), Description("The Focused status image name in the ImageList."), _
        DefaultValue(""), Editor("System.Windows.Forms.Design.ImageIndexEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", _
        GetType(Drawing.Design.UITypeEditor)), Localizable(True), RefreshProperties(RefreshProperties.Repaint), TypeConverter(GetType(ImageKeyConverter))> _
    Public Property FocusedImage() As String
        Get
            Return _focusedImage
        End Get
        Set(ByVal value As String)
            _focusedImage = value
        End Set
    End Property

#End Region

#Region " PRIVATE METHODS "

    Private Sub initDefaultSplitImages()
        initDefaultSplitImages(False)
    End Sub

    Private Sub initDefaultSplitImages(ByVal refresh As Boolean)

        If String.IsNullOrEmpty(_normalImage) Then
            _normalImage = "Normal"
        End If

        If String.IsNullOrEmpty(_hoverImage) Then
            _hoverImage = "Hover"
        End If

        If String.IsNullOrEmpty(_clickedImage) Then
            _clickedImage = "Clicked"
        End If

        If String.IsNullOrEmpty(_disabledImage) Then
            _disabledImage = "Disabled"
        End If

        If String.IsNullOrEmpty(_focusedImage) Then
            _focusedImage = "Focused"
        End If

        If _defaultSplitImages Is Nothing Then
            _defaultSplitImages = New ImageList()
        End If

        If _defaultSplitImages.Images.Count = 0 OrElse refresh Then
            If _defaultSplitImages.Images.Count > 0 Then
                _defaultSplitImages.Images.Clear()
            End If

            Dim w As Integer = 0
            Dim h As Integer = 0

            If (Not _calculateSplitRect) AndAlso _splitWidth > 0 Then
                w = _splitWidth
            Else
                w = 18
            End If

            If (Not CalculateSplitRect) AndAlso _splitHeight > 0 Then
                h = _splitHeight
            Else
                h = Height
            End If

            h -= 8

            _defaultSplitImages.ImageSize = New Size(w, h)

            Dim mw As Integer = w \ 2
            mw += (mw Mod 2)
            Dim mh As Integer = h \ 2

            Dim fPen As Pen = New Pen(ForeColor, 1)
            Dim fBrush As SolidBrush = New SolidBrush(ForeColor)

            Dim imgN As Bitmap = New Bitmap(w, h)
            Dim g As Graphics = Graphics.FromImage(imgN)

            g.CompositingQuality = Drawing.Drawing2D.CompositingQuality.HighQuality

            g.DrawLine(SystemPens.ButtonShadow, New Point(1, 1), New Point(1, h - 2))
            g.DrawLine(SystemPens.ButtonFace, New Point(2, 1), New Point(2, h))

            g.FillPolygon(fBrush, New Point() {New Point(mw - 2, mh - 1), New Point(mw + 3, mh - 1), New Point(mw, mh + 2)})

            g.Dispose()

            Dim imgH As Bitmap = New Bitmap(w, h)
            g = Graphics.FromImage(imgH)

            g.CompositingQuality = Drawing.Drawing2D.CompositingQuality.HighQuality

            g.DrawLine(SystemPens.ButtonShadow, New Point(1, 1), New Point(1, h - 2))
            g.DrawLine(SystemPens.ButtonFace, New Point(2, 1), New Point(2, h))

            g.FillPolygon(fBrush, New Point() {New Point(mw - 3, mh - 2), New Point(mw + 4, mh - 2), New Point(mw, mh + 2)})

            g.Dispose()

            Dim imgC As Bitmap = New Bitmap(w, h)
            g = Graphics.FromImage(imgC)

            g.CompositingQuality = Drawing.Drawing2D.CompositingQuality.HighQuality

            g.DrawLine(SystemPens.ButtonShadow, New Point(1, 1), New Point(1, h - 2))
            g.DrawLine(SystemPens.ButtonFace, New Point(2, 1), New Point(2, h))

            g.FillPolygon(fBrush, New Point() {New Point(mw - 2, mh - 1), New Point(mw + 3, mh - 1), New Point(mw, mh + 2)})

            g.Dispose()

            Dim imgD As Bitmap = New Bitmap(w, h)
            g = Graphics.FromImage(imgD)

            g.CompositingQuality = Drawing.Drawing2D.CompositingQuality.HighQuality

            g.DrawLine(SystemPens.GrayText, New Point(1, 1), New Point(1, h - 2))

            g.FillPolygon(New SolidBrush(SystemColors.GrayText), New Point() {New Point(mw - 2, mh - 1), New Point(mw + 3, mh - 1), New Point(mw, mh + 2)})

            g.Dispose()

            Dim imgF As Bitmap = New Bitmap(w, h)
            g = Graphics.FromImage(imgF)

            g.CompositingQuality = Drawing.Drawing2D.CompositingQuality.HighQuality

            g.DrawLine(SystemPens.ButtonShadow, New Point(1, 1), New Point(1, h - 2))
            g.DrawLine(SystemPens.ButtonFace, New Point(2, 1), New Point(2, h))

            g.FillPolygon(fBrush, New Point() {New Point(mw - 2, mh - 1), New Point(mw + 3, mh - 1), New Point(mw, mh + 2)})

            g.Dispose()

            fPen.Dispose()
            fBrush.Dispose()

            _defaultSplitImages.Images.Add(_normalImage, imgN)
            _defaultSplitImages.Images.Add(_hoverImage, imgH)
            _defaultSplitImages.Images.Add(_clickedImage, imgC)
            _defaultSplitImages.Images.Add(_disabledImage, imgD)
            _defaultSplitImages.Images.Add(_focusedImage, imgF)
        End If
    End Sub

    Private Function mouseInSplit() As Boolean
        Return pointInSplit(PointToClient(MousePosition))
    End Function

    Private Function pointInSplit(ByVal pt As Point) As Boolean

        Dim splitRect As Rectangle = SplitButton.getImageRect(Me.ImageList.Images(_normalImage), _
            Me, Me.ImageAlign, Me.FillSplitHeight)

        If (Not _calculateSplitRect) Then
            splitRect.Width = _splitWidth
            splitRect.Height = _splitHeight
        End If

        Return splitRect.Contains(pt)
    End Function

    Private Sub setSplit(ByVal imageName As String)
        If Not imageName Is Nothing AndAlso Not ImageList Is Nothing AndAlso ImageList.Images.ContainsKey(imageName) Then
            Me.ImageKey = imageName
        End If
    End Sub

    ''' <summary>
    ''' Get image rectange withing the specified <para>Control</para>.
    ''' </summary>
    ''' <param name="currentImage"></param>
    ''' <param name="control"></param>
    ''' <param name="imageAlign"></param>
    ''' <param name="fillHeight"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function getImageRect(ByVal currentImage As Image, ByVal control As Control, _
      ByVal imageAlign As ContentAlignment, ByVal fillHeight As Boolean) As Rectangle

        If currentImage Is Nothing OrElse control Is Nothing Then
            Return Rectangle.Empty
        End If

        Dim x As Integer = 0, y As Integer = 0, w As Integer = currentImage.Width + 1, h As Integer = currentImage.Height + 1

        If w > control.Width Then
            w = control.Width
        End If

        If h > control.Width Then
            h = control.Width
        End If

        Select Case imageAlign
            Case ContentAlignment.TopLeft
                x = 0
                y = 0

                Exit Select
            Case ContentAlignment.TopCenter
                x = (control.Width - w) \ 2
                y = 0

                If (control.Width - w) Mod 2 > 0 Then
                    x += 1
                End If

                Exit Select
            Case ContentAlignment.TopRight
                x = control.Width - w
                y = 0

                Exit Select
            Case ContentAlignment.MiddleLeft
                x = 0
                y = (control.Height - h) \ 2

                If (control.Height - h) Mod 2 > 0 Then
                    y += 1
                End If

                Exit Select
            Case ContentAlignment.MiddleCenter
                x = (control.Width - w) \ 2
                y = (control.Height - h) \ 2

                If (control.Width - w) Mod 2 > 0 Then
                    x += 1
                End If
                If (control.Height - h) Mod 2 > 0 Then
                    y += 1
                End If

                Exit Select
            Case ContentAlignment.MiddleRight
                x = control.Width - w
                y = (control.Height - h) \ 2

                If (control.Height - h) Mod 2 > 0 Then
                    y += 1
                End If

                Exit Select
            Case ContentAlignment.BottomLeft
                x = 0
                y = control.Height - h

                If (control.Height - h) Mod 2 > 0 Then
                    y += 1
                End If

                Exit Select
            Case ContentAlignment.BottomCenter
                x = (control.Width - w) \ 2
                y = control.Height - h

                If (control.Width - w) Mod 2 > 0 Then
                    x += 1
                End If

                Exit Select
            Case ContentAlignment.BottomRight
                x = control.Width - w
                y = control.Height - h

                Exit Select
        End Select

        If fillHeight AndAlso h < control.Height Then
            h = control.Height
        End If

        If x > 0 Then
            x -= 1
        End If
        If y > 0 Then
            y -= 1
        End If

        Return New Rectangle(x, y, w, h)

    End Function


#End Region

#Region " EVENT HANDLERS "

    Private _defaultSplitImages As ImageList

    <System.ComponentModel.Browsable(True), _
        System.ComponentModel.Category("Action"), _
        System.ComponentModel.Description("Occurs when the button part of the SplitButton is clicked.")> _
    Public Event ButtonClick As EventHandler

    <System.ComponentModel.Browsable(True), _
        System.ComponentModel.Category("Action"), _
        System.ComponentModel.Description("Occurs when the button part of the SplitButton is clicked.")> _
    Public Event ButtonDoubleClick As EventHandler

    Protected Overrides Sub OnCreateControl()

        initDefaultSplitImages()

        If Me.ImageList Is Nothing Then
            Me.ImageList = _defaultSplitImages
        End If

        If Enabled Then
            setSplit(_normalImage)
        Else
            setSplit(_disabledImage)
        End If

        MyBase.OnCreateControl()

    End Sub

    Protected Overrides Sub OnMouseMove(ByVal e As MouseEventArgs)
        If _alwaysDropDown OrElse _alwaysHoverChange OrElse mouseInSplit() Then
            If Enabled Then
                setSplit(_hoverImage)
            End If
        Else
            If Enabled Then
                setSplit(_normalImage)
            End If
        End If

        MyBase.OnMouseMove(e)
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        If Enabled Then
            setSplit(_normalImage)
        End If

        MyBase.OnMouseLeave(e)
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        If _alwaysDropDown OrElse mouseInSplit() Then
            If Enabled Then
                setSplit(_clickedImage)

                If Not Me.ContextMenuStrip Is Nothing AndAlso Me.ContextMenuStrip.Items.Count > 0 Then
                    Me.ContextMenuStrip.Show(Me, New Point(0, Height))
                End If
            End If
        Else
            If Enabled Then
                setSplit(_normalImage)
            End If
        End If

        MyBase.OnMouseDown(e)
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal e As MouseEventArgs)
        If _alwaysDropDown OrElse _alwaysHoverChange OrElse mouseInSplit() Then
            If Enabled Then
                setSplit(_hoverImage)
            End If
        Else
            If Enabled Then
                setSplit(_normalImage)
            End If
        End If

        MyBase.OnMouseUp(e)
    End Sub

    Protected Overrides Sub OnEnabledChanged(ByVal e As EventArgs)
        If (Not Enabled) Then
            setSplit(_disabledImage)
        Else
            If mouseInSplit() Then
                setSplit(_hoverImage)
            Else
                setSplit(_normalImage)
            End If
        End If

        MyBase.OnEnabledChanged(e)
    End Sub

    Protected Overrides Sub OnGotFocus(ByVal e As EventArgs)
        If Enabled Then
            setSplit(_focusedImage)
        End If

        MyBase.OnGotFocus(e)
    End Sub

    Protected Overrides Sub OnLostFocus(ByVal e As EventArgs)
        If Enabled Then
            setSplit(_normalImage)
        End If
        MyBase.OnLostFocus(e)
    End Sub

    Protected Overrides Sub OnClick(ByVal e As EventArgs)
        MyBase.OnClick(e)
        If (Not mouseInSplit()) AndAlso (Not _alwaysDropDown) AndAlso ButtonClickEvent IsNot Nothing Then ButtonClickEvent(Me, e)
    End Sub

    Protected Overrides Sub OnDoubleClick(ByVal e As EventArgs)
        If _doubleClickedEnabled Then
            MyBase.OnDoubleClick(e)
            If (Not mouseInSplit()) AndAlso (Not _alwaysDropDown) AndAlso ButtonClickEvent Is Nothing Then ButtonClickEvent(Me, e)
        End If
    End Sub

#End Region

End Class
