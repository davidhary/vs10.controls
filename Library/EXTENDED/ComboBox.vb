﻿Imports System.Drawing
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Security.Permissions
''' <summary>
''' Combo box with read only capability.
''' </summary>
''' <remarks></remarks>
''' <shoutouts>
''' Dan.A.
''' http://www.codeproject.com/KB/combobox/csReadonlyComboBox.aspx
''' </shoutouts>
<DesignerCategory("code"), System.ComponentModel.Description("Extended Combo Box"), _
    System.Drawing.ToolboxBitmap(GetType(ComboBox))> _
Public Class ComboBox
    Inherits Windows.Forms.ComboBox

    Public Sub New()
        MyBase.New()
        _DropDownStyle = ComboBoxStyle.DropDown
        '_ReadOnlyBackColor = SystemColors.Control
        '_ReadOnlyForeColor = SystemColors.WindowText
    End Sub

    Private readWriteBackColor As Color
    Private readWriteForeColor As Color
    Private readWriteContextMenu As ContextMenu
    Private _ReadOnly As Boolean

    <DefaultValue(False)> _
    <Category("Behavior")> _
    <Description("Indicates whether the combo box is read only.")> _
    Public Property [ReadOnly]() As Boolean
        Get
            Return _ReadOnly
        End Get
        Set(ByVal value As Boolean)
            If _ReadOnly <> value Then
                _ReadOnly = value
                If value Then
                    readWriteContextMenu = MyBase.ContextMenu
                    Me.ContextMenu = New ContextMenu()
                    Dim h As Integer = MyBase.Height
                    MyBase.DropDownStyle = ComboBoxStyle.Simple
                    MyBase.Height = h + 3
                    Me.readWriteBackColor = Me.BackColor
                    Me.readWriteForeColor = Me.ForeColor
                    MyBase.BackColor = Me.ReadOnlyBackColor
                    MyBase.ForeColor = Me.ReadOnlyForeColor
                Else
                    Me.ContextMenu = readWriteContextMenu
                    MyBase.DropDownStyle = _DropDownStyle
                    MyBase.BackColor = readWriteBackColor
                    MyBase.ForeColor = readWriteForeColor
                End If
            End If
        End Set
    End Property

    Private _ReadOnlyBackColor As Color
    <DefaultValue(GetType(Drawing.Color), "SystemColors.Control")> _
    <Description("Back color when read only"), Category("Appearance")> _
    Public Property ReadOnlyBackColor() As Color
        Get
            Return _ReadOnlyBackColor
        End Get
        Set(ByVal value As Color)
            _ReadOnlyBackColor = value
        End Set
    End Property

    Private _ReadOnlyForeColor As Color
    <DefaultValue(GetType(Drawing.Color), "SystemColors.WindowText")> _
    <Description("Fore color when read only"), Category("Appearance")> _
    Public Property ReadOnlyForeColor() As Color
        Get
            Return _ReadOnlyForeColor
        End Get
        Set(ByVal value As Color)
            _ReadOnlyForeColor = value
        End Set
    End Property

    Private _DropDownStyle As ComboBoxStyle
    Public Shadows Property DropDownStyle() As ComboBoxStyle
        Get
            Return _DropDownStyle
        End Get
        Set(ByVal value As ComboBoxStyle)
            If _DropDownStyle <> value Then
                _DropDownStyle = value
                If (Not Me.ReadOnly) Then
                    MyBase.DropDownStyle = value
                End If
            End If
        End Set
    End Property

    Protected Overrides Sub OnKeyDown(ByVal e As KeyEventArgs)
        If Me.ReadOnly AndAlso (e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Delete OrElse _
                                e.KeyCode = Keys.F4 OrElse e.KeyCode = Keys.PageDown OrElse e.KeyCode = Keys.PageUp) Then
            e.Handled = True
        Else
            MyBase.OnKeyDown(e)
        End If
    End Sub

    Protected Overrides Sub OnKeyPress(ByVal e As KeyPressEventArgs)
        If Me.ReadOnly Then
            e.Handled = True
        Else
            MyBase.OnKeyPress(e)
        End If
    End Sub

End Class
