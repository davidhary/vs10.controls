<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ControlsTestPanel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlashLed1 = New isr.Controls.FlashLed
        Me.SuspendLayout()
        '
        'FlashLed1
        '
        Me.FlashLed1.BackColor = System.Drawing.Color.Transparent
        Me.FlashLed1.FlashColor = System.Drawing.SystemColors.Control
        Me.FlashLed1.Location = New System.Drawing.Point(10, 10)
        Me.FlashLed1.Name = "FlashLed1"
        Me.FlashLed1.OffColor = System.Drawing.SystemColors.Control
        Me.FlashLed1.OnColor = System.Drawing.Color.Red
        Me.FlashLed1.Size = New System.Drawing.Size(96, 93)
        Me.FlashLed1.TabIndex = 0
        '
        'ControlsTestPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(551, 334)
        Me.Controls.Add(Me.FlashLed1)
        Me.Name = "ControlsTestPanel"
        Me.Text = "ControlsTestPanel"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FlashLed1 As isr.Controls.FlashLed

End Class
