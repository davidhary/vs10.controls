﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ISR Controls VB Library")> 
<Assembly: AssemblyDescription("ISR Controls VB Library")> 
<Assembly: AssemblyCompany("Integrated Scientific Resources, Inc.")> 
<Assembly: AssemblyProduct("ISR Controls VB Library 1.2")> 
<Assembly: AssemblyCopyright("(c) 2007 Integrated Scientific Resources, Inc. All rights reserved.")> 
<Assembly: AssemblyTrademark("Licensed under the Apache 2.0 License")> 
<Assembly: CLSCompliant(True)> 
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)> 


<Assembly: ComVisible(False)>
'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("36024683-a7b6-4854-9e0e-fa37c75a8660")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
<Assembly: AssemblyVersion("1.2.*")> 
<Assembly: AssemblyFileVersion("1.2.0.0")> 
<Assembly: AssemblyInformationalVersion("1.2.0.0")> 
