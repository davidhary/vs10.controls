'''   <summary>A flashing LED Control.</summary>
''' <history date="09/08/07" by="Microsoft" revision="1.0.2807.x">
''' .Net toogle Button example.
''' </history>
''' <history date="09/08/07" by="David Hary" revision="1.0.2807.x">
''' Add to controls.
''' </history>
<System.ComponentModel.Description("Flat Toggle Button"), _
    System.Drawing.ToolboxBitmap(GetType(FlatToggleButton))> _
Public Class FlatToggleButton

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructor.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Default to first toggle
        flatButton1.IsSelected = True

        ' Repaint
        Invalidate()

    End Sub

#End Region

#Region " PROPERTIES  "

    ''' <summary>
    ''' Gets or sets the value indicating whether the first character that is preceded with an 
    ''' Ampersand is used as a mnemonic key.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Indicates using a mnemonic key."), _
      System.ComponentModel.DefaultValue(True)> _
    Public Property UseMnemonic() As Boolean
        Get
            Return _flatButton1.UseMnemonic
        End Get
        Set(ByVal value As Boolean)
            _flatButton1.UseMnemonic = value
            _flatButton2.UseMnemonic = value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if button one is selected.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property IsButton1Selected() As Boolean
        Get
            Return _flatButton1.IsSelected
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the text of the first button.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Caption of first button.")> _
    Public Property FlatButton1Text() As String
        Get
            Return flatButton1.Text
        End Get
        Set(ByVal Value As String)
            flatButton1.Text = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the image index of the first button.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Image index of first button."), _
      System.ComponentModel.DefaultValue(1)> _
    Public Property FlatButton1ImageIndex() As Integer
        Get
            Return flatButton1.ImageIndex
        End Get
        Set(ByVal Value As Integer)
            flatButton1.ImageIndex = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the location of the fiorst flat button.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FlatButton1Location() As Point
        Get
            Return flatButton1.Location
        End Get
        Set(ByVal value As Point)
            flatButton1.Location = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the text of the second button.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Caption of second button.")> _
    Public Property FlatButton2Text() As String
        Get
            Return flatButton2.Text
        End Get
        Set(ByVal Value As String)
            flatButton2.Text = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the image index of the second button.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Image index of second button."), _
      System.ComponentModel.DefaultValue(2)> _
    Public Property FlatButton2ImageIndex() As Integer
        Get
            Return flatButton2.ImageIndex
        End Get
        Set(ByVal Value As Integer)
            flatButton2.ImageIndex = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the location of the fiorst flat button.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FlatButton2Location() As Point
        Get
            Return flatButton2.Location
        End Get
        Set(ByVal value As Point)
            flatButton2.Location = value
        End Set
    End Property

#End Region

#Region " EVENT HANDLERS "

    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Action"), _
      System.ComponentModel.Description("Occurs when either button is clicked.")> _
    Public Event ButtonClick As EventHandler

    ''' <summary>
    ''' Selects button 1.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OnClickButton1(ByVal sender As Object, ByVal e As System.EventArgs) Handles flatButton1.Click

        ' Toggle button views
        flatButton1.IsSelected = True
        flatButton2.IsSelected = False

        ' raise the button click event.
        If ButtonClickEvent IsNot Nothing Then ButtonClickEvent(Me, e)

        ' Repaint
        Me.Invalidate()

    End Sub

    ''' <summary>
    ''' Selects the second button.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OnClickButton2(ByVal sender As Object, ByVal e As System.EventArgs) Handles flatButton2.Click

        ' Toggle button views
        flatButton1.IsSelected = False
        flatButton2.IsSelected = True

        ' raise the button click event.
        If ButtonClickEvent IsNot Nothing Then ButtonClickEvent(Me, e)


        ' Repaint
        Invalidate()

    End Sub

    ''' <summary>
    ''' Paints the button
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)

        Dim grayPen As New Pen(Color.Gray, 1)
        Dim whitePen As New Pen(Color.White, 1)
        Dim toggleGraphics As Graphics = e.Graphics

        If Me.IsButton1Selected Then
            'toggleGraphics.DrawRectangle(grayPen, 72, 4, 68, 11)
            toggleGraphics.DrawLine(grayPen, 116, 0, 116, 19)
            toggleGraphics.DrawLine(whitePen, 117, 0, 117, 19)
        Else
            'toggleGraphics.DrawRectangle(grayPen, 4, 4, 68, 11)
            toggleGraphics.DrawLine(grayPen, 0, 0, 0, 19)
            toggleGraphics.DrawLine(whitePen, 1, 0, 1, 19)
        End If
        grayPen.Dispose()
        whitePen.Dispose()

    End Sub

#End Region

End Class
