<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Switchboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Switchboard))
        Me._cancelButton = New System.Windows.Forms.Button
        Me._acceptButton = New System.Windows.Forms.Button
        Me._testButton = New System.Windows.Forms.Button
        Me._showAboutBoxButton = New System.Windows.Forms.Button
        Me._actionSelectorComboBox = New System.Windows.Forms.ComboBox
        Me._openPanelButton = New System.Windows.Forms.Button
        Me._messagesPanel = New System.Windows.Forms.Panel
        Me._messagesBox = New isr.Controls.MessagesBox
        Me._messagesPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'cancelButton1
        '
        Me._cancelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._cancelButton.Location = New System.Drawing.Point(167, 38)
        Me._cancelButton.Name = "cancelButton1"
        Me._cancelButton.Size = New System.Drawing.Size(60, 23)
        Me._cancelButton.TabIndex = 7
        Me._cancelButton.Text = "&Cancel"
        '
        'exitButton
        '
        Me._acceptButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._acceptButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me._acceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._acceptButton.Location = New System.Drawing.Point(374, 38)
        Me._acceptButton.Name = "exitButton"
        Me._acceptButton.Size = New System.Drawing.Size(60, 23)
        Me._acceptButton.TabIndex = 5
        Me._acceptButton.Text = "E&xit"
        '
        'testButton
        '
        Me._testButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._testButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._testButton.Location = New System.Drawing.Point(236, 38)
        Me._testButton.Name = "testButton"
        Me._testButton.Size = New System.Drawing.Size(60, 23)
        Me._testButton.TabIndex = 8
        Me._testButton.Text = "&Test"
        '
        'aboutButton
        '
        Me._showAboutBoxButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._showAboutBoxButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._showAboutBoxButton.Location = New System.Drawing.Point(305, 38)
        Me._showAboutBoxButton.Name = "aboutButton"
        Me._showAboutBoxButton.Size = New System.Drawing.Size(60, 23)
        Me._showAboutBoxButton.TabIndex = 10
        Me._showAboutBoxButton.Text = "&About"
        '
        'ActionsComboBox
        '
        Me._actionSelectorComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._actionSelectorComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._actionSelectorComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._actionSelectorComboBox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._actionSelectorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._actionSelectorComboBox.Location = New System.Drawing.Point(9, 3)
        Me._actionSelectorComboBox.Name = "ActionsComboBox"
        Me._actionSelectorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._actionSelectorComboBox.Size = New System.Drawing.Size(361, 27)
        Me._actionSelectorComboBox.TabIndex = 11
        Me._actionSelectorComboBox.Text = "Select option from the list"
        '
        'openButton
        '
        Me._openPanelButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._openPanelButton.Location = New System.Drawing.Point(376, 4)
        Me._openPanelButton.Name = "openButton"
        Me._openPanelButton.Size = New System.Drawing.Size(58, 24)
        Me._openPanelButton.TabIndex = 12
        Me._openPanelButton.Text = "&Open..."
        '
        'Panel1
        '
        Me._messagesPanel.Controls.Add(Me._actionSelectorComboBox)
        Me._messagesPanel.Controls.Add(Me._cancelButton)
        Me._messagesPanel.Controls.Add(Me._acceptButton)
        Me._messagesPanel.Controls.Add(Me._openPanelButton)
        Me._messagesPanel.Controls.Add(Me._testButton)
        Me._messagesPanel.Controls.Add(Me._showAboutBoxButton)
        Me._messagesPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._messagesPanel.Location = New System.Drawing.Point(0, 199)
        Me._messagesPanel.Name = "Panel1"
        Me._messagesPanel.Size = New System.Drawing.Size(442, 66)
        Me._messagesPanel.TabIndex = 15
        '
        'MessagesBox1
        '
        Me._messagesBox.Bullet = "* "
        Me._messagesBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._messagesBox.Location = New System.Drawing.Point(0, 0)
        Me._messagesBox.Multiline = True
        Me._messagesBox.Name = "MessagesBox1"
        Me._messagesBox.PresetCount = 50
        Me._messagesBox.ReadOnly = True
        Me._messagesBox.ResetCount = 100
        Me._messagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._messagesBox.Size = New System.Drawing.Size(442, 199)
        Me._messagesBox.TabIndex = 16
        Me._messagesBox.TimeFormat = "HH:mm:ss. "
        Me._messagesBox.UsingBullet = True
        Me._messagesBox.UsingTimeBullet = True
        '
        'Switchboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(442, 265)
        Me.Controls.Add(Me._messagesBox)
        Me.Controls.Add(Me._messagesPanel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Switchboard"
        Me.Text = "Form1"
        Me._messagesPanel.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents _cancelButton As System.Windows.Forms.Button
    Friend WithEvents _acceptButton As System.Windows.Forms.Button
    Friend WithEvents _testButton As System.Windows.Forms.Button
    Friend WithEvents _showAboutBoxButton As System.Windows.Forms.Button
    Friend WithEvents _actionSelectorComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents _openPanelButton As System.Windows.Forms.Button
    Friend WithEvents _messagesPanel As System.Windows.Forms.Panel
    Friend WithEvents _messagesBox As isr.Controls.MessagesBox
End Class
