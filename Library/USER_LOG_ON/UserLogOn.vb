﻿Imports System.ComponentModel
Imports isr.Core
''' <summary>
''' User log on/off interface.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/30/08" by="David Hary" revision="1.00.3225.x">
''' created.
''' </history>
<Description("User Log On Control"), _
    DefaultEvent("UserValidated"), _
    System.Drawing.ToolboxBitmap(GetType(UserLogOn))> _
<System.Runtime.InteropServices.ComVisible(False)> _
Public Class UserLogOn

#Region " USER ACTIONS "

    ''' <summary>
    ''' Gets or set the validation status of this user.
    ''' This is the same as logged in.
    ''' </summary>
    ''' <remarks></remarks>
    Private _isValidated As Boolean
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public Property IsValidated() As Boolean
        Get
            Return _isValidated
        End Get
        Set(ByVal value As Boolean)
            _isValidated = value
            If Me._isValidated Then
                Me._logOnButton.Text = Me.LogOffCaption
            Else
                Me._logOnButton.Text = Me.LogOnCaption
            End If
        End Set
    End Property

    Private _validatiionMessage As String
    ''' <summary>
    ''' Gets the last validation message.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public ReadOnly Property ValidationMessage() As String
        Get
            Return _validatiionMessage
        End Get
    End Property

#End Region

#Region " ALLOWED USER ROLES "

    Private _userRoles As List(Of String)

    ''' <summary>
    ''' Adds a user role to the list off allowed user roles. 
    ''' </summary>
    ''' <param name="value">Specifies a user role to add to the list of allowed user roles.</param>
    ''' <remarks></remarks>
    Public Sub AddUserRole(ByVal value As String)
        If _userRoles Is Nothing Then
            _userRoles = New List(Of String)
        End If
        If Not _userRoles.Contains(value) Then
            _userRoles.Add(value)
        End If
    End Sub

    ''' <summary>
    ''' Clears the allowed user roles.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearUserRoles()
        _userRoles = New List(Of String)
    End Sub

    ''' <summary>
    ''' Returns true if the specified role is allowed. 
    ''' </summary>
    ''' <param name="value">Specifies a user role to check against the list of allowed user roles.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsUserRoleAllowed(ByVal value As String) As Boolean
        Return _userRoles IsNot Nothing AndAlso _userRoles.Contains(value)
    End Function

#End Region

#Region " GUI "

    ''' <summary>
    ''' Gets or sets reference to the error provider.
    ''' </summary>
    ''' <remarks></remarks>
    Private WithEvents _errorProvider As New Windows.Forms.ErrorProvider()

    ''' <summary>
    ''' Log the user on.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub _logOn()
        _validatiionMessage = ""
        ' log in.
        If _userRoles Is Nothing Then
            If Me.UserLogOn.Validate(Me._userNameTextBox.Text, Me._passwordTextBox.Text.Trim) Then
                Me.IsValidated = True
                _validatiionMessage = _userLogOn.ValidationMessage
                If UserValidatedEvent IsNot Nothing Then UserValidatedEvent(Me, System.EventArgs.Empty)
            Else
                _errorProvider.SetError(_logOnButton, Me.ValidationMessage)
                _validatiionMessage = _userLogOn.ValidationMessage
                If ValidationFailedEvent IsNot Nothing Then ValidationFailedEvent(Me, System.EventArgs.Empty)
            End If
        ElseIf Me.UserLogOn.Validate(Me._userNameTextBox.Text, Me._passwordTextBox.Text.Trim, New Collections.ObjectModel.ReadOnlyCollection(Of String)(_userRoles)) Then
            Me.IsValidated = True
            Me._logOnButton.Text = Me.LogOffCaption
            _validatiionMessage = _userLogOn.ValidationMessage
            If UserValidatedEvent IsNot Nothing Then UserValidatedEvent(Me, System.EventArgs.Empty)
        Else
            _errorProvider.SetError(_logOnButton, Me.ValidationMessage)
            _validatiionMessage = _userLogOn.ValidationMessage
            If ValidationFailedEvent IsNot Nothing Then ValidationFailedEvent(Me, System.EventArgs.Empty)
        End If
    End Sub

    ''' <summary>
    ''' Logs in or out.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub _logOnButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _logOnButton.Click

        _errorProvider.SetError(_logOnButton, "")
        _validatiionMessage = ""

        If Me.IsValidated Then

            ' log out.
            Me.IsValidated = False
            If UserInvalidatedEvent IsNot Nothing Then UserInvalidatedEvent(Me, System.EventArgs.Empty)

        Else

            If String.IsNullOrEmpty(_userId) Then
                _logOn()
            ElseIf String.Equals(_userId, Me._userNameTextBox.Text, StringComparison.OrdinalIgnoreCase) Then
                _logOn()
            Else
                _validatiionMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "Only user {0} is allowed", _userId)
                _errorProvider.SetError(_logOnButton, Me.ValidationMessage)
                If ValidationFailedEvent IsNot Nothing Then ValidationFailedEvent(Me, System.EventArgs.Empty)
            End If

        End If

    End Sub

    Private _logOnCaption As String = "&LOGON"
    ''' <summary>
    ''' Gets or sets the log on caption
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Appearance"), Description("Caption for logon"), _
      Browsable(True), _
      DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
      DefaultValue("&LOGON")> _
    Public Property LogOnCaption() As String
        Get
            Return _logOnCaption
        End Get
        Set(ByVal value As String)
            _logOnCaption = value
            ' update the caption
            Me.IsValidated = Me.IsValidated
        End Set
    End Property

    Private _logOffCaption As String = "&LOGOFF"
    ''' <summary>
    ''' Gets or sets the log off caption
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Appearance"), Description("Caption for log off"), _
      Browsable(True), _
      DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
      DefaultValue("&LOGOFF")> _
    Public Property LogOffCaption() As String
        Get
            Return _logOffCaption
        End Get
        Set(ByVal value As String)
            _logOffCaption = value
            ' update the caption
            Me.IsValidated = Me.IsValidated
        End Set
    End Property

    Private _userId As String
    ''' <summary>
    ''' Gets or set the user ID for logon. When set, only a user with the specified ID can be 
    ''' validated.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Appearance"), Description("Singuler user id"), _
      Browsable(True), _
      DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
      DefaultValue("")> _
    Public Property UserId() As String
        Get
            Return _userId
        End Get
        Set(ByVal value As String)
            _userId = value
        End Set
    End Property

    Private _userLogOn As isr.Core.IUserLogOn
    ''' <summary>
    ''' Gets or sets reference to the user log on implementation object.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public Property UserLogOn() As isr.Core.IUserLogOn
        Get
            Return _userLogOn
        End Get
        Set(ByVal value As isr.Core.IUserLogOn)
            _userLogOn = value
            If _userLogOn IsNot Nothing Then
                ' enable the user name and password boxes
                Me._userNameTextBox.Enabled = True
                Me._passwordTextBox.Enabled = True
                Me._logOnButton.Enabled = True
            End If
        End Set
    End Property

    ''' <summary>Initializes the user interface and tool tips.</summary>
    Private Sub userLogOnControl_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        _validatiionMessage = ""
        If LoadingEvent IsNot Nothing Then LoadingEvent(Me, System.EventArgs.Empty)

        ' disable when loaded.
        Me._userNameTextBox.Enabled = False
        Me._userNameTextBox.Text = ""
        Me._passwordTextBox.Enabled = False
        Me._userNameTextBox.Text = ""
        Me._logOnButton.Enabled = False
        Me._logOnButton.Text = Me.LogOnCaption

    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' Occurs when control is loading.  Use this event to set the 
    ''' caption properties.
    ''' </summary>
    ''' <remarks></remarks>
    Event Loading As EventHandler(Of System.EventArgs)

    ''' <summary>
    ''' Occurs when validation failed.
    ''' </summary>
    ''' <remarks></remarks>
    Event ValidationFailed As EventHandler(Of System.EventArgs)

    ''' <summary>
    ''' Occurs when validation succeeded. This is the same as log on.
    ''' </summary>
    ''' <remarks></remarks>
    Event UserValidated As EventHandler(Of System.EventArgs)

    ''' <summary>
    ''' Occurs when invalidation succeeded.  This is the same as loging off.
    ''' </summary>
    ''' <remarks></remarks>
    Event UserInvalidated As EventHandler(Of System.EventArgs)

#End Region

End Class

