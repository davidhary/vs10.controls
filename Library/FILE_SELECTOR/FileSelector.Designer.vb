<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FileSelector

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Friend _toolTip As System.Windows.Forms.ToolTip
    Friend WithEvents _filePathNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents _browseButton As System.Windows.Forms.Button
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me._toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._browseButton = New System.Windows.Forms.Button
        Me._filePathNameTextBox = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        '_browseButton
        '
        Me._browseButton.BackColor = System.Drawing.SystemColors.Control
        Me._browseButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._browseButton.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._browseButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._browseButton.Location = New System.Drawing.Point(516, 0)
        Me._browseButton.Name = "_browseButton"
        Me._browseButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._browseButton.Size = New System.Drawing.Size(34, 22)
        Me._browseButton.TabIndex = 0
        Me._browseButton.Text = "..."
        Me._browseButton.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me._toolTip.SetToolTip(Me._browseButton, "Browses for a file")
        Me._browseButton.UseMnemonic = False
        Me._browseButton.UseVisualStyleBackColor = True
        '
        '_filePathNameTextBox
        '
        Me._filePathNameTextBox.AcceptsReturn = True
        Me._filePathNameTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._filePathNameTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._filePathNameTextBox.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._filePathNameTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._filePathNameTextBox.Location = New System.Drawing.Point(0, 0)
        Me._filePathNameTextBox.MaxLength = 0
        Me._filePathNameTextBox.Name = "_filePathNameTextBox"
        Me._filePathNameTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._filePathNameTextBox.Size = New System.Drawing.Size(510, 22)
        Me._filePathNameTextBox.TabIndex = 1
        '
        'FileSelector
        '
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.Controls.Add(Me._filePathNameTextBox)
        Me.Controls.Add(Me._browseButton)
        Me.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FileSelector"
        Me.Size = New System.Drawing.Size(552, 28)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

End Class
