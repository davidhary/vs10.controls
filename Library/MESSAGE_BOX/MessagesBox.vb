Imports System.ComponentModel
Imports isr.Core.EventHandlerExtensions

'''   <summary>A message logging form.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="09/21/02" by="David Hary" revision="1.0.839.x">
''' created.
''' </history>
<System.ComponentModel.Description("Messages List Control"), _
    System.Drawing.ToolboxBitmap(GetType(MessagesBox))> _
Public Class MessagesBox
    Implements System.ComponentModel.INotifyPropertyChanged

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        _resetCount = 100
        _presetCount = 50
        _timeFormat = "HH:mm:ss.fff"
        _delimiter = ", "
        _bullet = "* "
        _usingBullet = True
        _usingTimeBullet = True
        _tabCaption = "Messages"
        _tabCaptionformat = "{0} " & System.Text.Encoding.GetEncoding(437).GetString(New Byte() {240})

        ' set defaults
        Me.BackColor = Drawing.SystemColors.Info

        'Add any initialization after the InitializeComponent() call
        MyBase.ContextMenuStrip = createContextMenuStrip()
        worker = New System.ComponentModel.BackgroundWorker()
        worker.WorkerSupportsCancellation = True
        records = New List(Of String)
        cache = New System.Text.StringBuilder
    End Sub

    ''' <summary>
    ''' Disposes the managed resources.
    ''' </summary>
    Private Sub disposeManagedResources()

        If worker IsNot Nothing Then
            worker.CancelAsync()
            Windows.Forms.Application.DoEvents()
            If Not (worker.IsBusy OrElse worker.CancellationPending) Then
                worker.Dispose()
            End If
        End If

        If records IsNot Nothing Then
            records.Clear()
        End If
        cache = New System.Text.StringBuilder
    End Sub

#End Region

#Region " I NOTIFY PROPERTY CHANGED "

    ''' <summary>
    ''' Occurs when a property value changes.
    ''' </summary>
    Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    ''' <summary>
    ''' Raised to notify the binadable object of a change.
    ''' </summary>
    ''' <param name="name"></param>
    ''' <remarks>
    ''' Includes a work around because for some reason, the dinding wrte value does not occure.
    ''' This is dengerous becuase it could lead to a stack overflow.
    ''' It can be used if the property changed event is raised only if the property changed.
    ''' </remarks>
    Protected Overridable Sub OnPropertyChanged(ByVal name As String)
        isr.Core.EventHandlerExtensions.SafeBeginInvoke(PropertyChangedEvent, Me, New System.ComponentModel.PropertyChangedEventArgs(name))
    End Sub

#End Region

#Region " LIST MANAGER "

    ''' <summary>
    ''' Holds the list of data to display.
    ''' </summary>
    Private records As List(Of String)

    Private cache As System.Text.StringBuilder
    ''' <summary>
    ''' Adds the list item.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="append">if set to <c>true</c> [append].</param>
    Private Sub addListItem(ByVal value As String, ByVal append As Boolean)
        Dim values As String() = value.Split(CChar(Environment.NewLine))
        _isAppend = append
        If values IsNot Nothing AndAlso values.Length > 0 Then
            If append Then
                SyncLock records
                    records.AddRange(values)
                    cache.AppendLine(value)
                End SyncLock
            Else
                SyncLock records
                    records.InsertRange(0, values)
                    cache.Insert(0, Environment.NewLine)
                    cache.Insert(0, value)
                End SyncLock
            End If
            updateNewMessagesCount(Me.NewMessagesCount + values.Length)
        End If
    End Sub

    ''' <summary>
    ''' Adds the list item.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="append">if set to <c>true</c> [append].</param>
    Private Sub addListItem(ByVal value As isr.Core.ExtendedMessage, ByVal append As Boolean)
        addListItem(Me.MessageRecord(value), append)
    End Sub

#End Region

#Region " DISPLAY THE LIST "

    ''' <summary>
    ''' Clears all text from the text box control.
    ''' </summary>
    Public Shadows Sub Clear()
        If MyBase.InvokeRequired Then
            MyBase.Invoke(New Action(AddressOf Clear), New Object() {})
        Else
            If Not MyBase.IsDisposed AndAlso Me.records IsNot Nothing Then
                SyncLock records
                    cache = New System.Text.StringBuilder
                    Me.records.Clear()
                    MyBase.Clear()
                    Me.ClearNewMessagesCount()
                End SyncLock
            End If
        End If
    End Sub

    ''' <summary>
    ''' The background worker is used for setting the messages text box
    ''' in a thread safe way. 
    ''' </summary>
    ''' <remarks></remarks>
    Private WithEvents worker As System.ComponentModel.BackgroundWorker

    ''' <summary>
    ''' This event handler sets the Text property of the TextBox control. It is called on the 
    ''' thread that created the TextBox control, so the call is thread-safe.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub updateDisplay(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles worker.RunWorkerCompleted

        If Not (MyBase.IsDisposed OrElse e.Cancelled OrElse e.Error IsNot Nothing) Then
            If _isAppend Then
                If records.Count > Me._resetCount Then
                    SyncLock records
                        records.RemoveRange(0, records.Count - Me._presetCount)
                        cache = New System.Text.StringBuilder
                        For Each s As String In records
                            cache.AppendLine(s)
                        Next
                    End SyncLock
                End If
            Else
                If records.Count > Me._resetCount Then
                    SyncLock records
                        records.RemoveRange(Me._presetCount, records.Count - Me._presetCount)
                        cache = New System.Text.StringBuilder
                        For Each s As String In records
                            cache.AppendLine(s)
                        Next
                    End SyncLock
                End If
            End If
            MyBase.Text = cache.ToString
            MyBase.Invalidate()
            Windows.Forms.Application.DoEvents()
        End If
    End Sub

    ''' <summary>
    ''' Uses the message worker to flush any queued messages.
    ''' </summary>
    ''' <remarks>
    ''' If the calling thread is different from the thread that created the 
    ''' TextBox control, this method creates a SetTextCallback and calls 
    ''' itself asynchronously using the Invoke method.
    ''' </remarks>
    Public Sub FlushMessages()
        If MyBase.InvokeRequired Then
            MyBase.Invoke(New Action(AddressOf FlushMessages), New Object() {})
        Else
            If Not (MyBase.IsDisposed OrElse Me.worker.IsBusy) Then
                Me.worker.RunWorkerAsync()
                Windows.Forms.Application.DoEvents()
            End If
        End If
    End Sub

#End Region

#Region " METHODS "

#If False Then
  Public Sub ShowResourceNames()
    Dim resouceNames As Array = Me.GetType().Assembly.GetManifestResourceNames
    For Each name As String In resouceNames
      Debug.WriteLine(name)
    Next
  End Sub
#End If

    ''' <summary>
    ''' Prepends or appends a new content to the content queue.
    ''' </summary>
    ''' <param name="content ">Specifies the <see cref="System.String">message</see>.</param>
    ''' <param name="append">True to append or false to push the message.</param>
    ''' <remarks>
    ''' Starts the form's Background Worker by calling RunWorkerAsync. 
    ''' The Text property of the TextBox control is set when the Background Worker
    ''' raises the RunWorkerCompleted event.
    ''' </remarks>
    Public Sub AddContent(ByVal content As String, ByVal append As Boolean)
        Me.addListItem(content, append)
        Me.FlushMessages()
        'Me._isAppend = append
        ' add an item to the queue
        'Me.contentQueue.Enqueue(content)
        'Me.NewMessagesCount += 1
        'Me.FlushContent()
    End Sub

    ''' <summary>
    ''' Prepends or appends a new message to the messages list.
    ''' </summary>
    ''' <param name="condition">True to add the message or false to ignore.</param>
    ''' <param name="newMessage">Specifies the <see cref="isr.Core.ExtendedMessage">message</see>.</param>
    ''' <param name="append">True to append or false to push the message.</param>
    ''' <remarks>
    ''' Starts the form's Background Worker by calling RunWorkerAsync. 
    ''' The Text property of the TextBox control is set when the Background Worker
    ''' raises the RunWorkerCompleted event.
    ''' </remarks>
    Public Sub AddMessage(ByVal condition As Boolean, ByVal newMessage As isr.Core.ExtendedMessage, _
                          ByVal append As Boolean)
        If condition Then
            Me._lastMessage = newMessage
            Me.addListItem(newMessage, append)
            Me.FlushMessages()
            'Me._isAppend = append
            ' add an item to the queue
            'Me.messageQueue.Enqueue(Me._lastMessage)
            'Me.NewMessagesCount += 1
            'Me.FlushMessages()
        End If

    End Sub

    ''' <summary>
    ''' Prepends or appends a new message to the messages list.
    ''' </summary>
    ''' <param name="condition">True to add the message or false to ignore.</param>
    ''' <param name="newMessage">Specifies the message.</param>
    ''' <param name="append">True to append or false to push the message.</param>
    ''' <remarks>
    ''' Starts the form's Background Worker by calling RunWorkerAsync. 
    ''' The Text property of the TextBox control is set when the Background Worker
    ''' raises the RunWorkerCompleted event.
    ''' </remarks>
    Public Sub AddMessage(ByVal condition As Boolean, ByVal newMessage As String, ByVal append As Boolean)
        If condition Then
            Me.AddMessage(condition, New isr.Core.ExtendedMessage(newMessage), append)
        End If
    End Sub

    ''' <summary>Appends a new message to the message list.</summary>
    ''' <param name="newMessage">Specifies the message which to add.</param>
    ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
    Public Sub AppendMessage(ByVal newMessage As String)
        Me.AddMessage(True, newMessage, True)
    End Sub

    ''' <summary>Appends a message if the condition is met.</summary>
    ''' <param name="condition">True to log.  Use the application trace switch
    '''   to identify the condition such as TraceSwitch.TraceError meaning that
    '''   the condition is true if the TraceSwitch level is at error, info, warning, 
    '''   or verbose.</param>
    ''' <param name="newMessage">Specifies the message which to add.</param>
    Public Sub AppendMessageIf(ByVal condition As Boolean, ByVal newMessage As String)
        Me.AddMessage(condition, newMessage, True)
    End Sub

    ''' <summary>Returns a message record. This consists of a prefix (bullet or time stamp),
    ''' Trace level, synopsis, and details separated with a delimiter.</summary>
    Private Function MessageRecord(ByVal message As isr.Core.ExtendedMessage) As String

        Dim prefix As String = String.Empty
        If _usingBullet Then
            If _usingTimeBullet Then
                prefix = message.Timestamp.ToString(_timeFormat, System.Globalization.CultureInfo.CurrentCulture) & _delimiter
            Else
                prefix = Me._bullet
            End If
        End If

        Dim traceLevelCode As String = String.Empty
        If Me._usingTraceLevel Then
            traceLevelCode = message.TraceLevel.ToString.Substring(0, 1) & _delimiter
        End If

        Dim details As String
        If Me._usingSynopsis Then
            details = String.Format(System.Globalization.CultureInfo.CurrentCulture, "{1}{0}{2}", _delimiter, _
                  message.Synopsis, message.Details)
        Else
            details = message.Details
        End If

        Return String.Format(System.Globalization.CultureInfo.CurrentCulture, "{0}{1}{2}", _
            prefix, traceLevelCode, details)

    End Function

    ''' <summary>Adds an error message to the top of the message box.</summary>
    ''' <param name="value">The <see cref="System.Exception">error</see> to add.</param>
    Public Sub PrependException(ByVal value As Exception)

        If Not String.IsNullOrEmpty(value.Message) Then

            If value.InnerException IsNot Nothing Then
                Me.PrependException(value.InnerException)
            End If

            Dim messageBuilder As New System.Text.StringBuilder
            messageBuilder.Append(value.Message)
            If value IsNot Nothing AndAlso value.StackTrace IsNot Nothing Then
                messageBuilder.AppendLine()
                messageBuilder.Append("Stack Trace ")
                messageBuilder.AppendLine()
                messageBuilder.Append(value.StackTrace)
#If False Then
          Dim stackTrace As String() = value.StackTrace.Split(CChar(Environment.NewLine))
          For Each stackElement As String In stackTrace
            messageBuilder.AppendLine()
            messageBuilder.Append(stackElement)
          Next
#End If
            End If
            If value.Data IsNot Nothing AndAlso value.Data.Count > 0 Then
                messageBuilder.AppendLine()
                messageBuilder.Append("Data: ")
                For Each keyValuePair As System.Collections.DictionaryEntry In value.Data
                    messageBuilder.AppendLine()
                    messageBuilder.Append(keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString)
                Next
            End If

            Me.AddMessage(True, New isr.Core.ExtendedMessage(messageBuilder.ToString), False)

        End If

    End Sub

    ''' <summary>Adds an exception message on top of the message list.</summary>
    ''' <param name="details">Specifies the message which to add.</param>
    ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
    Public Sub PrependException(ByVal details As String)
        Me.AddMessage(True, New isr.Core.ExtendedMessage(TraceEventType.Error, details), False)
    End Sub

    ''' <summary>Add message on top of the message list.</summary>
    ''' <param name="newMessage">Specifies the message which to add.</param>
    ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
    Public Sub PrependMessage(ByVal newMessage As String)
        Me.AddMessage(True, newMessage, False)
    End Sub

    ''' <summary>Add message on top of the message list.</summary>
    ''' <param name="details">Specifies the message which to add.</param>
    ''' <param name="traceLevel">Specifies the message trace level.</param>
    ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
    Public Sub PrependMessage(ByVal traceLevel As Diagnostics.TraceEventType, ByVal details As String)
        Me.AddMessage(True, New isr.Core.ExtendedMessage(traceLevel, details), False)
    End Sub

    ''' <summary>Add message on top of the message list.</summary>
    ''' <param name="traceLevel">Specifies the message trace level.</param>
    ''' <param name="format">Specifies the message format.</param>
    ''' <param name="args">Specifies the format arguments.</param>
    ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
    Public Sub PrependMessage(ByVal traceLevel As Diagnostics.TraceEventType, ByVal format As String, ByVal ParamArray args() As Object)
        Me.AddMessage(True, New isr.Core.ExtendedMessage(traceLevel, "", format, args), False)
    End Sub

    ''' <summary>Add message on top of the message list.</summary>
    ''' <param name="newMessage">Specifies the message which to add.</param>
    ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
    Public Sub PrependMessage(ByVal newMessage As isr.Core.ExtendedMessage)
        Me.AddMessage(True, newMessage, False)
    End Sub

    ''' <summary>Add message on top of the message list if the condition is met.</summary>
    ''' <param name="condition">Specifies True to push the message onto the 
    '''   message list.  Use the application trace switch to identify the condition 
    '''   such as TraceSwitch.TraceError meaning that the condition is true if the 
    '''   TraceSwitch level is at error, info, warning, or verbose.</param>
    ''' <param name="newMessage">Specifies the message which to add.</param>
    Public Sub PrependMessageIf(ByVal condition As Boolean, ByVal newMessage As String)
        Me.AddMessage(condition, newMessage, False)
    End Sub

    ''' <summary>Add message on top of the message list.</summary>
    ''' <param name="newMessage">Specifies the message which to add.</param>
    ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
    Public Sub PushMessage(ByVal newMessage As String)
        Me.AddMessage(True, newMessage, False)
    End Sub

    ''' <summary>Add message on top of the message list if the condition is met.</summary>
    ''' <param name="condition">Specifies True to push the message onto the 
    '''   message list.  Use the application trace switch to identify the condition 
    '''   such as TraceSwitch.TraceError meaning that the condition is true if the 
    '''   TraceSwitch level is at error, info, warning, or verbose.</param>
    ''' <param name="newMessage">Specifies the message which to add.</param>
    Public Sub PushMessageIf(ByVal condition As Boolean, ByVal newMessage As String)
        Me.AddMessage(condition, newMessage, False)
    End Sub

#End Region

#Region " BULLET, TRACE LEVEL, DELIMITER "

    Private _delimiter As String
    ''' <summary>Gets or sets the Delimiter to use for prefixing messages.</summary>
    ''' <value><c>Delimiter</c>is a <see cref="System.String">String</see> property.</value>
    <Category("Appearance"), Description("Delimited for message data"), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue(", ")> _
    Public Property Delimiter() As String
        Get
            Return _delimiter
        End Get
        Set(ByVal Value As String)
            If Me.Delimiter <> Value Then
                _delimiter = Value
                Me.OnPropertyChanged("Delimiter")
            End If
        End Set
    End Property

    Private _bullet As String
    ''' <summary>Gets or sets the bullet to use for prefixing messages.</summary>
    ''' <value><c>Bullet</c>is a <see cref="System.String">String</see> property.</value>
    <Category("Appearance"), Description("Bullet for prefixing messages when turning off time prefix."), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue("* ")> _
    Public Property Bullet() As String
        Get
            Return _bullet
        End Get
        Set(ByVal Value As String)
            If Me.Bullet <> Value Then
                _bullet = Value
                Me.OnPropertyChanged("Bullet")
            End If
        End Set
    End Property

    Private _usingBullet As Boolean
    ''' <summary>Gets or sets the condition for each message is prefixed.
    ''' This would be the <see cref="Bullet"/> unless <see cref="UsingTimeBullet"/> is
    ''' True. Default is True.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see>.</value>
    <Category("Appearance"), Description("Use a time or bullet prefix."), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue(True)> _
    Public Property UsingBullet() As Boolean
        Get
            Return _usingBullet
        End Get
        Set(ByVal Value As Boolean)
            If Me.UsingBullet <> Value Then
                _usingBullet = Value
                Me.OnPropertyChanged("UsingBullet")
            End If
        End Set
    End Property

    Private _usingSynopsis As Boolean
    ''' <summary>Gets or sets the condition for each message includes the synopsys.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see>.</value>
    <Category("Appearance"), Description("Include a synopsis in addition to the message details."), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue(False)> _
    Public Property UsingSynopsis() As Boolean
        Get
            Return _usingSynopsis
        End Get
        Set(ByVal Value As Boolean)
            If Me.UsingSynopsis <> Value Then
                _usingSynopsis = Value
                Me.OnPropertyChanged("UsingSynopsis")
            End If
        End Set
    End Property

    Private _usingTimeBullet As Boolean
    ''' <summary>Gets or sets the condition for each message is prefixed with a time format.
    '''   Default is True.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see>.</value>
    <Category("Appearance"), Description("Use a time prefix."), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue(True)> _
    Public Property UsingTimeBullet() As Boolean
        Get
            Return _usingTimeBullet
        End Get
        Set(ByVal Value As Boolean)
            If Me.UsingTimeBullet <> Value Then
                _usingTimeBullet = Value
                Me.OnPropertyChanged("UsingTimeBullet")
            End If
        End Set
    End Property

    Private _usingTraceLevel As Boolean
    ''' <summary>Gets or sets the condition for each message is prefixed with a trace level code.
    ''' Default is False.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see>.</value>
    <Category("Appearance"), Description("Display the trace level as part of the prefix or bullet."), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue(False)> _
    Public Property UsingTraceLevel() As Boolean
        Get
            Return _usingTraceLevel
        End Get
        Set(ByVal Value As Boolean)
            If Me.UsingTraceLevel <> Value Then
                _usingTraceLevel = Value
                Me.OnPropertyChanged("UsingTraceLevel")
            End If
        End Set
    End Property

#End Region

#Region " TAB CAPTION "

    Private _hasMessagesChanged As Boolean
    ''' <summary>
    ''' Indicates if exisitng messages cleared or messages added to a cleared box.
    ''' This can be used to toggle the message available indicator in place of displaying the messages count.
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property HasMessagesChanged1() As Boolean
        Get
            Return _hasMessagesChanged
        End Get
    End Property


    ''' <summary>
    ''' Clears the new messages count.
    ''' Use from the messages tab to indicate that the messages were observed.
    ''' </summary>
    Public Sub ClearNewMessagesCount()
        updateNewMessagesCount(0)
    End Sub

    ''' <summary>
    ''' Updates the new messages count.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Private Sub updateNewMessagesCount(ByVal value As Integer)
        If _newMessagesCount <> value Then
            _newMessagesCount = value
            Me.OnPropertyChanged("NewMessagesCount")
            '      Me.OnMessageAdded()
        End If
#If False Then
    If (_newMessagesCount <> value) OrElse (value = 0) Then
      If _newMessagesCount = 0 Then
        _hasMessagesChanged = value <> 0
      Else
        _hasMessagesChanged = value = 0
      End If
    End If
#End If
    End Sub

    Private _newMessagesCount As Integer
    ''' <summary>Gets or sets the number of new messages that were not yet viewed.</summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public ReadOnly Property NewMessagesCount() As Integer
        Get
            Return _newMessagesCount
        End Get
    End Property

    Private _tabCaption As String
    ''' <summary>Gets or sets the tab caption.</summary>
    <Category("Appearance"), Description("Default title for the parent tab"), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue("Messages")> _
    Public Property TabCaption() As String
        Get
            Return _tabCaption
        End Get
        Set(ByVal Value As String)
            If _tabCaption <> Value Then
                _tabCaption = Value
                Me.OnPropertyChanged("TabCaption")
                ' Me.OnMessageAdded()
            Else
                _tabCaption = Value
                Me.OnPropertyChanged("TabCaption")
            End If
        End Set
    End Property

    Private _tabCaptionformat As String
    ''' <summary>Gets or sets the tab caption format.</summary>
    <Category("Appearance"), Description("Formats the tab caption with number of new messages"), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue("{0} ≡")> _
    Public Property TabCaptionFormat() As String
        Get
            Return _tabCaptionformat
        End Get
        Set(ByVal Value As String)
            If _tabCaptionformat <> Value Then
                _tabCaptionformat = Value
                Me.OnPropertyChanged("TabCaptionFormat")
            End If
        End Set
    End Property

    ''' <summary>Returns a caption based on the value plus the number of new messges.</summary>
    Public Function BuildTabCaption() As String
        Return Me.BuildTabCaption(_tabCaptionformat)
    End Function

    ''' <summary>Returns a caption based on the value plus the number of new messges.</summary>
    Public Function BuildTabCaption(ByVal captionFormat As String) As String
        If _newMessagesCount = 0 Then
            Return _tabCaption
        Else
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _
                                            captionFormat, _tabCaption, _newMessagesCount)
        End If
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Holds the append mode.  If true, messages are appended to the end of the 
    ''' messages box.  Otherwise, messages are pushed onto the top of the message box.
    ''' </summary>
    Private _isAppend As Boolean

    Private _lastMessage As isr.Core.ExtendedMessage
    ''' <summary>Gets or sets the last message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public ReadOnly Property LastMessage() As isr.Core.ExtendedMessage
        Get
            Return _lastMessage
        End Get
    End Property

    Private _resetCount As Integer
    ''' <summary>Gets or sets the reset count.</summary>
    ''' <value><c>ResetSize</c>is an integer property.</value>
    ''' <remarks>The message list gets reset to the preset count when the
    '''   message count exceeds the reset count.</remarks>
    <Category("Appearance"), Description("Number of lines aty which to reset"), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue("100")> _
    Public Property ResetCount() As Integer
        Get
            Return _resetCount
        End Get
        Set(ByVal value As Integer)
            If Me.ResetCount <> value Then
                _resetCount = value
                Me.OnPropertyChanged("ResetCount")
            End If
        End Set
    End Property

    Private _presetCount As Integer
    ''' <summary>Gets or sets the preset count.</summary>
    ''' <value><c>PresetSize</c>is an integer property.</value>
    ''' <remarks>The message list gets reset to the preset count when the
    '''   message count exceeds the reset count.</remarks>
    <Category("Appearance"), Description("Number of lines to reset to"), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue("50")> _
    Public Property PresetCount() As Integer
        Get
            Return _presetCount
        End Get
        Set(ByVal value As Integer)
            If Me.PresetCount <> value Then
                _presetCount = value
                Me.OnPropertyChanged("PresetCount")
            End If
        End Set
    End Property

    Private _timeFormat As String
    ''' <summary>Gets or sets the format for displaying the time prefix, such as 'HH:mm:ss.f'.</summary>
    ''' <value><c>TimeFormat</c>is a <see cref="System.String">String</see> property.</value>
    <Category("Appearance"), Description("Formats the time prefix"), _
      Browsable(True), _
      DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
      DefaultValue("HH:mm:ss.fff")> _
    Public Property TimeFormat() As String
        Get
            Return _timeFormat
        End Get
        Set(ByVal Value As String)
            If Me.TimeFormat <> Value Then
                ' try the time format to raise an error if not set correctly.
                Date.Now.ToString(Value, System.Globalization.CultureInfo.CurrentCulture)
                _timeFormat = Value
                Me.OnPropertyChanged("TimeFormat")
            End If
        End Set
    End Property

#End Region

#Region " CONTEXT MENU STRIP "

    ''' <summary>
    ''' Creates a context menu strip.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createContextMenuStrip() As Windows.Forms.ContextMenuStrip

        ' Create a new ContextMenuStrip control.
        Dim myContextMenuStrip As Windows.Forms.ContextMenuStrip = New Windows.Forms.ContextMenuStrip()

        ' Attach an event handler for the 
        ' ContextMenuStrip control's Opening event.
        AddHandler myContextMenuStrip.Opening, AddressOf contectMenuOpeningHandler

        Return myContextMenuStrip

    End Function

    ''' <summary>
    ''' Adds menu items.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' This event handler is invoked when the <see cref="ContextMenuStrip"/> control's 
    ''' Opening event is raised. 
    ''' </remarks>
    Private Sub contectMenuOpeningHandler(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

        Dim myContextMenuStrip As Windows.Forms.ContextMenuStrip = CType(sender, Windows.Forms.ContextMenuStrip)

        ' Clear the ContextMenuStrip control's Items collection.
        myContextMenuStrip.Items.Clear()

        ' Populate the ContextMenuStrip control with its default items.
        ' myContextMenuStrip.Items.Add("-")
        myContextMenuStrip.Items.Add(New Windows.Forms.ToolStripMenuItem("Clear &All", Nothing, _
            AddressOf clearAllHandler, "Clear"))
        myContextMenuStrip.Items.Add(New Windows.Forms.ToolStripMenuItem("Flush &Queue", Nothing, _
            AddressOf flushQueuesHandler, "Flush"))

        ' Set Cancel to false. 
        ' It is optimized to true based on empty entry.
        e.Cancel = False

    End Sub

    ''' <summary>Applies the high point Output.</summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub clearAllHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Clear()
    End Sub

    ''' <summary>
    ''' Flush messages and content.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub flushQueuesHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Me.FlushContent()
        Me.FlushMessages()
    End Sub

#End Region

End Class

#Region " REMOVED "
#If False Then

  ''' <summary>
  ''' Holds the queue of text.  The queue gets filled with content each time
  ''' the worker is busy.
  ''' </summary>
  ''' <remarks></remarks>
  Private contentQueue As System.Collections.Generic.Queue(Of String)

    contentWorker = New System.ComponentModel.BackgroundWorker()
    contentWorker.WorkerSupportsCancellation = True
    messageQueue = New System.Collections.Generic.Queue(Of isr.Core.ExtendedMessage)()
    contentQueue = New System.Collections.Generic.Queue(Of String)

    If contentWorker IsNot Nothing Then
      contentWorker.CancelAsync()
      Windows.Forms.Application.DoEvents()
      If Not (contentWorker.IsBusy OrElse contentWorker.CancellationPending) Then
        contentWorker.Dispose()
        If Me.contentQueue IsNot Nothing Then
          Me.contentQueue.Clear()
          Me.contentQueue = Nothing
        End If
      End If
    End If

#Region " EVENTS "

  ''' <summary>
  ''' Occurs when a new message is added.
  ''' </summary>
  ''' <param name="e">Specifies the <see cref="EventArgs">event arguments</see>.</param>
  Public Event MessageAdded1 As EventHandler(Of EventArgs)

  ''' <summary>
  ''' Occurs when a new message is added.  Adds tthe message to the message
  ''' count and returns a new caption.
  ''' </summary>
  Public Sub OnMessageAdded1()
    MessageAdded1Event.SafeBeginInvoke(Me)
  End Sub

#End Region

#Region " CONTENT QUEUE: THREAD SAFE DISPLAY "

  ''' <summary>
  ''' The content background worker is used for setting the messages text box
  ''' in a thread safe way. 
  ''' </summary>
  ''' <remarks></remarks>
  Private WithEvents contentWorker As System.ComponentModel.BackgroundWorker

  ''' <summary>
  ''' This event handler sets the Text property of the TextBox control. It is called on the 
  ''' thread that created the TextBox control, so the call is thread-safe.
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub updateContent(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) _
      Handles contentWorker.RunWorkerCompleted

    If Not (MyBase.IsDisposed OrElse e.Cancelled OrElse e.Error IsNot Nothing) Then
      Do While Me.contentQueue IsNot Nothing AndAlso Me.contentQueue.Count > 0

        Dim message As String = Me.contentQueue.Dequeue
        If Me._isAppend Then
          MyBase.Text = String.Format(System.Globalization.CultureInfo.CurrentCulture, _
                "{0}{1}{2}", MyBase.Text, Environment.NewLine, message)
          If MyBase.Lines.Length > Me._resetCount Then
            Array.Reverse(MyBase.Lines)
            ReDim Preserve MyBase.Lines(Me._presetCount)
            Array.Reverse(MyBase.Lines)
          End If
        Else
          MyBase.Text = String.Format(System.Globalization.CultureInfo.CurrentCulture, _
                "{0}{1}{2}", message, Environment.NewLine, MyBase.Text)
          If MyBase.Lines.Length > Me._resetCount Then
            ReDim Preserve MyBase.Lines(Me._presetCount)
          End If
        End If

        MyBase.Invalidate()
        Windows.Forms.Application.DoEvents()

      Loop

    End If

  End Sub

  ''' <summary>
  ''' Uses the content worker to flush any queued content.
  ''' Execute the worker thread and makes a thread-safe call on the 
  ''' user interface.
  ''' </summary>
  ''' <remarks>
  ''' If the calling thread is different from the thread that created the 
  ''' TextBox control, this method creates a SetTextCallback and calls 
  ''' itself asynchronously using the Invoke method.
  ''' </remarks>
  Public Sub FlushContent1()
    If MyBase.InvokeRequired Then
      MyBase.Invoke(New Action(AddressOf FlushContent1), New Object() {})
    Else
      If Not (MyBase.IsDisposed OrElse Me.contentWorker.IsBusy) Then
        Me.contentWorker.RunWorkerAsync()
        Windows.Forms.Application.DoEvents()
      End If
    End If
  End Sub

#End Region

#Region " MESSAGE QUEUE:  THREAD SAFE DISPLAY "

  ''' <summary>
  ''' Holds the queue of messages.  The queue gets filled with messages each time
  ''' the worker is busy.
  ''' </summary>
  ''' <remarks></remarks>
  Private messageQueue As System.Collections.Generic.Queue(Of isr.Core.ExtendedMessage)

  ''' <summary>
  ''' This event handler sets the Text property of the TextBox control. It is called on the 
  ''' thread that created the TextBox control, so the call is thread-safe.
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub worker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)

    If Not (MyBase.IsDisposed OrElse e.Cancelled OrElse e.Error IsNot Nothing) Then
      Do While Me.messageQueue IsNot Nothing AndAlso Me.messageQueue.Count > 0
        Dim message As isr.Core.ExtendedMessage = Me.messageQueue.Dequeue
        If Me._isAppend Then
          MyBase.Text = String.Format(System.Globalization.CultureInfo.CurrentCulture, _
                "{0}{1}{2}", MyBase.Text, Environment.NewLine, Me.MessageRecord(message))
          If MyBase.Lines.Length > Me._resetCount Then
            Array.Reverse(MyBase.Lines)
            ReDim Preserve MyBase.Lines(Me._presetCount)
            Array.Reverse(MyBase.Lines)
          End If
        Else
          MyBase.Text = String.Format(System.Globalization.CultureInfo.CurrentCulture, _
                "{0}{1}{2}", Me.MessageRecord(message), Environment.NewLine, MyBase.Text)
          If MyBase.Lines.Length > Me._resetCount Then
            ReDim Preserve MyBase.Lines(Me._presetCount)
          End If
        End If

        MyBase.Invalidate()
        Windows.Forms.Application.DoEvents()

      Loop
    End If
  End Sub

  ''' <summary>
  ''' Uses the message worker to flush any queued messages.
  ''' </summary>
  ''' <remarks>
  ''' If the calling thread is different from the thread that created the 
  ''' TextBox control, this method creates a SetTextCallback and calls 
  ''' itself asynchronously using the Invoke method.
  ''' </remarks>
  Public Sub FlushMessages1()
    If MyBase.InvokeRequired Then
      MyBase.Invoke(New Action(AddressOf FlushMessages1), New Object() {})
    Else
      If Not (MyBase.IsDisposed OrElse Me.worker.IsBusy) Then
        Me.worker.RunWorkerAsync()
        Windows.Forms.Application.DoEvents()
      End If
    End If
  End Sub

#End Region


#End If
#End Region