''' <summary>Provides a test platform for the Flat Toggle Button.</summary>
''' <license>
''' (c) 2007 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="09/08/07" by="David Hary" revision="1.0.2807.x">
''' Created
''' </history>
Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ToggleSelector1 As isr.Controls.FlatToggleButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents _htmlRichTextBox As System.Windows.Forms.RichTextBox
    Friend WithEvents PushButton1 As isr.Controls.PushButton
    Friend WithEvents _xmlRichTextBox As System.Windows.Forms.RichTextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me._htmlRichTextBox = New System.Windows.Forms.RichTextBox
        Me._xmlRichTextBox = New System.Windows.Forms.RichTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.PushButton1 = New isr.Controls.PushButton
        Me.ToggleSelector1 = New isr.Controls.FlatToggleButton
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        '_htmlRichTextBox
        '
        Me._htmlRichTextBox.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me._htmlRichTextBox.Location = New System.Drawing.Point(8, 16)
        Me._htmlRichTextBox.Name = "_htmlRichTextBox"
        Me._htmlRichTextBox.Size = New System.Drawing.Size(128, 184)
        Me._htmlRichTextBox.TabIndex = 0
        Me._htmlRichTextBox.Text = "<html>  " & Global.Microsoft.VisualBasic.ChrW(10) & "    <head> " & Global.Microsoft.VisualBasic.ChrW(10) & "        <title>HTMLSample</title>  " & Global.Microsoft.VisualBasic.ChrW(10) & "    </head> " & Global.Microsoft.VisualBasic.ChrW(10) & "    <body> " & _
            "" & Global.Microsoft.VisualBasic.ChrW(10) & "        HTML Sample demonstrate the use of the  <b>.NET Toggle Control</b>. " & Global.Microsoft.VisualBasic.ChrW(10) & " <" & _
            "/body>" & Global.Microsoft.VisualBasic.ChrW(10) & "</html> "
        '
        '_xmlRichTextBox
        '
        Me._xmlRichTextBox.BackColor = System.Drawing.SystemColors.ScrollBar
        Me._xmlRichTextBox.Location = New System.Drawing.Point(144, 16)
        Me._xmlRichTextBox.Name = "_xmlRichTextBox"
        Me._xmlRichTextBox.Size = New System.Drawing.Size(128, 184)
        Me._xmlRichTextBox.TabIndex = 1
        Me._xmlRichTextBox.Text = "<Loan>  " & Global.Microsoft.VisualBasic.ChrW(10) & "    <LoanAmount> 1111</LoanAmount> " & Global.Microsoft.VisualBasic.ChrW(10) & "    <InterestRate>2222</InterestRate" & _
            ">  " & Global.Microsoft.VisualBasic.ChrW(10) & "    <Term>3333</Term> " & Global.Microsoft.VisualBasic.ChrW(10) & "</Loan> "
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me._xmlRichTextBox)
        Me.Panel1.Controls.Add(Me._htmlRichTextBox)
        Me.Panel1.Location = New System.Drawing.Point(8, 16)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(280, 208)
        Me.Panel1.TabIndex = 1
        '
        'PushButton1
        '
        Me.PushButton1.BackColor = System.Drawing.Color.Transparent
        Me.PushButton1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PushButton1.ButtonText = "HTM&L/XML"
        Me.PushButton1.Caption = "XML"
        Me.PushButton1.CaptionDockStyle = System.Windows.Forms.DockStyle.Right
        Me.PushButton1.CaptionWidth = 60
        Me.PushButton1.CheckedCaption = "XML"
        Me.PushButton1.Location = New System.Drawing.Point(144, 230)
        Me.PushButton1.Name = "PushButton1"
        Me.PushButton1.Size = New System.Drawing.Size(142, 30)
        Me.PushButton1.TabIndex = 2
        Me.PushButton1.UncheckedCaption = "XML"
        '
        'ToggleSelector1
        '
        Me.ToggleSelector1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ToggleSelector1.FlatButton1ImageIndex = 0
        Me.ToggleSelector1.FlatButton1Text = "&HTML"
        Me.ToggleSelector1.FlatButton2ImageIndex = 1
        Me.ToggleSelector1.FlatButton2Text = "&XML"
        Me.ToggleSelector1.Location = New System.Drawing.Point(9, 234)
        Me.ToggleSelector1.Name = "ToggleSelector1"
        Me.ToggleSelector1.Size = New System.Drawing.Size(118, 20)
        Me.ToggleSelector1.TabIndex = 0
        Me.ToggleSelector1.UseMnemonic = True
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.PushButton1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ToggleSelector1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

#End Region

#Region " PROPERTIES "

    Private _statusMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    Public ReadOnly Property StatusMessage() As String
        Get
            Return _statusMessage
        End Get
    End Property

    ''' <summary>Returns true if an instance of the class was created and not disposed.</summary>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            Return My.Application.OpenForms.Count > 0 AndAlso _
                My.Application.OpenForms.Item(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name) IsNot Nothing
        End Get
    End Property

#End Region

#Region " EVENT HANDLERS "

#End Region

    Private Sub ToggleDisplay(ByVal value As Boolean)

        If value Then

            ' Display the correct control in the given container
            If Me.Panel1 IsNot Nothing Then
                Me.Panel1.Controls.Remove(Me._xmlRichTextBox)
                Me.Panel1.Controls.Add(Me._htmlRichTextBox)
            End If
            Me._htmlRichTextBox.Refresh()
            Me.Panel1.Refresh()

        Else

            'Display the correct control in the given container
            If Me.Panel1 IsNot Nothing Then
                Me.Panel1.Controls.Remove(Me._htmlRichTextBox)
                Me.Panel1.Controls.Add(Me._xmlRichTextBox)
            End If
            Me._xmlRichTextBox.Refresh()
            Me.Panel1.Refresh()

        End If

    End Sub

    Private Sub PushButton1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PushButton1.CheckedChanged
        ToggleDisplay(PushButton1.Checked)
    End Sub

    ''' <summary>
    ''' Displays to proper control.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ToggleSelector1_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToggleSelector1.ButtonClick

        ToggleDisplay(ToggleSelector1.IsButton1Selected)

    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        ' If we are not in design mode then add the controls to our container
        ' and have them fill the whole container
        If Not Me.DesignMode Then
            If Me._htmlRichTextBox IsNot Nothing Then
                Me.Panel1.Controls.Add(Me._htmlRichTextBox)
                Me._htmlRichTextBox.Dock = DockStyle.Fill
            End If
            If Me._xmlRichTextBox IsNot Nothing Then
                Me.Panel1.Controls.Add(Me._xmlRichTextBox)
                Me._xmlRichTextBox.Dock = DockStyle.Fill
            End If
        End If
        MyBase.OnLoad(e)
    End Sub

End Class
