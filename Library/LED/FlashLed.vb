Imports System.ComponentModel
'''   <summary>A flashing LED Control.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="09/04/07" by="Horia Tudosie" revision="1.0.2803.x">
''' http://www.codeproject.com/cs/miscctrl/FlashLED.asp
''' </history>
''' <history date="09/05/07" by="David Hary" revision="1.0.2804.x">
''' Add Flush Color property to use for flush mode.  Restore Active color when ending Flush mode.
''' </history>
''' <history date="09/04/07" by="Horia Tudosie" revision="1.0.2803.x">
''' created.
''' </history>
<System.ComponentModel.Description("Flashing LED Control"), _
    System.Drawing.ToolboxBitmap(GetType(FlashLed))> _
Public Class FlashLed

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        SetStyle(ControlStyles.DoubleBuffer, True)
        SetStyle(ControlStyles.UserPaint, True)
        SetStyle(ControlStyles.ResizeRedraw, True)

        ' These too lines are required to make the control transparent.
        SetStyle(ControlStyles.SupportsTransparentBackColor, True)
        _flashColor = Color.Red
        '_flash = False
        _flashColorsArray = New Color(4) {Color.Red, Color.Empty, Color.Yellow, Color.Empty, Color.Blue}
        _FlashColors = "Red,,Yellow,,Blue"
        _flashIntervalsArray = New Integer(5) {500, 250, 500, 250, 500, 250}
        _flashIntervals = "500,250,500,250,500,250"
        _sparklePenWidth = 2
        Me.Active = True
        Me.BackColor = Color.Transparent
        Me.OffColor = SystemColors.Control
        Me.OnColor = Color.Red

        ' Set the Width and Height to an odd number for the convenience of having a pixel in the centre 
        ' of the control. 17 is a good size for a perfect circle for a circular LED.
        'Width = 17
        'Height = 17

        tick = New Timer()
        tick.Enabled = False
        AddHandler tick.Tick, AddressOf _Tick

    End Sub

#End Region

#Region " PROPERTIES "

    Private _active As Boolean
    ''' <summary>
    ''' Turns on the <see cref="OnColor">on color</see> when True or
    ''' the <see cref="OffColor">off color</see> when False.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Behavior"), DefaultValue(True)> _
    Public Property Active() As Boolean
        Get
            Return _active
        End Get
        Set(ByVal value As Boolean)
            _active = value
            Invalidate()
        End Set
    End Property

    Private _flash As Boolean
    ''' <summary>
    ''' Gets or sets the Flash condition.  When True, the Led flashes.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Behavior"), DefaultValue(False)> _
    Public Property Flash() As Boolean
        Get
            Return _flash
        End Get
        Set(ByVal value As Boolean)
            _flash = value ' AndAlso (_flashIntervalsArray.Length > 0)
            tickIndex = 0
            tick.Interval = _flashIntervalsArray(tickIndex)
            tick.Enabled = _flash
            Me.Active = Me.Active
        End Set
    End Property

    Private _flashColor As Color
    ''' <summary>
    ''' Gets or sets the Flash color.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Appearance")> _
    Public Property FlashColor() As Color
        Get
            Return _flashColor
        End Get
        Set(ByVal value As Color)
            _flashColor = value
            Invalidate()
        End Set
    End Property

    ''' <summary>
    ''' Holds the array of flash colors in case a set of colors is used.
    ''' </summary>
    ''' <remarks></remarks>
    Private _flashColorsArray As Color()

    Private _FlashColors As String
    ''' <summary>
    ''' Gets or sets the set of flash colors
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>
    ''' Accepts a delimited string that is converted to a color array for setting the flash color.
    ''' Any reasonable delimiter will break the input string, and error items will default to 
    ''' Color.Empty for colors that will switch the LED Off.
    ''' </remarks>
    <Category("Appearance"), DefaultValue("Red,,Yellow,,Blue")> _
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Public Property FlashColors() As String
        Get
            Return _FlashColors
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                _FlashColors = String.Empty
            Else
                _FlashColors = value
            End If
            If String.IsNullOrEmpty(_FlashColors) Then
                _flashColorsArray = Nothing
            Else
                Dim fc As String() = _FlashColors.Split(New Char() {","c, "/"c, "|"c, " "c, Environment.NewLine.ToCharArray()(0), Environment.NewLine.ToCharArray()(1)})
                _flashColorsArray = New Color(fc.Length - 1) {}
                For i As Integer = 0 To fc.Length - 1
                    Try
                        If (fc(i) <> "") Then
                            _flashColorsArray(i) = Color.FromName(fc(i))
                        Else
                            _flashColorsArray(i) = Color.Empty
                        End If
                    Catch
                        _flashColorsArray(i) = Color.Empty
                    End Try
                Next i
            End If
        End Set
    End Property

    ''' <summary>
    ''' Holds the array of flash intervals.
    ''' </summary>
    ''' <remarks></remarks>
    Private _flashIntervalsArray As Integer()

    Private _flashIntervals As String
    ''' <summary>
    ''' Gets or sets the set of flash intervals
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>
    ''' Accepts a delimited string that is converted to a interval array for setting the flash intervals.
    ''' Any reasonable delimiter will break the input string, and error items will default to 
    ''' 25 ms for the interval.
    ''' </remarks>
    <Category("Appearance"), DefaultValue("500,250,500,250,500,250")> _
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Public Property FlashIntervals() As String
        Get
            Return _flashIntervals
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                _flashIntervals = String.Empty
            Else
                _flashIntervals = value
            End If
            If String.IsNullOrEmpty(_flashIntervals) Then
                _flashIntervalsArray = Nothing
            Else
                Dim fi As String() = _flashIntervals.Split(New Char() {","c, "/"c, "|"c, " "c, Environment.NewLine.ToCharArray()(0), Environment.NewLine.ToCharArray()(1)})
                _flashIntervalsArray = New Integer(fi.Length - 1) {}
                For i As Integer = 0 To fi.Length - 1
                    Try
                        _flashIntervalsArray(i) = Integer.Parse(fi(i), Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture)
                    Catch
                        _flashIntervalsArray(i) = 25
                    End Try
                Next i
            End If
        End Set
    End Property

    Private _offColor As Color
    ''' <summary>
    ''' Gets or sets the OFF color.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Appearance")> _
    Public Property OffColor() As Color
        Get
            Return _offColor
        End Get
        Set(ByVal value As Color)
            _offColor = value
            Invalidate()
        End Set
    End Property

    Private _onColor As Color
    ''' <summary>
    ''' Gets or sets the ON color.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Appearance")> _
    Public Property OnColor() As Color
        Get
            Return _onColor
        End Get
        Set(ByVal value As Color)
            _onColor = value
            Invalidate()
        End Set
    End Property

#End Region

#Region " HELPER COLOR FUNCTIONS "

    ''' <summary>
    ''' Blends the <para>firstColor</para> over the <para>secondColor</para> with 
    ''' a weighted blend of <para>firstWeight</para> and <para>secondWeight</para>. 
    ''' </summary>
    ''' <param name="firstColor"></param>
    ''' <param name="secondColor"></param>
    ''' <param name="firstWeight"></param>
    ''' <param name="secondWeight"></param>
    ''' <returns></returns>
    ''' <remarks>
    ''' The function works by splitting the two colors in R, G and B components, applying the ratio,
    ''' and returning the recomposed color. This function blends the LED color in the margin of the 
    ''' bubble, and adds the sparkle that will give the bubble's volume. When the LED is On, the
    ''' margin is enlightened with White, while - when Off is darkened with Black. The same for the 
    ''' sparkle, but with different ratios.
    ''' </remarks>
    Public Shared Function FadeColor(ByVal firstColor As Color, ByVal secondColor As Color, ByVal firstWeight As Integer, ByVal secondWeight As Integer) As Color
        Dim r As Integer = CInt((firstWeight * firstColor.R + secondWeight * secondColor.R) / (firstWeight + secondWeight))
        Dim g As Integer = CInt((firstWeight * firstColor.G + secondWeight * secondColor.G) / (firstWeight + secondWeight))
        Dim b As Integer = CInt((firstWeight * firstColor.B + secondWeight * secondColor.B) / (firstWeight + secondWeight))
        Return Color.FromArgb(r, g, b)
    End Function

    ''' <summary>
    ''' Blends two colors with equal weights.
    ''' </summary>
    ''' <param name="firstColor"></param>
    ''' <param name="secondColor"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FadeColor(ByVal firstColor As Color, ByVal secondColor As Color) As Color
        Return FadeColor(firstColor, secondColor, 1, 1)
    End Function

    Private _sparklePenWidth As Single
    ''' <summary>
    ''' Draws the LED.
    ''' </summary>
    ''' <param name="graphics"></param>
    ''' <param name="color"></param>
    ''' <param name="sparkle"></param>
    ''' <remarks></remarks>
    Private Sub DrawLed(ByVal graphics As Graphics, ByVal color As Color, ByVal sparkle As Boolean)
        Dim halfPenWidth As Single = _sparklePenWidth / 2
        Dim imageWidth As Integer = Width - Me.Margin.Left
        Dim imageHeight As Integer = Height - Me.Margin.Top
        If sparkle Then
            graphics.FillEllipse(New SolidBrush(color), 1, 1, imageWidth, imageHeight)
            graphics.DrawArc(New Pen(FadeColor(color, color.White, 1, 2), _sparklePenWidth), _
                halfPenWidth + 2, halfPenWidth + 2, Width - 6 - halfPenWidth, Height - 6 - halfPenWidth, -90.0F, -90.0F)
            graphics.DrawEllipse(New Pen(FadeColor(color, color.White), 1), 1, 1, imageWidth, imageHeight)
        Else
            graphics.FillEllipse(New SolidBrush(color), 1, 1, imageWidth, imageHeight)
            graphics.DrawArc(New Pen(FadeColor(color, color.Black, 2, 1), _sparklePenWidth), _
                3, 3, Width - 7, Height - 7, 0.0F, 90.0F)
            graphics.DrawEllipse(New Pen(FadeColor(color, color.Black), 1), 1, 1, imageWidth, imageHeight)
        End If
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>
    ''' Event for adding external features.
    ''' </summary>
    ''' <remarks></remarks>
    Public Shadows Event Paint As PaintEventHandler

    ''' <summary>
    ''' Draws the control.
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        If PaintEvent IsNot Nothing Then
            PaintEvent(Me, e)
        Else
            MyBase.OnPaint(e)
            '        e.Graphics.Clear(BackColor);
            If Enabled Then
                If Me._flash Then
                    Me.DrawLed(e.Graphics, Me._flashColor, True)
                    'e.Graphics.FillEllipse(New SolidBrush(Me.FlashColor), 1, 1, Width - 3, Height - 3)
                    'e.Graphics.DrawArc(New Pen(FadeColor(FlashColor, Color.White, 1, 2), 2), 3, 3, Width - 7, Height - 7, -90.0F, -90.0F)
                    'e.Graphics.DrawEllipse(New Pen(FadeColor(FlashColor, Color.White), 1), 1, 1, Width - 3, Height - 3)
                Else
                    ' if non flash mode, set the on or off color.
                    If Active Then
                        Me.DrawLed(e.Graphics, Me._onColor, True)
                        'e.Graphics.FillEllipse(New SolidBrush(Me.OnColor), 1, 1, Width - 3, Height - 3)
                        'e.Graphics.DrawArc(New Pen(FadeColor(OnColor, Color.White, 1, 2), 2), 3, 3, Width - 7, Height - 7, -90.0F, -90.0F)
                        'e.Graphics.DrawEllipse(New Pen(FadeColor(OnColor, Color.White), 1), 1, 1, Width - 3, Height - 3)
                    Else
                        Me.DrawLed(e.Graphics, Me._offColor, False)
                        'e.Graphics.FillEllipse(New SolidBrush(OffColor), 1, 1, Width - 3, Height - 3)
                        'e.Graphics.DrawArc(New Pen(FadeColor(OffColor, Color.Black, 2, 1), 2), 3, 3, Width - 7, Height - 7, 0.0F, 90.0F)
                        'e.Graphics.DrawEllipse(New Pen(FadeColor(OffColor, Color.Black), 1), 1, 1, Width - 3, Height - 3)
                    End If
                End If
            Else
                e.Graphics.DrawEllipse(New Pen(System.Drawing.SystemColors.ControlDark, 1), 1, 1, Width - 3, Height - 3)
            End If
        End If
    End Sub

    Private tickIndex As Integer
    ''' <summary>
    ''' Handles the change of colors in flash mode.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub _Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        tickIndex += 1
        If tickIndex >= _flashIntervalsArray.Length Then
            tickIndex = 0
        End If
        tick.Interval = _flashIntervalsArray(tickIndex)
        If _flashColorsArray Is Nothing Then
            ' if no color array defined or array color index out of bounds,
            ' use the on/off colors to flash
            If Me._flashColor.Equals(Me._onColor) Then
                Me.FlashColor = Me._offColor
            Else
                Me.FlashColor = Me._onColor
            End If
        ElseIf (_flashColorsArray.Length <= tickIndex) _
              OrElse (_flashColorsArray(tickIndex).Equals(Color.Empty)) Then
            Me.FlashColor = Me._offColor
        Else
            Me.FlashColor = _flashColorsArray(tickIndex)
        End If
    End Sub

#End Region

End Class
