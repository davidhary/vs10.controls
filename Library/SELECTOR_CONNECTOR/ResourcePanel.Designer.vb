<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ResourcePanel

#If False Then
  Partial MustInherit Class InstrumentPanel
  Partial Class InstrumentPanel
#End If

    Inherits UserControlBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If disposing Then

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Connector = New isr.Controls.SelectorConnector
        Me.MainStatusBar = New System.Windows.Forms.StatusBar
        Me.StatusPanel = New System.Windows.Forms.StatusBarPanel
        Me.IdentityPanel = New System.Windows.Forms.StatusBarPanel
        Me.TipsToolTip = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.StatusPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IdentityPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Connector
        '
        Me.Connector.BackColor = System.Drawing.Color.Transparent
        Me.Connector.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Connector.IsConnected = False
        Me.Connector.Location = New System.Drawing.Point(0, 0)
        Me.Connector.Name = "Connector"
        Me.Connector.SelectedName = ""
        Me.Connector.Size = New System.Drawing.Size(312, 31)
        Me.Connector.TabIndex = 14
        '
        'MainStatusBar
        '
        Me.MainStatusBar.Location = New System.Drawing.Point(0, 31)
        Me.MainStatusBar.Name = "MainStatusBar"
        Me.MainStatusBar.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusPanel, Me.IdentityPanel})
        Me.MainStatusBar.ShowPanels = True
        Me.MainStatusBar.Size = New System.Drawing.Size(312, 24)
        Me.MainStatusBar.SizingGrip = False
        Me.MainStatusBar.TabIndex = 13
        Me.MainStatusBar.Text = "StatusBar1"
        '
        'statusPanel
        '
        Me.StatusPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.StatusPanel.Name = "statusPanel"
        Me.StatusPanel.Text = "<status>"
        Me.StatusPanel.Width = 285
        '
        'idPanel
        '
        Me.IdentityPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me.IdentityPanel.Name = "IdentityPanel"
        Me.IdentityPanel.Text = "<>"
        Me.IdentityPanel.Width = 27
        '
        'InstrumentPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.Connector)
        Me.Controls.Add(Me.MainStatusBar)
        Me.Name = "InstrumentPanel"
        Me.Size = New System.Drawing.Size(312, 55)
        CType(Me.StatusPanel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IdentityPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Protected WithEvents Connector As isr.Controls.SelectorConnector
    Protected WithEvents MainStatusBar As System.Windows.Forms.StatusBar
    Protected WithEvents TipsToolTip As System.Windows.Forms.ToolTip
    Protected WithEvents StatusPanel As System.Windows.Forms.StatusBarPanel
    Protected WithEvents IdentityPanel As System.Windows.Forms.StatusBarPanel

End Class
