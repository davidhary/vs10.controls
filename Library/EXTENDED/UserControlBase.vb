﻿''' <summary>
''' A base control implementing an Anonymous Publisher.
''' Useful for a settings publisher
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/02/2010" by="David Hary" revision="1.2.3988.x">
''' created.
''' </history>
Public Class UserControlBase
    Implements System.ComponentModel.INotifyPropertyChanged

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>A private constructor for this class making it not publicly creatable.
    ''' This ensure using the class as a singleton.
    ''' </summary>
    Protected Sub New()
        MyBase.new()
        Me.InitializeComponent()
    End Sub

#End Region

#Region " I NOTIFY PROPERTY CHANGED "

    ''' <summary>
    ''' Occurs when a property value changes.
    ''' </summary>
    Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

    ''' <summary>
    ''' Raised to notify the binadable object of a change.
    ''' </summary>
    ''' <param name="name"></param>
    ''' <remarks>
    ''' Includes a work around because for some reason, the dinding wrte value does not occure.
    ''' This is dengerous becuase it could lead to a stack overflow.
    ''' It can be used if the property changed event is raised only if the property changed.
    ''' </remarks>
    Protected Overridable Sub OnPropertyChanged(ByVal name As String)

        If PropertyChangedEvent IsNot Nothing Then
            PropertyChangedEvent(Me, New System.ComponentModel.PropertyChangedEventArgs(name))
        End If

    End Sub

#End Region

#Region " TOOL TIP WORK AROUND "

    ''' <summary>
    ''' Sets the Tool tip for all form controls that inherit the 
    ''' <see cref="UserControlBase">control base.</see>
    ''' </summary>
    ''' <param name="parent">Reference to the parent form or control.</param>
    ''' <param name="toolTip">The parent form or control tooltip.</param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")> _
    Public Shared Sub ToolTipSetter(ByVal parent As Windows.Forms.Control, ByVal toolTip As ToolTip)
        If TypeOf parent Is isr.Controls.UserControlBase Then
            CType(parent, isr.Controls.UserControlBase).ToolTipSetter(toolTip)
        ElseIf parent.HasChildren Then
            For Each control As Windows.Forms.Control In parent.Controls
                ToolTipSetter(control, toolTip)
            Next
        End If
    End Sub

    ''' <summary>
    ''' Sets a tool tip for all controls on the user control.
    ''' Uses the message already set for this control.
    ''' </summary>
    ''' <param name="toolTip">The tool tipe control from the parent form.</param>
    ''' <remarks>
    ''' This is required because setting a tool tip from the parent form
    ''' does not show the tool tip if hovering above children controls 
    ''' hosted by the user control.
    ''' </remarks>
    Public Sub ToolTipSetter(ByVal toolTip As ToolTip)
        applyToolTipToChildControls(Me, toolTip, toolTip.GetToolTip(Me))
    End Sub

    ''' <summary>
    ''' Sets a tool tip for all controls on the user control.
    ''' </summary>
    ''' <param name="toolTip">The tool tip control from the parent form.</param>
    ''' <param name="message">The tool tip message to apply to all the children controls and their children.</param>
    ''' <remarks>
    ''' This is required because setting a tool tip from the parent form
    ''' does not show the tool tip if hovering above children controls 
    ''' hosted by the user control.
    ''' </remarks>
    ''' <shoutouts>
    ''' pizzaboy at http://www.vbdotnetforums.com/component-development/42984-usercontrol-tooltip-message-not-appearing.html
    ''' </shoutouts>
    Public Sub ToolTipSetter(ByVal toolTip As ToolTip, ByVal message As String)
        applyToolTipToChildControls(Me, toolTip, message)
    End Sub

    ''' <summary>
    ''' Allpies the tool tip to all control hosted by the parent as well as all 
    ''' the children with these control.
    ''' </summary>
    ''' <param name="parent">The parent control.</param>
    ''' <param name="toolTip">The tool tip control from the parent form.</param>
    ''' <param name="message">The tool tip message to apply to all the children controls and their children.</param>
    ''' <remarks></remarks>
    Private Sub applyToolTipToChildControls(ByVal parent As Control, ByVal toolTip As ToolTip, ByVal message As String)
        For Each control As Control In parent.Controls
            toolTip.SetToolTip(control, message)
            If parent.HasChildren Then
                applyToolTipToChildControls(control, toolTip, message)
            End If
        Next
    End Sub

#End Region

End Class
