﻿Imports System.ComponentModel
''' <summary>
''' A Simple Push button control.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="09/21/02" by="David Hary" revision="1.0.839.x">
''' created.
''' </history>
<System.ComponentModel.Description("Push Button Control"), _
    System.Drawing.ToolboxBitmap(GetType(PushButton)), _
    System.ComponentModel.DefaultEvent("CheckedChanged")> _
Public Class PushButton

#Region " LABEL "

    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Caption width.")> _
    Public Property CaptionWidth() As Integer
        Get
            Return Me._label.Size.Width
        End Get
        Set(ByVal value As Integer)
            If value <> Me.CaptionWidth Then
                Me._label.Size = New System.Drawing.Size(value, Me._label.Size.Height)
                OnPropertyChanged("CaptionWidth")
            End If
        End Set
    End Property

    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Current caption."), _
      System.ComponentModel.DefaultValue("OFF")> _
    Public Property Caption() As String
        Get
            Return Me._label.Text
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                value = ""
            End If
            If Not Me.Caption.Equals(value) Then
                Me._label.Text = value
                OnPropertyChanged("Caption")
            End If
        End Set
    End Property

    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Location of caption relative to button."), _
      System.ComponentModel.DefaultValue(Windows.Forms.DockStyle.Top)> _
    Public Property CaptionDockStyle() As Windows.Forms.DockStyle
        Get
            Return _label.Dock
        End Get
        Set(ByVal value As Windows.Forms.DockStyle)
            If _label.Dock <> value Then
                If value = DockStyle.Bottom OrElse value = DockStyle.Left _
                   OrElse value = DockStyle.Right OrElse value = DockStyle.Top Then
                    _label.Dock = value
                    OnPropertyChanged("CaptionDockStyle")
                End If
            End If
        End Set
    End Property

    Private _checkedCaption As String
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Displayed when button is checked."), _
      System.ComponentModel.DefaultValue("ON")> _
    Public Property CheckedCaption() As String
        Get
            Return _checkedCaption
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                value = "ON"
            End If
            If value <> Me.CheckedCaption Then
                _checkedCaption = value
                OnPropertyChanged("CheckedCaption")
            End If
        End Set
    End Property

    Private _uncheckedCaption As String
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Displayed when button is unchecked."), _
      System.ComponentModel.DefaultValue("OFF")> _
    Public Property UncheckedCaption() As String
        Get
            Return _uncheckedCaption
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                value = "OFF"
            End If
            If value <> Me.UncheckedCaption Then
                _uncheckedCaption = value
                OnPropertyChanged("UncheckedCaption")
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets reference to the label control.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Label() As Label
        Get
            Return Me._label
        End Get
    End Property

#End Region

#Region " TOGGLE "

    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Text displayed on the button."), _
      System.ComponentModel.DefaultValue("&ON/OFF")> _
    Public Property ButtonText() As String
        Get
            Return Me._toggle.Text
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                value = "&ON/OFF"
            End If
            If Me.ButtonText <> value Then
                Me._toggle.Text = value
                OnPropertyChanged("ButtonText")
            End If
        End Set
    End Property

    ''' <summary>
    ''' Sets the <see cref="PushButton">push button</see> checked value to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' The setter disables the control before altering the checked state allowing the 
    ''' control code to use the enabled state for preventing the execution of the control
    ''' checked change actions.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    Public Function SafeSilentCheckedSetter(ByVal value As Boolean) As Boolean
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Boolean)(AddressOf SafeSilentCheckedSetter), New Object() {value})
        Else
            Dim wasEnabled As Boolean = Me.Enabled
            Me.Enabled = False
            Me.Checked = value
            Me.Enabled = wasEnabled
        End If
        Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="PushButton">push button</see> checked value to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    Public Function SafeCheckedSetter(ByVal value As Boolean) As Boolean
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Boolean)(AddressOf SafeCheckedSetter), New Object() {value})
        Else
            Me.Checked = value
        End If
        Return value
    End Function

    ''' <summary>
    ''' Sets the <see cref="PushButton">push button</see> enabled value to the 
    ''' <paramref name="value">value</paramref>.
    ''' This setter is thread safe.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <returns>value</returns>
    ''' <remarks></remarks>
    Public Function SafeEnabledSetter(ByVal value As Boolean) As Boolean
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Boolean)(AddressOf SafeEnabledSetter), New Object() {value})
        Else
            Me.Enabled = value
        End If
        Return value
    End Function

    ''' <summary>
    ''' Gets or sets the checked value of the control.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Status of the toggle button."), _
      System.ComponentModel.DefaultValue(False)> _
    Public Property Checked() As Boolean
        Get
            Return _toggle.Checked
        End Get
        Set(ByVal value As Boolean)
            If Me.Checked <> value Then
                _toggle.Checked = value
                OnPropertyChanged("Checked")
            End If
        End Set
    End Property

    ''' <summary>
    ''' Raised whenever a property is changed.
    ''' </summary>
    ''' <remarks></remarks>
    Public Event CheckedChanged As EventHandler(Of EventArgs)

    Private Sub _toggle_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _toggle.CheckedChanged
        If _toggle.Checked Then
            _label.Text = _checkedCaption
        Else
            _label.Text = _uncheckedCaption
        End If
        If CheckedChangedEvent IsNot Nothing Then CheckedChangedEvent(Me, e)
    End Sub

    Public ReadOnly Property Toggle() As System.Windows.Forms.CheckBox
        Get
            Return _toggle
        End Get
    End Property

#End Region

End Class
