﻿Imports System.Runtime.CompilerServices

''' <summary>
''' Includes extensions for 
''' <see cref="System.Windows.Forms.Control">controls</see>.
''' </summary>
''' <remarks></remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/09/2009" by="David Hary" revision="1.1.3386.x">
''' created.
''' </history>
Public Module ControlExtensions

  ''' <summary>
  ''' Returns the control requesting help based on the mouse
  ''' position clicked.</summary>
  ''' <param name="containerControl">Specifiesthe container control requesting the help.</param>
  ''' <returns>Returns the control upon which the operator clicked with
  '''   the help icon or nothing if no control was clicked.</returns>
  ''' <remarks></remarks>
  <Extension()> _
  Public Function ControlRequestingHelp(ByVal containerControl As Windows.Forms.Control, _
                                        ByVal helpEvent As Windows.Forms.HelpEventArgs) As Windows.Forms.Control

    If helpEvent Is Nothing Then
      Return Nothing
    End If

    ' Convert screen coordinates to client coordinates
    Dim clientCoordinatePoint As System.Drawing.Point = containerControl.PointToClient(helpEvent.MousePos)

    ' look for the control upon which the operator clicked.
    For i As Integer = 0 To containerControl.Controls.Count
      If containerControl.Controls.Item(i).Bounds.Contains(clientCoordinatePoint) Then
        Return containerControl.Controls.Item(i)
      End If
    Next

    ' if no control was located, return nothing
    Return Nothing

  End Function

  ''' <summary>Searches the combo box and selects a located item.</summary>
  ''' <param name="combo">Specifies an instance of a ComboBox.</param>
  ''' <param name="e">Specifies an instance of the 
  '''   <see cref="System.Windows.Forms.KeyPressEventArgs">event arguments</see>,
  '''   which specify the key pressed.</param>
  ''' <remarks>Use this method to search and select combo box index.</remarks>
  <Extension()> _
  Public Function SearchAndSelect(ByVal combo As Windows.Forms.ComboBox, _
                                  ByVal e As System.Windows.Forms.KeyPressEventArgs) As Integer

    If e IsNot Nothing AndAlso Char.IsControl(e.KeyChar) Then
      Return combo.SelectedIndex
    Else
      Dim cursorPosition As Integer = combo.SelectionStart
      Dim selectionLength As Integer = combo.SelectionLength
      Dim itemNumber As Integer = combo.FindString(combo.Text)
      If itemNumber >= 0 Then
        ' if we have a match, select the current item and reposition the cursor
        combo.SelectedIndex = itemNumber
        combo.SelectionStart = cursorPosition
        combo.SelectionLength = selectionLength
        Return itemNumber
      Else
        Return combo.SelectedIndex
      End If
    End If

  End Function

  ''' <summary>Searches the combo box and selects a located item
  '''   upon releasing a key.</summary>
  ''' <param name="combo">Specifies an instance of a ComboBox.</param>
  ''' <param name="e">Specifies an instance of the 
  '''   <see cref="System.Windows.Forms.KeyEventArgs">event arguments</see>,
  '''   which specify the key released.</param>
  ''' <remarks>Use this method to search and select combo box index.</remarks>
  <Extension()> _
  Public Function SearchAndSelect(ByVal combo As Windows.Forms.ComboBox, _
                                  ByVal e As System.Windows.Forms.KeyEventArgs) As Integer

    If e Is Nothing Then
      Throw New ArgumentNullException("e")
    End If

    If e.KeyCode.ToString.Length > 1 Then
      ' if a control character, skip
      Return combo.SelectedIndex
    Else
      Dim cursorPosition As Integer = combo.SelectionStart
      Dim selectionLength As Integer = combo.SelectionLength
      Dim itemNumber As Integer = combo.FindString(combo.Text)
      If itemNumber >= 0 Then
        ' if we have a match, select the current item and reposition the cursor
        combo.SelectedIndex = itemNumber
        combo.SelectionStart = cursorPosition
        combo.SelectionLength = selectionLength
        Return itemNumber
      Else
        Return combo.SelectedIndex
      End If
    End If

  End Function

  ''' <summary>Copies properties from a source to a destination controls.</summary>
  ''' <param name="source">
  '''   specifies an instance of the control which
  '''   properties are copied from.</param>
  ''' <param name="destination">
  '''   specifies an instance of the control which
  '''   properties are copied to.</param>
  ''' <remarks>Use this method to copy the properties of a control to a new control.</remarks>
  ''' <example>This example adds a button to a form.
  '''   <code>
  '''     Private buttons As New ArrayList()
  '''     Private Sub addButton(ByVal sourceButton As Button)
  '''       Dim newButton As New Button()
  '''       WinFormsSupport.CopyControlProperties(sourceButton, newButton)
  '''       Me.Controls.Add(newButton)
  '''       AddHandler newButton.Click, AddressOf exitButton_Click
  '''       With newButton
  '''         .Name = String.Format( System.Globalization.CultureInfo.CurrentCulture, "newButton {0}", buttons.count.toString)
  '''         .Text = .Name
  '''         .Top = .Top + .Height * (buttons.Count + 1)
  '''         buttons.Add(newButton)
  '''       End With
  '''     End Sub
  '''   </code>
  '''   To run this example, paste the code fragment into the method region
  '''   of a Visual Basic form.  Run the program by pressing F5.
  ''' </example>
  <Extension()> _
  Public Sub CopyControlProperties(ByVal source As System.Windows.Forms.Control, _
                                   ByVal destination As System.Windows.Forms.Control)

    Dim dontCopyNames() As String = {"WindowTarget"}
    Dim pinfos(), pinfo As System.Reflection.PropertyInfo
    If Not (source Is Nothing OrElse destination Is Nothing) Then
      pinfos = source.GetType.GetProperties()
      For Each pinfo In pinfos
        ' copy only those properties without parameters - you'll rarely need indexed properties
        If pinfo.GetIndexParameters().Length = 0 Then
          ' skip properties that appear in the list of disallowed names.
          If Array.IndexOf(dontCopyNames, pinfo.Name) < 0 Then
            ' can only copy those properties that can be read and written.  
            If pinfo.CanRead And pinfo.CanWrite Then
              ' set the destination based on the source.
              pinfo.SetValue(destination, pinfo.GetValue(source, Nothing), Nothing)
            End If
          End If
        End If
      Next
    End If

  End Sub

  ''' <summary>
  ''' Validates the specified control.</summary>
  ''' <param name="value">Control to be validated.</param>
  ''' <param name="validatedContainerControl">specifies the 
  '''   instance of the container control which controls are to be validated.</param>
  ''' <returns>Returns false if failed to validate. Retursn true if control validated or not visible.</returns>
  ''' <remarks></remarks>
  <Extension()> _
  Public Function Validate(ByVal value As Control, ByVal validatedContainerControl As Windows.Forms.ContainerControl) As Boolean

    ' focus on and validate the control
    If validatedContainerControl.Visible AndAlso value.Visible Then
      value.Focus()
      Return Not validatedContainerControl.Validate()
    Else
      Return True
    End If

  End Function

  ''' <summary>
  ''' Validates all controls in the container control.</summary>
  ''' <param name="validatedContainerControl">specifies the 
  '''   instance of the container control which controls are to be validated.</param>
  ''' <returns>Returns false if any control failed to validated.</returns>
  ''' <remarks></remarks>
  <Extension()> _
  Public Function ValidateControls(ByVal validatedContainerControl As Windows.Forms.ContainerControl) As Boolean

    ValidateControls = True
    Dim controls As Windows.Forms.Control() = validatedContainerControl.Controls.RetrieveControls()
    For i As Integer = 0 To controls.Length - 1
      ' validate the selected control
      If Not controls(i).Validate(validatedContainerControl) Then
        ValidateControls = False
      End If
    Next

  End Function

  ''' <summary>Retrieve all controls and child controls.</summary>
  ''' <param name="winFormControls"></param>
  ''' <returns>Returns an array of controls</returns>
  ''' <remarks>Make sure to send contrll back at lowest depth first so that most
  '''   child controls are checked for things before container controls, e.g., 
  '''   a TextBox is checked before a GroupBox control.</remarks>
  ''' <history date="04/15/05" by="David Hary" revision="1.0.1566.x">
  '''   Bug fix.  Return empty controls rather than nothing if no controls.
  ''' </history>
  <Extension()> _
  Public Function RetrieveControls(ByVal winFormControls As Windows.Forms.Control.ControlCollection) As Windows.Forms.Control()

    Dim controlList As New ArrayList

    ' Dim allControls As ArrayList = New ArrayList
    Dim myQueue As Queue = New Queue
    ' add controls to the queue
    myQueue.Enqueue(winFormControls)

    Dim myControls As Windows.Forms.Control.ControlCollection
    'Dim current As Object
    Do While myQueue.Count > 0
      ' remove and return the object at the begining of the queue
      ' current = myQueue.Dequeue()
      If TypeOf myQueue.Peek Is Windows.Forms.Control.ControlCollection Then
        myControls = CType(myQueue.Dequeue, Windows.Forms.Control.ControlCollection)
        If myControls.Count > 0 Then
          For i As Integer = 0 To myControls.Count - 1
            Dim myControl As Windows.Forms.Control = myControls.Item(i)
            ' add this control to the array list
            controlList.Add(myControl)
            ' check if this control has a collection
            If myControl.Controls IsNot Nothing Then
              ' if so, add to the queue
              myQueue.Enqueue(myControl.Controls)
            End If
          Next
          'For Each myControl As Windows.Forms.Control In myControls
          ' add this control to the array list
          'controlList.Add(myControl)
          ' check if this control has a collection
          'If Not IsNothing(myControl.Controls) Then
          ' if so, add to the queue
          ' myQueue.Enqueue(myControl.Controls)
          'End If
          'Next
        End If
      Else
        myQueue.Dequeue()
      End If
    Loop
    If controlList.Count > 0 Then
      Dim allControls(controlList.Count - 1) As Windows.Forms.Control
      controlList.CopyTo(allControls)
      Return allControls
    Else
      Return New Windows.Forms.Control() {}
    End If

  End Function

End Module
