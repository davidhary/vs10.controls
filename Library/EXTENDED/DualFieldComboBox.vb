﻿Imports System.ComponentModel
''' <summary>
''' A drop down box with two fields.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/23/2011" by="David Hary" revision="1.02.4099.x">
''' created.
''' </history>
<Description("DualFieldComboBox"), _
    Drawing.ToolboxBitmap(GetType(DualFieldComboBox))> _
    <DefaultBindingProperty("Text")> _
Public Class DualFieldComboBox

    Public ReadOnly Property ComboBox() As System.Windows.Forms.ComboBox
        Get
            Return Me._TextComboBox
        End Get
    End Property

    Private Sub parseRecord(ByVal value As String)
        If String.IsNullOrEmpty(value) OrElse String.IsNullOrEmpty(value.Trim) Then
            value = ""
            _FirstFieldTextBox.Text = value
            _SecondFieldTextBox.Text = value
        Else
            value = value.Trim
            Dim names As String() = value.Split(" "c)
            If names.Length = 1 Then
                _SecondFieldTextBox.Text = value
                _FirstFieldTextBox.Text = ""
            Else
                _SecondFieldTextBox.Text = names(names.Length - 1)
                _FirstFieldTextBox.Text = value.Substring(0, value.Length - SecondField.Length).Trim
            End If
        End If
        _text = value
        OnPropertyChanged("Text")
    End Sub

    Private _text As String
    ''' <summary>
    ''' Gets or sets the text, which includes the two fields.
    ''' </summary>
    ''' <value>
    ''' The full name.
    ''' </value>
    Public Overrides Property Text() As String
        Get
            Return _text
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                value = ""
            End If
            If Not value.Equals(Me.Text) Then
                parseRecord(value)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the first field.
    ''' </summary>
    ''' <value>
    ''' The first field.
    ''' </value>
    Public ReadOnly Property FirstField() As String
        Get
            Return _FirstFieldTextBox.Text
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the second field.
    ''' </summary>
    ''' <value>
    ''' The second field.
    ''' </value>
    Public ReadOnly Property SecondField() As String
        Get
            Return _SecondFieldTextBox.Text
        End Get
    End Property

    Private Sub _ValueComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _TextComboBox.SelectedIndexChanged
        Me.parseRecord(_TextComboBox.Text)
    End Sub

    Public Function BuildValue() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0} {1}", Me.FirstField, Me.SecondField)
    End Function

    Private Sub _FirstFieldTextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _FirstFieldTextBox.Validated
        ' Me.parseRecord(Me.BuildValue)
    End Sub

    Private Sub _SecondFieldTextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SecondFieldTextBox.Validated
        ' Me.parseRecord(Me.BuildValue)
    End Sub

    Private Sub DualFieldComboBox_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        resizeMe()
    End Sub

    Private Sub resizeMe()
        Const buttonWidth As Integer = 17
        _Layout.Width = Me.Width - buttonWidth - Me.Padding.Left - Me.Padding.Right
        _TextComboBox.Width = _Layout.Width + 17 - _Layout.Padding.Left
        _TextComboBox.Left = _Layout.Left + _Layout.Padding.Left
        _TextComboBox.Top = Me.Height - Me.Padding.Top - Me.Padding.Bottom - _TextComboBox.Height
    End Sub

    Private Sub _TextComboBox_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles _TextComboBox.Layout
        resizeMe()
    End Sub

    Private Sub DualFieldComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Validated
        Me.parseRecord(Me.BuildValue)
    End Sub

End Class
