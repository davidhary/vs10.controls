﻿Imports System.ComponentModel
''' <summary>
''' A dual radio button control functioning as a check box control displaying both its states.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="12/09/2010" by="David Hary" revision="1.02.3995.x">
''' created.
''' </history>
<Description("DualRadioButton"), _
    DefaultEvent("CheckToggled"), _
    Drawing.ToolboxBitmap(GetType(DualRadioButton))> _
    <DefaultBindingProperty("Checked")> _
Public Class DualRadioButton

#Region " CONSTRUCTORS "

    ''' <summary>
    ''' Clears internal properties.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()
        InitializeComponent()
        Me._PrimaryButtonHorizontalLocation = HorizontalLocation.Left
        Me._PrimaryButtonVerticalLocation = VerticalLocation.Center
        Me._SecondaryButtonHorizontalLocation = HorizontalLocation.Right
        Me._SecondaryButtonVerticalLocation = VerticalLocation.Center
        Me.PrimaryRadioButton.ReadOnly = False
        Me.SecondaryRadioButton.ReadOnly = False
        Me._Checked = False
        Me.PrimaryRadioButton.Checked = False
        Me.SecondaryRadioButton.Checked = False
        Me.updateButtonsLocation()
    End Sub

#End Region

#Region " APPEARANCE "

    Private _PrimaryButtonHorizontalLocation As HorizontalLocation
    <DefaultValue(GetType(HorizontalLocation), "Left")> _
    <Category("Behavior")> _
    <Description("Horizontal location of the primary button.")> _
    Public Property PrimaryButtonHorizontalLocation() As HorizontalLocation
        Get
            Return _PrimaryButtonHorizontalLocation
        End Get
        Set(ByVal value As HorizontalLocation)
            If value <> Me.PrimaryButtonHorizontalLocation Then
                _PrimaryButtonHorizontalLocation = value
                Me.OnPropertyChanged("PrimaryButtonHorizontalLocation")
                updateButtonsLocation()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the text associated with the primary button.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Appearance"), Description("The text associated with the primary button"), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue("ON")> _
    Public Property PrimaryButtonText() As String
        Get
            Return Me.PrimaryRadioButton.Text
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                value = ""
            End If
            If Me.PrimaryButtonText <> value Then
                Me.PrimaryRadioButton.Text = value
                Me.OnPropertyChanged("PrimaryButtonText")
            End If
        End Set
    End Property

    Private _PrimaryButtonVerticalLocation As VerticalLocation
    <DefaultValue(GetType(VerticalLocation), "Center")> _
    <Category("Behavior")> _
    <Description("Vertical location of the primary button.")> _
    Public Property PrimaryButtonVerticalLocation() As VerticalLocation
        Get
            Return _PrimaryButtonVerticalLocation
        End Get
        Set(ByVal value As VerticalLocation)
            If value <> Me.PrimaryButtonVerticalLocation Then
                _PrimaryButtonVerticalLocation = value
                Me.OnPropertyChanged("PrimaryButtonVerticalLocation")
                updateButtonsLocation()
            End If
        End Set
    End Property

    Private _SecondaryButtonHorizontalLocation As HorizontalLocation
    <DefaultValue(GetType(HorizontalLocation), "Right")> _
    <Category("Behavior")> _
    <Description("Horizontal location of the Secondary button.")> _
    Public Property SecondaryButtonHorizontalLocation() As HorizontalLocation
        Get
            Return _SecondaryButtonHorizontalLocation
        End Get
        Set(ByVal value As HorizontalLocation)
            If value <> Me.SecondaryButtonHorizontalLocation Then
                _SecondaryButtonHorizontalLocation = value
                Me.OnPropertyChanged("SecondaryButtonHorizontalLocation")
                updateButtonsLocation()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the text associated with the secondary button.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Appearance"), Description("The text associated with the Secondary button"), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue("ON")> _
    Public Property SecondaryButtonText() As String
        Get
            Return Me.SecondaryRadioButton.Text
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                value = ""
            End If
            If Me.SecondaryButtonText <> value Then
                Me.SecondaryRadioButton.Text = value
                Me.OnPropertyChanged("SecondaryButtonText")
            End If
        End Set
    End Property

    Private _SecondaryButtonVerticalLocation As VerticalLocation
    <DefaultValue(GetType(VerticalLocation), "Center")> _
    <Category("Behavior")> _
    <Description("Vertical location of the Secondary button.")> _
    Public Property SecondaryButtonVerticalLocation() As VerticalLocation
        Get
            Return _SecondaryButtonVerticalLocation
        End Get
        Set(ByVal value As VerticalLocation)
            If value <> Me.SecondaryButtonVerticalLocation Then
                _SecondaryButtonVerticalLocation = value
                Me.OnPropertyChanged("SecondaryButtonVerticalLocation")
                updateButtonsLocation()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Updates the location of the buttons.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub updateButtonsLocation()
        Dim primaryColumn As Integer = 1
        Dim primaryRow As Integer = 1
        Dim secondaryColumn As Integer = 2
        Dim secondaryRow As Integer = 1

        Dim rowPercent As Single = 100.0!
        Dim colPercent As Single = 100.0!

        ' clear the styles
        For Each colStyle As ColumnStyle In Me._Layout.ColumnStyles
            colStyle.SizeType = SizeType.Percent
        Next
        For Each rowStyle As RowStyle In Me._Layout.RowStyles
            rowStyle.SizeType = SizeType.Percent
        Next

        Select Case PrimaryButtonVerticalLocation
            Case isr.Controls.VerticalLocation.Bottom
                primaryRow = 2
            Case isr.Controls.VerticalLocation.Center
                primaryRow = 1
            Case isr.Controls.VerticalLocation.Top
                primaryRow = 0
        End Select
        Me._Layout.RowStyles(primaryRow).SizeType = SizeType.AutoSize

        Select Case PrimaryButtonHorizontalLocation
            Case HorizontalLocation.Center
                primaryColumn = 1
            Case HorizontalLocation.Left
                primaryColumn = 0
            Case HorizontalLocation.Right
                primaryColumn = 2
        End Select
        Me._Layout.ColumnStyles(primaryColumn).SizeType = SizeType.AutoSize

        Select Case SecondaryButtonVerticalLocation
            Case isr.Controls.VerticalLocation.Bottom
                secondaryRow = 2
            Case isr.Controls.VerticalLocation.Center
                secondaryRow = 1
            Case isr.Controls.VerticalLocation.Top
                secondaryRow = 0
        End Select
        Me._Layout.RowStyles(secondaryRow).SizeType = SizeType.AutoSize
        If primaryRow = secondaryRow Then
            rowPercent = 50
        End If
        For Each rowStyle As RowStyle In Me._Layout.RowStyles
            If rowStyle.SizeType = SizeType.Percent Then
                rowStyle.Height = rowPercent
            End If
        Next

        Select Case SecondaryButtonHorizontalLocation
            Case HorizontalLocation.Center
                secondaryColumn = 1
            Case HorizontalLocation.Left
                secondaryColumn = 0
            Case HorizontalLocation.Right
                secondaryColumn = 2
        End Select
        If secondaryColumn = primaryColumn AndAlso secondaryRow = primaryRow Then
            Select Case PrimaryButtonHorizontalLocation
                Case HorizontalLocation.Center
                    secondaryColumn = 2
                Case HorizontalLocation.Left
                    secondaryColumn = 2
                Case HorizontalLocation.Right
                    secondaryColumn = 0
            End Select
        End If
        Me._Layout.ColumnStyles(secondaryColumn).SizeType = SizeType.AutoSize
        If primaryColumn = secondaryColumn Then
            colPercent = 50
        End If
        For Each ColumnStyle As ColumnStyle In Me._Layout.ColumnStyles
            If ColumnStyle.SizeType = SizeType.Percent Then
                ColumnStyle.Width = colPercent
            End If
        Next
        Me._Layout.Controls.Clear()
        Me._Layout.Controls.Add(Me.PrimaryRadioButton, primaryColumn, primaryRow)
        Me._Layout.Controls.Add(Me.SecondaryRadioButton, secondaryColumn, secondaryRow)

    End Sub

#End Region

#Region " BEHAVIOR "

    ''' <summary>
    ''' Gets or sets the read only property
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DefaultValue(False)> _
    <Category("Behavior")> _
    <Description("Indicates wether the dual radio button is read only.")> _
    Public Property [ReadOnly]() As Boolean
        Get
            Return Me.PrimaryRadioButton.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            If Me.ReadOnly <> value Then
                Me.PrimaryRadioButton.ReadOnly = value
                Me.SecondaryRadioButton.ReadOnly = value
                Me.OnPropertyChanged("ReadOnly")
            End If
        End Set
    End Property

    Private _Checked As Boolean
    ''' <summary>
    ''' Gets or sets the checked property
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DefaultValue(False)> _
    <Category("Behavior")> _
    <Description("Indicates whether the radio button is checked or not.")> _
    Public Property Checked() As Boolean
        Get
            Return Me._Checked
        End Get
        Set(ByVal value As Boolean)
            If Me.Checked <> value Then
                Me._Checked = value
                PrimaryRadioButton.Checked = value
                SecondaryRadioButton.Checked = Not value
                Me.OnPropertyChanged("Checked")
                Me.OnCheckToggled()
            End If
        End Set
    End Property

    Private Sub PrimaryRadioButton_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PrimaryRadioButton.CheckedChanged
        Me.Checked = PrimaryRadioButton.Checked
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>Occurs when the checked value changed..
    ''' </summary>
    ''' <param name="e">Specifies the
    ''' <see cref="EventArgs">event arguments</see>.
    ''' </param>
    Public Event CheckToggled As EventHandler(Of EventArgs)

    ''' <summary>Raises the Check changed event.
    ''' </summary>
    Protected Overridable Sub OnCheckToggled()
        If CheckToggledEvent IsNot Nothing Then CheckToggledEvent(Me, EventArgs.Empty)
    End Sub

#End Region

End Class

Public Enum HorizontalLocation
  ''' <summary>Button located on the left side of the control (defauld behavior)</summary>
  Left
  ''' <summary>Button located in the center of the control</summary>
  Center
  ''' <summary>Button located at the right side of the control</summary>
  Right
End Enum

Public Enum VerticalLocation
  ''' <summary>Button located on the top side of the control (defauld behavior)</summary>
  Top
  ''' <summary>Button located in the center of the control</summary>
  Center
  ''' <summary>Button located at the bottom of the control</summary>
  Bottom
End Enum

