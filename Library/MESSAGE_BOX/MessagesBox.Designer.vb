<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MessagesBox
    Inherits System.Windows.Forms.TextBox

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If disposing Then

                ' Free managed resources when explicitly called
                disposeManagedResources()

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

            ' Free shared unmanaged resources
            ' onDisposeUnmanagedResources

        Finally

            ' Invoke the base class dispose method        
            MyBase.Dispose(disposing)

        End Try

    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
        MyBase.Multiline = True
        MyBase.ReadOnly = True
        MyBase.ScrollBars = System.Windows.Forms.ScrollBars.Both
        MyBase.Size = New System.Drawing.Size(150, 150)
    End Sub
End Class
