﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ThreadSafeControls;

namespace ThreadSafeControlsTest {
    public partial class ThreadSafeControlsTestForm : Form {

        private static readonly Color[] Colors;
        private static readonly Font[] Fonts;

        static ThreadSafeControlsTestForm() {
            Colors = new Color[] { Color.Red, Color.Orange, Color.Yellow, Color.Green, Color.Blue, Color.Indigo, Color.Violet };

            var installedFonts = new InstalledFontCollection();
            var families = installedFonts.Families;
            var availableFonts = new List<Font>();
            for (int i = 0; i < families.Length; ++i) {
                var family = families[i];

                try {
                    if (family.IsStyleAvailable(FontStyle.Regular)) {
                        var font = new Font(family, Control.DefaultFont.Size);
                        availableFonts.Add(font);
                    }

                } catch (Exception) {
                    continue;
                }
            }

            Fonts = availableFonts.ToArray();
        }

        public ThreadSafeControlsTestForm() {
            InitializeComponent();
        }

        private void StartThreadSafeTestButton_Click(object sender, EventArgs e) {
            if (ThreadSafeTestWorker.IsBusy)
                return;

            ThreadSafeTestWorker.RunWorkerAsync();
        }

        private void StopThreadSafeTestButton_Click(object sender, EventArgs e) {
            if (!ThreadSafeTestWorker.IsBusy)
                return;

            ThreadSafeTestWorker.CancelAsync();
        }

        private void ThreadSafeTestWorker_DoWork(object sender, DoWorkEventArgs e) {
            var worker = sender as BackgroundWorker;
            if (sender == null)
                return;

            var rand = new Random();

            var frm = this.AsThreadSafeControl();
            var tbx = ThreadSafeTestTextBox.AsThreadSafe();
            var lbx = ThreadSafeTestListBox.AsThreadSafe();
            var lvw = ThreadSafeTestListView.AsThreadSafe();
            var lblFontChanging = ThreadSafeTestFontChangingLabel.AsThreadSafeControl();
            var lblBlinking = ThreadSafeTestBlinkingLabel.AsThreadSafeControl();
            var lblMoving = ThreadSafeTestMovingLabel.AsThreadSafeControl();
            var btnStart = StartThreadSafeTestButton.AsThreadSafe();
            var btnStop = StopThreadSafeTestButton.AsThreadSafe();
            var prg = ThreadSafeTestProgressBar.AsThreadSafe();
            var dtm = ThreadSafeTestDateTimePicker.AsThreadSafe();
            var cbx = ThreadSafeTestCheckBox.AsThreadSafe();
            var rbn = ThreadSafeTestRadioButton.AsThreadSafe();

            while (!worker.CancellationPending) {
                // randomly perform one of the following GUI updates
                switch (rand.Next(0, 22)) {
                    case 0:
                        // appends "text" to the textbox
                        tbx.AppendText("text");
                        break;
                    case 1:
                        // clear textbox
                        tbx.Clear();
                        break;
                    case 2:
                        // randomly change textbox's background color
                        tbx.BackColor = Colors[rand.Next(0, Colors.Length)];
                        break;
                    case 3:
                        // randomly add 1-10 item(s) to the listbox
                        var items = new object[rand.Next(1, 11)];
                        for (int i = 0; i < items.Length; ++i)
                            items[i] = "item" + i.ToString();

                        lbx.Items.AddRange(items);
                        break;
                    case 4:
                        // randomly remove a single item from the listbox
                        if (lbx.Items.Count > 0)
                            lbx.Items.RemoveAt(rand.Next(0, lbx.Items.Count));
                        break;
                    case 5:
                        // randomly clear the listbox
                        lbx.Items.Clear();
                        break;
                    case 6:
                        // randomly add 1-3 column(s) to the listview
                        int n = rand.Next(1, 4);
                        for (int i = 0; i < n; ++i)
                            lvw.Columns.Add("column" + i.ToString());
                        break;
                    case 7:
                        // randomly add 1-10 item(s) to the listview
                        var lvwItems = new ListViewItem[rand.Next(1, 11)];
                        for (int i = 0; i < lvwItems.Length; ++i)
                            lvwItems[i] = new ListViewItem("item" + i.ToString());

                        lvw.Items.AddRange(lvwItems);
                        break;
                    case 8:
                        // randomly remove a single item from the listview
                        if (lvw.Items.Count > 0)
                            lvw.Items.RemoveAt(rand.Next(0, lvw.Items.Count));
                        break;
                    case 9:
                        // randomly change the text of an item in the listview
                        if (lvw.Items.Count > 0)
                            lvw.Items[rand.Next(0, lvw.Items.Count)].Text = rand.Next(0, 2) == 0 ? "Hello!" : "Goodbye!";
                        break;
                    case 10:
                        // randomly change the text of a subitem in the listview
                        if (lvw.Items.Count > 0 && lvw.Columns.Count > 0) {
                            var lvwItem = lvw.Items[rand.Next(0, lvw.Items.Count)];
                            if (lvwItem.SubItems.Count < lvw.Columns.Count)
                                lvwItem.SubItems.Add("text");
                            else
                                lvwItem.SubItems[rand.Next(0, lvwItem.SubItems.Count)].Text = rand.Next(0, 2) == 0 ? "Hello!" : "Goodbye!";
                        }
                        break;
                    case 11:
                        // randomly clear the columns in the listview
                        lvw.Columns.Clear();
                        break;
                    case 12:
                        // randomly clear the items in the listview
                        lvw.Items.Clear();
                        break;
                    case 13:
                        // randomly change start button's bg color
                        btnStart.BackColor = Colors[rand.Next(0, Colors.Length)];
                        break;
                    case 14:
                        // randomly change stop button's bg color
                        btnStop.BackColor = Colors[rand.Next(0, Colors.Length)];
                        break;
                    case 15:
                        // randomly change font-changing label's font
                        lblFontChanging.Font = Fonts[rand.Next(0, Fonts.Length)];
                        break;
                    case 16:
                        // make blinking label appear/disappear
                        lblBlinking.Visible = !lblBlinking.Visible;
                        break;
                    case 17:
                        // randomly change moving label's position
                        lblMoving.Left = rand.Next(0, frm.Width - lblMoving.Width);
                        break;
                    case 18:
                        // randomly change the value of the progressbar
                        prg.Value = rand.Next(prg.Minimum, prg.Maximum + 1);
                        break;
                    case 19:
                        // randomly change the value of the datetimepicker
                        var range = dtm.MaxDate - dtm.MinDate;
                        var milliseconds = range.TotalMilliseconds;
                        dtm.Value = dtm.MinDate + TimeSpan.FromMilliseconds(milliseconds * rand.NextDouble());
                        break;
                    case 20:
                        // randomly check/uncheck the checkbox
                        cbx.Checked = !cbx.Checked;
                        break;
                    case 21:
                        // randomly check/uncheck the radiobutton
                        rbn.Checked = !rbn.Checked;
                        break;
                }
            }
        }
    }
}
