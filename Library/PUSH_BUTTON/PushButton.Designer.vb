﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PushButton
    Inherits UserControlBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._toggle = New System.Windows.Forms.CheckBox
        Me._label = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        '_toggle
        '
        Me._toggle.Appearance = System.Windows.Forms.Appearance.Button
        Me._toggle.Dock = System.Windows.Forms.DockStyle.Fill
        Me._toggle.Location = New System.Drawing.Point(3, 18)
        Me._toggle.Name = "_toggle"
        Me._toggle.Size = New System.Drawing.Size(144, 129)
        Me._toggle.TabIndex = 0
        Me._toggle.Text = "&ON/OFF"
        Me._toggle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._toggle.UseVisualStyleBackColor = True
        '
        '_label
        '
        Me._label.Dock = System.Windows.Forms.DockStyle.Top
        Me._label.Location = New System.Drawing.Point(3, 3)
        Me._label.Name = "_label"
        Me._label.Size = New System.Drawing.Size(144, 15)
        Me._label.TabIndex = 1
        Me._label.Text = "OFF"
        Me._label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PushButton
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me._toggle)
        Me.Controls.Add(Me._label)
        Me.Name = "PushButton"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents _toggle As System.Windows.Forms.CheckBox
    Friend WithEvents _label As System.Windows.Forms.Label

End Class
