Partial Public Class SplitButtonPanel
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (Not components Is Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SplitButtonPanel))
        Me.groupBox1 = New System.Windows.Forms.GroupBox()
        Me.groupBox2 = New System.Windows.Forms.GroupBox()
        Me.InfoText = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.SplitWidth = New System.Windows.Forms.NumericUpDown()
        Me.label1 = New System.Windows.Forms.Label()
        Me.SplitHeight = New System.Windows.Forms.NumericUpDown()
        Me.FillSplitHeight = New System.Windows.Forms.CheckBox()
        Me.CalculateSplitRect = New System.Windows.Forms.CheckBox()
        Me.AlwaysHoverChange = New System.Windows.Forms.CheckBox()
        Me.AlwaysDropDown = New System.Windows.Forms.CheckBox()
        Me.SplitEnabled = New System.Windows.Forms.CheckBox()
        Me.SplitButtonDropDown = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.toolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.toolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SplitButtonImages = New System.Windows.Forms.ImageList(Me.components)
        Me.SplitButtonDemoToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.SplitButton1 = New isr.Controls.SplitButton()
        Me.groupBox1.SuspendLayout()
        Me.groupBox2.SuspendLayout()
        CType(Me.SplitWidth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitHeight, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitButtonDropDown.SuspendLayout()
        Me.SuspendLayout()
        ' 
        ' groupBox1
        ' 
        Me.groupBox1.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
        Me.groupBox1.Controls.Add(Me.groupBox2)
        Me.groupBox1.Controls.Add(Me.label2)
        Me.groupBox1.Controls.Add(Me.SplitWidth)
        Me.groupBox1.Controls.Add(Me.label1)
        Me.groupBox1.Controls.Add(Me.SplitHeight)
        Me.groupBox1.Controls.Add(Me.FillSplitHeight)
        Me.groupBox1.Controls.Add(Me.CalculateSplitRect)
        Me.groupBox1.Controls.Add(Me.AlwaysHoverChange)
        Me.groupBox1.Controls.Add(Me.AlwaysDropDown)
        Me.groupBox1.Controls.Add(Me.SplitEnabled)
        Me.groupBox1.Controls.Add(Me.SplitButton1)
        Me.groupBox1.Location = New System.Drawing.Point(13, 13)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(145, 334)
        Me.groupBox1.TabIndex = 0
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "SplitButton"
        ' 
        ' groupBox2
        ' 
        Me.groupBox2.Controls.Add(Me.InfoText)
        Me.groupBox2.Location = New System.Drawing.Point(9, 220)
        Me.groupBox2.Name = "groupBox2"
        Me.groupBox2.Size = New System.Drawing.Size(125, 72)
        Me.groupBox2.TabIndex = 14
        Me.groupBox2.TabStop = False
        Me.groupBox2.Text = "Information"
        ' 
        ' InfoText
        ' 
        Me.InfoText.Anchor = (CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
        Me.InfoText.Location = New System.Drawing.Point(7, 20)
        Me.InfoText.Name = "InfoText"
        Me.InfoText.Size = New System.Drawing.Size(112, 49)
        Me.InfoText.TabIndex = 0
        ' 
        ' label2
        ' 
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(6, 187)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(61, 13)
        Me.label2.TabIndex = 13
        Me.label2.Text = "Split Width:"
        ' 
        ' SplitWidth
        ' 
        Me.SplitWidth.Location = New System.Drawing.Point(85, 185)
        Me.SplitWidth.Maximum = New Decimal(New Integer() {125, 0, 0, 0})
        Me.SplitWidth.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.SplitWidth.Name = "SplitWidth"
        Me.SplitWidth.Size = New System.Drawing.Size(49, 20)
        Me.SplitWidth.TabIndex = 12
        Me.SplitButtonDemoToolTip.SetToolTip(Me.SplitWidth, "The split width (ignored if CalculateSplitRect is setted to true).")
        Me.SplitWidth.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '			Me.SplitWidth.ValueChanged += New System.EventHandler(Me.SplitWidth_ValueChanged);
        ' 
        ' label1
        ' 
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(6, 158)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(64, 13)
        Me.label1.TabIndex = 11
        Me.label1.Text = "Split Height:"
        ' 
        ' SplitHeight
        ' 
        Me.SplitHeight.Location = New System.Drawing.Point(85, 156)
        Me.SplitHeight.Maximum = New Decimal(New Integer() {26, 0, 0, 0})
        Me.SplitHeight.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.SplitHeight.Name = "SplitHeight"
        Me.SplitHeight.Size = New System.Drawing.Size(49, 20)
        Me.SplitHeight.TabIndex = 10
        Me.SplitButtonDemoToolTip.SetToolTip(Me.SplitHeight, "The split height (ignored if CalculateSplitRect is setted to true).")
        Me.SplitHeight.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '			Me.SplitHeight.ValueChanged += New System.EventHandler(Me.SplitHeight_ValueChanged);
        ' 
        ' FillSplitHeight
        ' 
        Me.FillSplitHeight.AutoSize = True
        Me.FillSplitHeight.Checked = True
        Me.FillSplitHeight.CheckState = System.Windows.Forms.CheckState.Checked
        Me.FillSplitHeight.Location = New System.Drawing.Point(9, 125)
        Me.FillSplitHeight.Name = "FillSplitHeight"
        Me.FillSplitHeight.Size = New System.Drawing.Size(89, 17)
        Me.FillSplitHeight.TabIndex = 9
        Me.FillSplitHeight.Text = "FillSplitHeight"
        Me.SplitButtonDemoToolTip.SetToolTip(Me.FillSplitHeight, "Indicates whether the split height must be filled to the button height even if th" & "e split image height is lower.")
        Me.FillSplitHeight.UseVisualStyleBackColor = True
        '			Me.FillSplitHeight.CheckedChanged += New System.EventHandler(Me.FillSplitHeight_CheckedChanged);
        ' 
        ' CalculateSplitRect
        ' 
        Me.CalculateSplitRect.AutoSize = True
        Me.CalculateSplitRect.Checked = True
        Me.CalculateSplitRect.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CalculateSplitRect.Location = New System.Drawing.Point(9, 101)
        Me.CalculateSplitRect.Name = "CalculateSplitRect"
        Me.CalculateSplitRect.Size = New System.Drawing.Size(113, 17)
        Me.CalculateSplitRect.TabIndex = 8
        Me.CalculateSplitRect.Text = "CalculateSplitRect"
        Me.SplitButtonDemoToolTip.SetToolTip(Me.CalculateSplitRect, "Indicates whether the split rectange must be calculated (basing on Split image si" & "ze).")
        Me.CalculateSplitRect.UseVisualStyleBackColor = True
        '			Me.CalculateSplitRect.CheckedChanged += New System.EventHandler(Me.CalculateSplitRect_CheckedChanged);
        ' 
        ' AlwaysHoverChange
        ' 
        Me.AlwaysHoverChange.AutoSize = True
        Me.AlwaysHoverChange.Location = New System.Drawing.Point(9, 77)
        Me.AlwaysHoverChange.Name = "AlwaysHoverChange"
        Me.AlwaysHoverChange.Size = New System.Drawing.Size(125, 17)
        Me.AlwaysHoverChange.TabIndex = 7
        Me.AlwaysHoverChange.Text = "AlwaysHoverChange"
        Me.SplitButtonDemoToolTip.SetToolTip(Me.AlwaysHoverChange, "Indicates whether the SplitButton always shows the Hover image status in the spli" & "t part even if the button part of the SplitButton is hovered.")
        Me.AlwaysHoverChange.UseVisualStyleBackColor = True
        '			Me.AlwaysHoverChange.CheckedChanged += New System.EventHandler(Me.AlwaysHoverChange_CheckedChanged);
        ' 
        ' AlwaysDropDown
        ' 
        Me.AlwaysDropDown.AutoSize = True
        Me.AlwaysDropDown.Location = New System.Drawing.Point(9, 53)
        Me.AlwaysDropDown.Name = "AlwaysDropDown"
        Me.AlwaysDropDown.Size = New System.Drawing.Size(110, 17)
        Me.AlwaysDropDown.TabIndex = 6
        Me.AlwaysDropDown.Text = "AlwaysDropDown"
        Me.SplitButtonDemoToolTip.SetToolTip(Me.AlwaysDropDown, "Indicates whether the SplitButton always shows the drop down menu even if the but" & "ton part of the SplitButton is clicked.")
        Me.AlwaysDropDown.UseVisualStyleBackColor = True
        '			Me.AlwaysDropDown.CheckedChanged += New System.EventHandler(Me.AlwaysDropDown_CheckedChanged);
        ' 
        ' SplitEnabled
        ' 
        Me.SplitEnabled.AutoSize = True
        Me.SplitEnabled.Checked = True
        Me.SplitEnabled.CheckState = System.Windows.Forms.CheckState.Checked
        Me.SplitEnabled.Location = New System.Drawing.Point(9, 29)
        Me.SplitEnabled.Name = "SplitEnabled"
        Me.SplitEnabled.Size = New System.Drawing.Size(65, 17)
        Me.SplitEnabled.TabIndex = 5
        Me.SplitEnabled.Text = "Enabled"
        Me.SplitEnabled.UseVisualStyleBackColor = True
        '			Me.SplitEnabled.CheckedChanged += New System.EventHandler(Me.SplitEnabled_CheckedChanged);
        ' 
        ' SplitButtonDropDown
        ' 
        Me.SplitButtonDropDown.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripMenuItem1, Me.toolStripSeparator1, Me.toolStripMenuItem2, Me.toolStripMenuItem3})
        Me.SplitButtonDropDown.Name = "SplitButtonDropDown"
        Me.SplitButtonDropDown.Size = New System.Drawing.Size(189, 76)
        ' 
        ' toolStripMenuItem1
        ' 
        Me.toolStripMenuItem1.Name = "toolStripMenuItem1"
        Me.toolStripMenuItem1.Size = New System.Drawing.Size(188, 22)
        Me.toolStripMenuItem1.Text = "SplitButton DropDown 1"
        ' 
        ' toolStripSeparator1
        ' 
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(185, 6)
        ' 
        ' toolStripMenuItem2
        ' 
        Me.toolStripMenuItem2.Name = "toolStripMenuItem2"
        Me.toolStripMenuItem2.Size = New System.Drawing.Size(188, 22)
        Me.toolStripMenuItem2.Text = "SplitButton DropDown 2"
        ' 
        ' toolStripMenuItem3
        ' 
        Me.toolStripMenuItem3.Name = "toolStripMenuItem3"
        Me.toolStripMenuItem3.Size = New System.Drawing.Size(188, 22)
        Me.toolStripMenuItem3.Text = "SplitButton DropDown 3"
        ' 
        ' SplitButtonImages
        ' 
        Me.SplitButtonImages.ImageStream = (CType(resources.GetObject("SplitButtonImages.ImageStream"), System.Windows.Forms.ImageListStreamer))
        Me.SplitButtonImages.TransparentColor = System.Drawing.Color.Transparent
        Me.SplitButtonImages.Images.SetKeyName(0, "SplitButtonImage.gif")
        Me.SplitButtonImages.Images.SetKeyName(1, "SplitButtonHoverImage.gif")
        Me.SplitButtonImages.Images.SetKeyName(2, "SplitButtonClickedImage.gif")
        Me.SplitButtonImages.Images.SetKeyName(3, "SplitButtonDisabledImage.gif")
        ' 
        ' SplitButton1
        ' 
        Me.SplitButton1.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
        Me.SplitButton1.ClickedImage = "SplitButtonClickedImage.gif"
        Me.SplitButton1.ContextMenuStrip = Me.SplitButtonDropDown
        Me.SplitButton1.DisabledImage = "SplitButtonDisabledImage.gif"
        Me.SplitButton1.FocusedImage = "Focused"
        Me.SplitButton1.HoverImage = "SplitButtonHoverImage.gif"
        Me.SplitButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.SplitButton1.ImageKey = "SplitButtonImage.gif"
        Me.SplitButton1.Location = New System.Drawing.Point(9, 302)
        Me.SplitButton1.Name = "SplitButton1"
        Me.SplitButton1.NormalImage = "SplitButtonImage.gif"
        Me.SplitButton1.Size = New System.Drawing.Size(125, 26)
        Me.SplitButton1.TabIndex = 0
        Me.SplitButton1.Text = "Split Button Test"
        Me.SplitButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.SplitButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.SplitButton1.UseVisualStyleBackColor = True
        '			Me.SplitButton1.Click += New System.EventHandler(Me.SplitButton1_Click);
        '			Me.SplitButton1.ButtonClick += New System.EventHandler(Me.SplitButton1_ButtonClick);
        ' 
        ' SplitButtonDemoForm
        ' 
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(170, 359)
        Me.Controls.Add(Me.groupBox1)
        Me.Name = "SplitButtonDemoForm"
        Me.Text = "SplitButton"
        Me.groupBox1.ResumeLayout(False)
        Me.groupBox1.PerformLayout()
        Me.groupBox2.ResumeLayout(False)
        CType(Me.SplitWidth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitHeight, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitButtonDropDown.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private WithEvents SplitButton1 As isr.Controls.SplitButton
    Private groupBox1 As System.Windows.Forms.GroupBox
    Private WithEvents SplitEnabled As System.Windows.Forms.CheckBox
    Private WithEvents AlwaysDropDown As System.Windows.Forms.CheckBox
    Private SplitButtonImages As System.Windows.Forms.ImageList
    Private label2 As System.Windows.Forms.Label
    Private WithEvents SplitWidth As System.Windows.Forms.NumericUpDown
    Private label1 As System.Windows.Forms.Label
    Private WithEvents SplitHeight As System.Windows.Forms.NumericUpDown
    Private WithEvents FillSplitHeight As System.Windows.Forms.CheckBox
    Private SplitButtonDemoToolTip As System.Windows.Forms.ToolTip
    Private WithEvents CalculateSplitRect As System.Windows.Forms.CheckBox
    Private WithEvents AlwaysHoverChange As System.Windows.Forms.CheckBox
    Private groupBox2 As System.Windows.Forms.GroupBox
    Private InfoText As System.Windows.Forms.Label
    Private SplitButtonDropDown As System.Windows.Forms.ContextMenuStrip
    Private WithEvents toolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Private toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents toolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
End Class

