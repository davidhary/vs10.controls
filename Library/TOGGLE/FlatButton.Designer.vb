Partial Class FlatButton
  Inherits System.Windows.Forms.Button

  <System.Diagnostics.DebuggerNonUserCode()> _
  Public Sub New(ByVal container As System.ComponentModel.IContainer)
    MyClass.New()

    'Required for Windows.Forms Class Composition Designer support
    If Container IsNot Nothing Then
      Container.Add(Me)
    End If

  End Sub

  <System.Diagnostics.DebuggerNonUserCode()> _
  Public Sub New()
    MyBase.New()

    'This call is required by the Component Designer.
    InitializeComponent()

  End Sub

  'Component overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Component Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Component Designer
  'It can be modified using the Component Designer.
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FlatButton))
    Me.components = New System.ComponentModel.Container
    Me.SuspendLayout()
    Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
    '
    'ImageList
    '
    Me.ImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
    Me.ImageList.ImageSize = New System.Drawing.Size(9, 9)
    Me.ImageList.ImageStream = CType(resources.GetObject("flatButtonsImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
    Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
    '
    'FlatButton
    '
    Me.FlatStyle = System.Windows.Forms.FlatStyle.Flat
    Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Size = New System.Drawing.Size(57, 19)
    Me.UseMnemonic = True
    Me.ResumeLayout(False)

  End Sub

End Class
