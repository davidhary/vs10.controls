Imports System.ComponentModel
''' <summary>
''' Provides a user interface for a <see cref="isr.Core.IConnectableInterface">connectable interface</see>
''' such as a VISA Interface.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
''' created.
''' </history>
''' <history date="01/21/2011" by="David Hary" revision="1.2.4038.x">
''' Update to using the <see cref="isr.Core.IConnectableInterface">connectable interface.</see>
''' </history>
<System.ComponentModel.Description("Connectable Interface Panel - Windows Forms Custom Control")> _
Public Class InterfacePanel
    Implements isr.Core.IMessageObserver

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        'onInstantiate()
        ' prevent the resource chooser from being connectible.
        'Me.interfaceChooser.Connectible = True
        'Me.interfaceChooser.Clearable = True
        'Me.interfaceChooser.Searchable = True
        'Me.resourceChooser.Connectible = False
        'Me.resourceChooser.Clearable = False
        'Me.resourceChooser.Searchable = True
        ' disable until the connectible interface is set
        Me.Enabled = False
        Me._interfaceChooser.Connectible = True
        Me._interfaceChooser.Clearable = True

    End Sub

    ''' <summary>Cleans up managed components.</summary>
    ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
    Private Sub onDisposeManagedResources()

        _connectableInterface = Nothing

    End Sub

#End Region

#Region " I MESSAGE PUBLISHER "

    ''' <summary>
    ''' Raises the Message Available event.
    ''' </summary>
    ''' <param name="e">Specifies the event arguments.</param>
    Public Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As Boolean Implements isr.Core.IMessageObserver.OnMessageAvailable
        Me._statusPanel.Text = e.Synopsis
        Me._messagesBox.PrependMessage(e.Details)
        Return True
    End Function

    ''' <summary>
    ''' Raises a message.
    ''' </summary>
    ''' <param name="traceLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">trace level</see>.</param>
    ''' <param name="synopsis">Specifies the message short synopsis.</param>
    ''' <param name="format">Specifies the format for building the message detains</param>
    ''' <param name="args">Specifies the format arguments.</param>
    ''' <remarks></remarks>
    Public Overridable Function OnMessageAvailable(ByVal traceLevel As Diagnostics.TraceEventType, _
                                  ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return Me.OnMessageAvailable(traceLevel, traceLevel, synopsis, format, args)
    End Function

    ''' <summary>
    ''' Raises the Message Available event.
    ''' </summary>
    ''' <param name="broadcastLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">broadcast level</see>.</param>
    ''' <param name="traceLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">trace level</see>.</param>
    ''' <param name="synopsis">Specifies the message Synopsis.</param>
    ''' <param name="format">Specifies the message details.</param>
    ''' <param name="args">Arguments to use in the format statement.</param>
    Public Overridable Function OnMessageAvailable(ByVal broadcastLevel As Diagnostics.TraceEventType, ByVal traceLevel As Diagnostics.TraceEventType, _
                                  ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return OnMessageAvailable(New isr.Core.MessageEventArgs(broadcastLevel, traceLevel, synopsis, format, args))
    End Function

#End Region

#Region " INTERFACE PROPERTIES AND METHODS "

    ''' <summary>Opens a GPIB VISA interface for the specified resource.</summary>
    ''' <param name="resourceName">Specifies the interface resource name.</param>
    ''' <returns>Returns True if success or false if it failed opening the session.</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Public Function ConnectInterface(ByVal resourceName As String) As Boolean

        Try

            Me.OnMessageAvailable(TraceEventType.Information, "CONNECTING", "Connecting to {0}", resourceName)
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

            If Not Me.ConnectableInterface.Connect(resourceName) Then

                ' if failed, alert and release the connect button
                Me._interfaceChooser.IsConnected = False

                Me._resourceChooser.Enabled = False
                Me._clearSelectedResourceButton.Enabled = False
                Me._clearAllResourcesButton.Enabled = False

            Else

                ' connect the connector.
                Me._interfaceChooser.IsConnected = True
                Me.OnMessageAvailable(TraceEventType.Information, "CONNECTIED", "Connected to {0}", resourceName)

                ' display the selected GPIB resources.
                Me._resourceChooser.Enabled = True
                Dim inrefaceNames As String() = Me.ConnectableInterface.FindInterfaces
                Me._resourceChooser.DisplayNames(inrefaceNames)

                If Me.ConnectableInterface.FailedFindingResources Then
                    Me.OnMessageAvailable(TraceEventType.Warning, "FAILED FETCHING THE LIST OF INTERFACES", _
                                          "Failed fetching the list of interfaes because: {0}. Try entering Resource String in the format 'GPIB[board]::INTFC'.", Me.ConnectableInterface.ResourceFindingFailureSymptom)
                ElseIf inrefaceNames Is Nothing OrElse inrefaceNames.Length <= 0 Then
                    Me.OnMessageAvailable(TraceEventType.Warning, "VISA MANAGER LOCATED NO INTERFACES", _
                                          "Failed locating interfaces because: {0}.", Me.ConnectableInterface.ResourceFindingFailureSymptom)
                End If

            End If

        Catch ex As ArgumentException

            ' close to meet strong guarantees
            Try
                Me.Disconnect()
            Finally
            End Try

            Me.OnMessageAvailable(TraceEventType.Error, "EXCPETION OCCURRED OPENING", "Excpetion occurred opening '{0}'. Details: {1}.", resourceName, ex)

        Catch ex As Exception

            ' close to meet strong guarantees
            Try
                Me.Disconnect()
            Finally
            End Try

            Me.OnMessageAvailable(TraceEventType.Error, "EXCPETION OCCURRED OPENING", "Excpetion occurred opening '{0}'. Details: {1}.", resourceName, ex)

        Finally

            Me._interfaceChooser.Invalidate()
            Me._resourceChooser.Invalidate()
            Me._clearSelectedResourceButton.Invalidate()
            Me._clearAllResourcesButton.Invalidate()

            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

        End Try

        Return _connectableInterface IsNot Nothing

    End Function

    ''' <summary>Closes the instance.</summary>
    ''' <returns>A Boolean data type</returns>
    ''' <remarks>Use this method to close the instance.  The method returns true if success or 
    '''   false if it failed closing the instance.</remarks>
    Public Function Disconnect() As Boolean

        If _connectableInterface IsNot Nothing Then

            Me.OnMessageAvailable(TraceEventType.Information, "DISCONNECTING", "Disconnecting from {0}", _connectableInterface.ResourceName)

            _connectableInterface.Disconnect()

        End If

    End Function

    Private WithEvents _connectableInterface As isr.Core.IConnectableInterface
    ''' <summary>Gets or sets reference to the Gpib interface for this panel.</summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public Property ConnectableInterface() As isr.Core.IConnectableInterface
        Get
            Return _connectableInterface
        End Get
        Set(ByVal Value As isr.Core.IConnectableInterface)
            _connectableInterface = Value
            Me.Enabled = Value IsNot Nothing
        End Set
    End Property

    ''' <summary>Returns true if the interface is connected.
    ''' </summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public Property IsConnected() As Boolean
        Get
            Return Me._interfaceChooser.IsConnected AndAlso Me._connectableInterface IsNot Nothing
        End Get
        Set(ByVal value As Boolean)
            Me._interfaceChooser.IsConnected = value
            With Me._resourceChooser
                .Enabled = value
                .Visible = True
                .Invalidate()
            End With
            With Me._clearSelectedResourceButton
                .Enabled = value
                .Visible = True
                .Invalidate()
            End With
            With Me._clearAllResourcesButton
                .Enabled = value
                .Visible = True
                .Invalidate()
            End With
        End Set

    End Property

    Private Sub _connectableInterface_ConnectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _connectableInterface.ConnectionChanged
        Me.IsConnected = CType(sender, isr.Core.IConnectableInterface).IsConnected
        If Me.IsConnected Then
            Me.OnMessageAvailable(TraceEventType.Information, "CONNECTED", "Connected.")
            Me._messagesBox.PrependMessage(_connectableInterface.Identity)
        Else
            Me.OnMessageAvailable(TraceEventType.Information, "DISCONNECTED", "Disconnected.")
        End If
    End Sub

    Private Sub _connectableInterface_MessageAvailable(ByVal sender As Object, ByVal e As Core.MessageEventArgs) Handles _connectableInterface.MessageAvailable
        Me.OnMessageAvailable(e)
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles MyBase.Load
        Me._statusPanel.Text = "Find and select an interface."
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    Private Sub clearSelectedResourceButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _clearSelectedResourceButton.Click
        ' Transmit the SDC command to the interface.
        Me.ConnectableInterface.ClearSelectiveResource(Me._resourceChooser.SelectedName)
    End Sub

    Private Sub deviceClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _clearAllResourcesButton.Click
        Me.ConnectableInterface.ClearResources()
    End Sub

#End Region

#Region " RESOURCE CHOOSER EVENT HANDLERS "

    ''' <summary>
    ''' Displays resource names.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub findResources()

        Try

            Me._clearAllResourcesButton.Enabled = False

            ' get the list of available GPIB interfaces 
            Dim names() As String = Me.ConnectableInterface.FindResources()
            If names Is Nothing Then

                ' send a verbose message.
                Me.OnMessageAvailable(TraceEventType.Warning, "RESOURCE NOT FOUND", "Resource names are empty")

            ElseIf names.Length = 0 Then

                ' send a verbose message.
                Me.OnMessageAvailable(TraceEventType.Warning, "RESOURCES NOT FOUND", "Resources not found")

                ' check if failure occurred fetching resources
                If Me.ConnectableInterface.FailedFindingResources Then
                    Me.OnMessageAvailable(TraceEventType.Warning, "FAILED FETCHING THE LIST OF RESOURCES", _
                                          "Failed fetching the list of resources because: {0}. Try entering Resource String in the format 'GPIB[board]::address::INSTR'.", Me.ConnectableInterface.ResourceFindingFailureSymptom)
                ElseIf names Is Nothing OrElse names.Length <= 0 Then
                    Me.OnMessageAvailable(TraceEventType.Warning, "VISA MANAGER LOCATED NO RESOURCES", _
                                          "Failed locating resources because: {0}.", Me.ConnectableInterface.ResourceFindingFailureSymptom)
                End If

            Else

                Me._clearAllResourcesButton.Enabled = True
                Me._resourceChooser.DisplayNames(names)

                ' send a verbose message.
                Me.OnMessageAvailable(TraceEventType.Information, "RESOURCE NAMES LISTED", "Resource names listed")

            End If

        Catch ex As System.ArgumentException

            Me.OnMessageAvailable(TraceEventType.Error, "Error occurred", "Failed finding or displaying resources.  Connect the resources(s) and click Find. Details: {0}.", ex)

        End Try

    End Sub

    Private Sub resourceChooser_Clear(ByVal sender As Object, ByVal e As System.EventArgs) Handles _resourceChooser.Clear
        Me.ConnectableInterface.ClearSelectiveResource(Me._resourceChooser.SelectedName)
    End Sub

    ''' <summary>
    ''' Displays resource names.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub resourceChooser_FindNames(ByVal sender As Object, ByVal e As System.EventArgs) Handles _resourceChooser.FindNames
        findResources()
    End Sub

    Private Sub resourceChooser_Selected(ByVal sender As Object, ByVal e As System.EventArgs) Handles _resourceChooser.Selected
        Me._clearSelectedResourceButton.Enabled = True
        Me.OnMessageAvailable(TraceEventType.Information, "RESOURCE SELECTED", "Selected resource {0}", Me._resourceChooser.SelectedName)
    End Sub

#End Region

#Region " INTERFACE CHOOSER EVENT HANDLERS "

    ''' <summary>Display the interface names based on the last interface type.
    ''' </summary>
    Public Sub DisplayInterfaceNames()
        Try

            ' get the list of available GPIB interfaces 
            Dim names() As String = Me.ConnectableInterface.FindInterfaces()

            If names Is Nothing Then

                Me.OnMessageAvailable(TraceEventType.Warning, "INTERFACES NOT FOUND -- EMPTY", "Interface not found -- empty.")

            ElseIf names.Length = 0 Then

                Me.OnMessageAvailable(TraceEventType.Warning, "INTERFACES NOT FOUND", "Interface not found.")
                Me._interfaceChooser.DisplayNames(names)

                If Me.ConnectableInterface.FailedFindingResources Then
                    Me.OnMessageAvailable(TraceEventType.Warning, "FAILED FETCHING THE LIST OF INTERFACES", _
                                          "Failed fetching the list of interfaes because: {0}. Try entering Resource String in the format 'GPIB[board]::INTFC'.", Me.ConnectableInterface.ResourceFindingFailureSymptom)
                ElseIf names Is Nothing OrElse names.Length <= 0 Then
                    Me.OnMessageAvailable(TraceEventType.Warning, "VISA MANAGER LOCATED NO INTERFACES", _
                                          "Failed locating interfaces because: {0}.", Me.ConnectableInterface.ResourceFindingFailureSymptom)
                End If

            Else

                Me._interfaceChooser.DisplayNames(names)
                Me.OnMessageAvailable(TraceEventType.Information, "INTERFACE NAMES LISTED", "Interface names listed")

            End If

        Catch ex As System.ArgumentException

            Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION FINDING INTERACES", "Failed finding or listing interfaces.  Connect the interface(s) and click Find. Details: {0}", ex)

        End Try

    End Sub

    ''' <summary>
    ''' Clears the interface by issueing an interface clear.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Private Sub interfaceChooser_Clear(ByVal sender As Object, ByVal e As System.EventArgs) Handles _interfaceChooser.Clear
        If _connectableInterface Is Nothing Then
            Exit Sub
        End If
        Try
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
            Me.ConnectableInterface.ClearResource()
        Catch ex As Exception
            Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING", "Exception occurred clearing. Details: {0}", ex)
            _messagesBox.PrependMessage(ex.ToString)
        Finally
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Connects the interface.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub interfaceChooser_Connect(ByVal sender As Object, ByVal e As System.EventArgs) Handles _interfaceChooser.Connect
        Dim resourcename As String = Me._interfaceChooser.SelectedName
        Me.ConnectInterface(resourcename)
        Me.findResources()
    End Sub

    ''' <summary>
    ''' Disconnects the interface.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub interfaceChooser_Disconnect(ByVal sender As Object, ByVal e As System.EventArgs) Handles _interfaceChooser.Disconnect
        Me.Disconnect()
    End Sub

    ''' <summary>
    ''' Displays available interface names.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub interfaceChooser_FindNames(ByVal sender As Object, ByVal e As System.EventArgs) Handles _interfaceChooser.FindNames
        Me.DisplayInterfaceNames()
    End Sub

    ''' <summary>
    ''' Notifies that an interface name was selected.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub interfaceChooser_Selected(ByVal sender As Object, ByVal e As System.EventArgs) Handles _interfaceChooser.Selected
        Me.OnMessageAvailable(TraceEventType.Information, "SELECTED INTERFACE", "Selected interface {0}", _interfaceChooser.SelectedName)
    End Sub

#End Region

End Class
