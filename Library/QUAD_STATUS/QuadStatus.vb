﻿Imports System.ComponentModel
''' <summary>
''' Display four status images.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <remarks></remarks>
''' <history date="02/20/10" by="David Hary" revision="1.0.3703.x">
''' created.
''' </history>
<System.ComponentModel.Description("Quad Status Control"), _
    System.Drawing.ToolboxBitmap(GetType(QuadStatus))> _
Public Class QuadStatus

    Implements INotifyPropertyChanged

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()

        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        Me.State = QuadState.Unknown

    End Sub

#End Region

    Private _state As QuadState
    ''' <summary>
    ''' Gets or sets the <see cref="QuadStatus">Quad Status</see> <see cref="QuadState">state</see>.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.DefaultValue(QuadState.Unknown), _
      System.ComponentModel.Description("Gets or sets the Quad Status state")> _
    Public Property State() As QuadState
        Get
            Return _state
        End Get
        Set(ByVal value As QuadState)
            _state = value
            Me.BackgroundImage = Me._imageList.Images(value.ToString)
        End Set
    End Property

    ''' <summary>
    ''' Fix the size to the image size.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub QuadStatus_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Me.Size = _imageList.ImageSize
    End Sub

End Class

#Region " TYPES "

''' <summary>Enumerates the states.</summary>
Public Enum QuadState
  None
  Unknown
  Okay
  Problem
  Broken
End Enum

#End Region