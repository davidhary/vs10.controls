﻿Imports System.ComponentModel

''' <summary>
''' Database connector control.
''' Displays and sets the connection string and status.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/21/2009" by="David Hary" revision="7.0.3581.x">
''' created.
''' </history>
<System.ComponentModel.Description("Database Connector Control"), _
    System.Drawing.ToolboxBitmap(GetType(DatabaseConnector)), _
    System.ComponentModel.DefaultEvent("ConnectionStringChanged")> _
Public Class DatabaseConnector

    Implements INotifyPropertyChanged

#Region " INotifyPropertyChanged "

    ''' <summary>
    ''' Raised whenever a property is changed.
    ''' </summary>
    ''' <remarks></remarks>
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary>
    ''' Raised to notify the binadable object of a change.
    ''' </summary>
    ''' <param name="name"></param>
    ''' <remarks></remarks>
    Protected Sub OnPropertyChanged(ByVal name As String)
        If PropertyChangedEvent IsNot Nothing Then PropertyChangedEvent(Me, New PropertyChangedEventArgs(name))
    End Sub

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' onInstantiate()
        _lastConnectionString = ""
        _connectionAttemptDetails = ""
        Me._connectionStatusLed.Enabled = False

        ' force default value to false.
        MyBase.Enabled = False

    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' Raised whenever the apply button is clicked. 
    ''' </summary>
    ''' <remarks></remarks>
    Public Event ApplyClicked As EventHandler(Of System.EventArgs)

    ''' <summary>
    ''' Raises the <see cref="ApplyClicked">apply clicked event.</see>
    ''' </summary>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Private Sub onApplyClicked(ByVal args As System.EventArgs)
        If Me.Enabled AndAlso ConnectionStringChangedEvent IsNot Nothing Then ConnectionStringChangedEvent(Me, args)
    End Sub

    ''' <summary>
    ''' Raise the <see cref="ApplyClicked">apply event</see>
    ''' for thecalling method handiling of the connection code.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub _applyButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _applyButton.Click
        onApplyClicked(System.EventArgs.Empty)
    End Sub

    ''' <summary>
    ''' Raised whenever the connection changed. 
    ''' </summary>
    ''' <remarks></remarks>
    Public Event ConnectionChanged As EventHandler(Of System.EventArgs)

    ''' <summary>
    ''' Raises the <see cref="ConnectionChanged">connection changed event.</see>
    ''' </summary>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Private Sub onConnectionChanged(ByVal args As System.EventArgs)
        If Me.Enabled AndAlso ConnectionChangedEvent IsNot Nothing Then ConnectionChangedEvent(Me, args)
    End Sub

    ''' <summary>
    ''' Raised whenever the connection string is changed. 
    ''' </summary>
    ''' <remarks></remarks>
    Public Event ConnectionStringChanged As EventHandler(Of System.EventArgs)

    ''' <summary>
    ''' Raises the <see cref="ConnectionStringChanged">connection string changed event.</see>
    ''' </summary>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Private Sub onConnectionStringChanged(ByVal args As System.EventArgs)
        If Me.Enabled AndAlso ConnectionStringChangedEvent IsNot Nothing Then ConnectionStringChangedEvent(Me, args)
    End Sub

    ''' <summary>
    ''' Refresh the status. This raises the connection changed event.
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub Refresh()
        MyBase.Refresh()
        Me.onConnectionStringChanged(System.EventArgs.Empty)
    End Sub

#End Region

#Region " APPEARANCE PROPERTIES "

    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Failure Color."), _
      System.ComponentModel.DefaultValue(GetType(System.Drawing.Color), "Color.Red")> _
    Public Property FailureColor() As System.Drawing.Color
        Get
            Return Me._connectionStatusLed.OffColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            If Me.FailureColor <> value Then
                Me._connectionStatusLed.OffColor = value
                OnPropertyChanged("FailureColor")
            End If
        End Set
    End Property

    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Appearance"), _
      System.ComponentModel.Description("Success Color."), _
      System.ComponentModel.DefaultValue(GetType(System.Drawing.Color), "Color.Green")> _
    Public Property SuccessColor() As System.Drawing.Color
        Get
            Return Me._connectionStatusLed.OnColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            If Me._connectionStatusLed.OnColor <> value Then
                Me._connectionStatusLed.OnColor = value
                OnPropertyChanged("SuccessColor")
            End If
        End Set
    End Property

#End Region

#Region " DATA PROPERTIES "

    Private _lastConnectionString As String
    <System.ComponentModel.Browsable(True), _
      System.ComponentModel.Category("Data"), _
      System.ComponentModel.Description("Connection string."), _
      System.ComponentModel.DefaultValue("")> _
    Public Property ConnectionString() As String
        Get
            Return _connectionStringTextBox.Text
        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                value = value.Trim
                If _lastConnectionString <> value Then
                    ' clear the status of the last connection
                    Me.ClearLastConnection()
                    _lastConnectionString = value
                    _connectionStringTextBox.Text = value
                    Me.onConnectionStringChanged(System.EventArgs.Empty)
                    OnPropertyChanged("ConnectionString")
                End If
            End If
        End Set
    End Property

#End Region

#Region " BEHAVIOR PROPERTIES "

    Private _connectionAttemptDetails As String
    ''' <summary>
    ''' Gets or sets the details of the last attempt to connect.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public Property ConnectionAttemptDetails() As String
        Get
            Return _connectionAttemptDetails
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                value = ""
            End If
            _connectionAttemptDetails = value
        End Set
    End Property

    ''' <summary>
    ''' Gets condition indicating if any connection attempt was made.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public ReadOnly Property ConnectionAttempted() As Boolean
        Get
            Return _lastConnectionSucceeded.HasValue
        End Get
    End Property

    ''' <summary>
    ''' Clears the status of the last connection.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearLastConnection()
        _lastConnectionSucceeded = New Boolean?
        _connectionAttemptDetails = ""
        Me._connectionStatusLed.Enabled = False
        Me._connectionStatusLed.Refresh()
    End Sub

    Private _lastConnectionSucceeded As Boolean?
    ''' <summary>
    ''' Gets or sets the status of the last connection.
    ''' </summary>
    ''' <remarks></remarks>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public Property LastConnectionSucceeded() As Boolean
        Get
            Return _lastConnectionSucceeded.GetValueOrDefault(False)
        End Get
        Set(ByVal value As Boolean)
            If Not _lastConnectionSucceeded.HasValue OrElse _lastConnectionSucceeded.Value <> value Then
                Me._connectionStatusLed.Enabled = True
                _lastConnectionSucceeded = value
                Me._connectionStatusLed.Active = value
                Me.onConnectionChanged(System.EventArgs.Empty)
            End If
        End Set
    End Property

#End Region

End Class
