﻿Imports System.Drawing
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Security.Permissions

<DesignerCategory("code"), System.ComponentModel.Description("Extended Radio Button"), _
    System.Drawing.ToolboxBitmap(GetType(RadioButton))> _
Public Class RadioButton
    Inherits Windows.Forms.RadioButton

    Private _readonly As Boolean
    ''' <summary>
    ''' Gets or sets the read only property
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DefaultValue(False)> _
    <Category("Behavior")> _
    <Description("Indicates whether the radio button is read only.")> _
    Public Property [ReadOnly]() As Boolean
        Get
            Return _readonly
        End Get
        Set(ByVal value As Boolean)
            If _readonly <> value Then
                _readonly = value
            End If
        End Set
    End Property

    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        If Not _readonly Then
            MyBase.OnClick(e)
        End If
    End Sub

End Class
