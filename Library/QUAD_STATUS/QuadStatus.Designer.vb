﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class QuadStatus
    Inherits UserControlBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(QuadStatus))
        Me._imageList = New System.Windows.Forms.ImageList(Me.components)
        Me.SuspendLayout()
        '
        '_imageList
        '
        Me._imageList.ImageStream = CType(resources.GetObject("_imageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me._imageList.TransparentColor = System.Drawing.Color.Transparent
        Me._imageList.Images.SetKeyName(0, "Okay")
        Me._imageList.Images.SetKeyName(1, "Broken")
        Me._imageList.Images.SetKeyName(2, "Unknown")
        Me._imageList.Images.SetKeyName(3, "Problem")
        '
        'QuadStatus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Name = "QuadStatus"
        Me.Size = New System.Drawing.Size(242, 188)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents _imageList As System.Windows.Forms.ImageList

End Class
