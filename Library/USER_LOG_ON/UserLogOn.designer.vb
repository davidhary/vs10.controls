﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserLogOn
    Inherits UserControlBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._passwordTextBox = New System.Windows.Forms.TextBox
        Me._passwordTextBoxLabel = New System.Windows.Forms.Label
        Me._userNameTextBox = New System.Windows.Forms.TextBox
        Me._userNameTextBoxLabel = New System.Windows.Forms.Label
        Me._logOnButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        '_passwordTextBox
        '
        Me._passwordTextBox.Location = New System.Drawing.Point(79, 24)
        Me._passwordTextBox.Name = "_passwordTextBox"
        Me._passwordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me._passwordTextBox.Size = New System.Drawing.Size(70, 20)
        Me._passwordTextBox.TabIndex = 3
        '
        '_passwordTextBoxLabel
        '
        Me._passwordTextBoxLabel.Location = New System.Drawing.Point(4, 24)
        Me._passwordTextBoxLabel.Name = "_passwordTextBoxLabel"
        Me._passwordTextBoxLabel.Size = New System.Drawing.Size(77, 20)
        Me._passwordTextBoxLabel.TabIndex = 2
        Me._passwordTextBoxLabel.Text = "PASSWORD: "
        Me._passwordTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_userNameTextBox
        '
        Me._userNameTextBox.Location = New System.Drawing.Point(79, 3)
        Me._userNameTextBox.Name = "_userNameTextBox"
        Me._userNameTextBox.Size = New System.Drawing.Size(70, 20)
        Me._userNameTextBox.TabIndex = 1
        Me._userNameTextBox.Text = "davidhary"
        '
        '_userNameTextBoxLabel
        '
        Me._userNameTextBoxLabel.Location = New System.Drawing.Point(5, 3)
        Me._userNameTextBoxLabel.Name = "_userNameTextBoxLabel"
        Me._userNameTextBoxLabel.Size = New System.Drawing.Size(76, 20)
        Me._userNameTextBoxLabel.TabIndex = 0
        Me._userNameTextBoxLabel.Text = "ID: "
        Me._userNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_logOnButton
        '
        Me._logOnButton.AutoSize = True
        Me._logOnButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._logOnButton.Location = New System.Drawing.Point(149, 22)
        Me._logOnButton.Name = "_logOnButton"
        Me._logOnButton.Size = New System.Drawing.Size(55, 23)
        Me._logOnButton.TabIndex = 4
        Me._logOnButton.Text = "LOGON"
        Me._logOnButton.UseVisualStyleBackColor = True
        '
        'UserLogOn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._logOnButton)
        Me.Controls.Add(Me._passwordTextBox)
        Me.Controls.Add(Me._passwordTextBoxLabel)
        Me.Controls.Add(Me._userNameTextBox)
        Me.Controls.Add(Me._userNameTextBoxLabel)
        Me.Name = "UserLogOn"
        Me.Size = New System.Drawing.Size(209, 50)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents _passwordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents _passwordTextBoxLabel As System.Windows.Forms.Label
    Friend WithEvents _userNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents _userNameTextBoxLabel As System.Windows.Forms.Label
    Friend WithEvents _logOnButton As System.Windows.Forms.Button

End Class
