﻿''' <summary>
''' A timer with state managmenet.</summary>
''' <remarks>
''' </remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="05/07/09" by="David Hary" revision="1.1.3414.x">
''' Add a sync lock to facilitate the orcastrating of close and dispose
''' </history>
''' <history date="01/30/08" by="David Hary" revision="1.0.2951.x">
''' created.
''' </history>
Public Class StateAwareTimer

    Inherits System.Timers.Timer

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructor specifying a <paramref name="syncObject">synchronizing object.</paramref>
    ''' </summary>
    ''' <param name="synchronizer">The form where this class resides.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal synchronizer As System.ComponentModel.ISynchronizeInvoke)

        ' instantiate the base class
        Me.New()
        MyBase.SynchronizingObject = synchronizer

    End Sub

    ''' <summary>Constructs this class.</summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New()

        Me._timerState = TimerStates.None
        onTickLocker = New Object

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets the dispose status sentinel of the base class.  This applies to the dervied class
    ''' provided proper implementation.
    ''' </summary>
    Protected ReadOnly Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            SyncLock onTickLocker

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me._isDisposed = True

                    ' Invoke the base class dispose method
                    MyBase.Dispose()

                End Try

            End SyncLock

        End If

    End Sub

#End Region

#Region " PROPERTIES "

    Private _timerState As TimerStates

    ''' <summary>
    ''' Gets the timer state.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property TimerState() As TimerStates
        Get
            Return _timerState
        End Get
    End Property

    ''' <summary>
    ''' Gets a True if the timer is processing an event.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property IsInTick() As Boolean
        Get
            Return (_timerState And TimerStates.InTick) <> 0
        End Get
    End Property
    ''' <summary>
    ''' Gets a True if timer is in Running state.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property IsRunning() As Boolean
        Get
            Return (_timerState And TimerStates.Running) <> 0
        End Get
    End Property

    ''' <summary>
    ''' Gets a True if timer is in Stopping state.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property IsStopping() As Boolean
        Get
            Return (_timerState And TimerStates.Stopping) <> 0
        End Get
    End Property

    Private _asynchronousInvoke As Boolean
    ''' <summary>
    ''' Gets or sets a condition for invoking the call back tick event
    ''' asynchronously.  Must have a <see cref="SynchronizingObject">synchronizing object</see>
    ''' for using asynchronous invokation.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property AsynchronousInvoke() As Boolean
        Get
            Return _asynchronousInvoke
        End Get
        Set(ByVal value As Boolean)
            _asynchronousInvoke = value
        End Set
    End Property

    ''' <summary>
    ''' Start or stos event raising.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shadows Property Enabled() As Boolean
        Get
            Return MyBase.Enabled
        End Get
        Set(ByVal value As Boolean)
            If value Then
                Me.Start()
            Else
                Me.Stop()
            End If
        End Set
    End Property

#End Region

#Region " METHODS "

    Public Shadows Function [Start]() As Boolean
        SyncLock onTickLocker
            If Me._isDisposed Then
                Return False
            End If
            MyBase.Start()
            Me._timerState = TimerStates.Running
            Return MyBase.Enabled
        End SyncLock
    End Function

    ''' <summary>
    ''' Stops the timer.
    ''' </summary>
    ''' <remarks></remarks>
    Public Shadows Function [Stop]() As Boolean
        SyncLock onTickLocker
            If Me._isDisposed Then
                Return True
            End If
            If Me.IsRunning Then
                Me._timerState = _timerState Or TimerStates.Stopping
            End If
            MyBase.Stop()
            Me._timerState = TimerStates.None
            Return Not MyBase.Enabled
        End SyncLock
    End Function

#If False Then
  ''' <summary>
  ''' Stops the timer allowing the last timer event to complete.
  ''' </summary>
  ''' <remarks>
  ''' No longer required when using Sync Locks.
  ''' </remarks>
  Public Function StopWait() As Boolean

    SyncLock onTickLocker

      MyBase.Stop()

      If Me.IsRunning Then

        Me._timerState = _timerState Or TimerStates.Stopping


      Else
        ' if not running assume we stopped.
        Me._timerState = TimerStates.None
      End If

      Return Not MyBase.Enabled
    End SyncLock

  End Function
#End If

#End Region

#Region " TICK EVENT "

    ''' <summary>
    ''' Handles tick events
    ''' </summary>
    ''' <remarks></remarks>
    Public Event Tick As EventHandler(Of System.EventArgs)

    ''' <summary>
    ''' Raises the <see cref="system.EventHandler">tick</see> event.
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub _onTick(ByVal e As System.EventArgs)
        If TickEvent IsNot Nothing Then TickEvent(Me, e)
    End Sub

    ''' <summary>Raises the Notify event.
    ''' </summary>
    ''' <param name="e">Specifies the event <see cref="System.EventArgs">arguments</see>.</param>
    Protected Sub OnTick(ByVal e As System.EventArgs)

        If _asynchronousInvoke Then
            MyBase.SynchronizingObject.BeginInvoke(New Action(Of EventArgs)(AddressOf _onTick), New Object() {e})
        Else
            If MyBase.SynchronizingObject IsNot Nothing AndAlso MyBase.SynchronizingObject.InvokeRequired Then
                MyBase.SynchronizingObject.Invoke(New Action(Of EventArgs)(AddressOf _onTick), New Object() {e})
            Else
                Me._onTick(e)
            End If
        End If

    End Sub

    Private onTickLocker As Object
    ''' <summary>
    ''' Raises the tick event.
    ''' </summary>
    ''' <remarks></remarks>
    ''' <history date="05/07/09" by="David Hary" revision="1.1.3414.x">
    ''' Add a sync lock to facilitate the orcastrating of close and dispose.
    ''' Remove exception handling - this should be done within the timer event handler
    ''' in the parent instance.
    ''' </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Protected Sub OnElapsed() Handles MyBase.Elapsed
        MyBase.Stop()
        SyncLock onTickLocker
            If Me._isDisposed Then
                Return
            End If
            Me._timerState = Me._timerState Or TimerStates.InTick
            Me.OnTick(System.EventArgs.Empty)
            If Me.IsStopping Then
                Me._timerState = TimerStates.None
            Else
                Me._timerState = TimerStates.Running
            End If
        End SyncLock
        If Me._timerState = TimerStates.Running Then
            MyBase.Start()
        End If
    End Sub

#End Region

End Class

''' <summary>
''' Enumerates the timer states.
''' </summary>
''' <remarks></remarks>
<Flags()> Public Enum TimerStates
  None = 0
  Running = 1
  Stopping = 2
  InTick = 4
End Enum
