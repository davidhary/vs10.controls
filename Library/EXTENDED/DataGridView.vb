﻿Imports System.Drawing
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Security.Permissions
<DesignerCategory("code"), System.ComponentModel.Description("Extended Data Grid View"), _
    System.Drawing.ToolboxBitmap(GetType(DataGridView))> _
Public Class DataGridView
    Inherits Windows.Forms.DataGridView

    ''' <summary>
    ''' Work around to a problem with the data grid view failure to handle the format provider.
    ''' http://www.codeproject.com/KB/cs/custstrformat.aspx?df=100+forumid=36436+exp=0+select=1661578#xx1661578xx 
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnCellFormatting(ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs)
        Dim col As DataGridViewColumn = Me.Columns(e.ColumnIndex)
        If TypeOf col.DefaultCellStyle.FormatProvider Is ICustomFormatter Then
            Dim originalData As Object = e.Value
            e.Value = String.Format(col.DefaultCellStyle.FormatProvider, col.DefaultCellStyle.Format, originalData)
        Else
            MyBase.OnCellFormatting(e)
        End If
    End Sub

End Class
