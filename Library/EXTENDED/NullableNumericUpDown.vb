﻿Imports System.Drawing
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Security.Permissions
''' <summary>
''' A numeric up down with nullable value.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="08/19/2011" by="David Hary" revision="1.02.4248.x">
''' created.
''' </history>
<DesignerCategory("code"), System.ComponentModel.Description("Nullable Numeric Up Down"), _
    System.Drawing.ToolboxBitmap(GetType(NullableNumericUpDown))> _
Public Class NullableNumericUpDown
    Inherits System.Windows.Forms.NumericUpDown

    ''' <summary>
    ''' Initializes a new instance of the <see cref="NullableNumericUpDown" /> class.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        _NullCaption = ""
        _nullableValue = New Decimal?
        _textbox = GetPrivateField(Of TextBox)(Me, "upDownEdit")
        If _textbox Is Nothing Then
            Throw New ArgumentNullException(Me.GetType.FullName _
                    & ": Can't find internal TextBox field.")
        End If
    End Sub

    ''' <summary>
    ''' Extracts a reference to a private underlying field
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")> _
    Protected Friend Shared Function GetPrivateField(Of T As Control)(ByVal ctrl As System.Windows.Forms.NumericUpDown, ByVal fieldName As String) As T
        ' find internal TextBox
        Dim fi As Reflection.FieldInfo _
            = GetType(System.Windows.Forms.NumericUpDown).GetField(fieldName, _
                        Reflection.BindingFlags.FlattenHierarchy _
                        Or Reflection.BindingFlags.NonPublic _
                        Or Reflection.BindingFlags.Instance)
        ' take some caution... they could change field name in the future!
        If fi Is Nothing Then
            Return Nothing
        Else
            Return TryCast(fi.GetValue(ctrl), T)
        End If
    End Function

    Private _NullCaption As String
    ''' <summary>
    ''' Gets or sets the value which to display when the control value is null.
    ''' </summary>
    ''' <value>
    ''' The null caption.
    ''' </value>
    <Category("Behavior"), Description("The value to display if the value is null."), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue("<Null>")> _
    Public Property NullCaption() As String
        Get
            Return _NullCaption
        End Get
        Set(ByVal value As String)
            _NullCaption = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the null value text.
    ''' </summary>
    ''' <value>
    ''' The null value text.
    ''' </value>
    <Category("Behavior"), Description("The null value text."), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue("")> _
    Public Property NullText() As String
        Get
            Return _textbox.Text
        End Get
        Set(ByVal value As String)
            Me.NullValueSetter(value)
        End Set
    End Property

    ''' <summary>
    ''' Sets the null value from a string value.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <remarks></remarks>
    Private Sub NullValueSetter(ByVal value As String)
        Dim numericValue As Decimal = Me.DefaultNullValue.GetValueOrDefault(0)
        If String.IsNullOrEmpty(value) Then
            Me.NullableValue = New Decimal?
        ElseIf Decimal.TryParse(value, numericValue) Then
            Me.NullableValue = numericValue
        Else
            Me.NullableValue = New Decimal?
        End If
    End Sub

    ''' <summary>
    ''' Sets the null value for a nullable number.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <remarks></remarks>
    Private Sub NullValueSetter(ByVal value As Decimal?)
        If value Is Nothing Then
            value = New Decimal?
        End If
        _nullableValue = value
        If value.HasValue Then
            If Me.DefaultNullValue.Equals(value) Then
                If Not _textbox.Text.Equals(Me.NullCaption) Then
                    _textbox.Text = Me.NullCaption
                End If
            Else
                If (Me.Value.Equals(value.Value) AndAlso value.Equals(_nullableValue)) Then
                    Me.Value = value.Value
                Else
                    ' _nullableValue = New Decimal?
                    Me.Value = value.Value
                End If
                End If
        Else
                If Not _textbox.Text.Equals(Me.NullCaption) Then
                    _textbox.Text = Me.NullCaption
                End If
        End If
    End Sub

    Dim _nullableValue As Decimal?
    ''' <summary>
    ''' Gets or sets the nullable value.
    ''' </summary>
    ''' <value>
    ''' The nullable value.
    ''' </value>
    <Category("Behavior"), Description("The nullable value."), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property NullableValue As Decimal?
        Get
            Return _nullableValue
        End Get
        Set(value As Decimal?)
            Me.NullValueSetter(value)
        End Set
    End Property

    ' reference to the underlying TextBox control
    Private _textbox As TextBox

    ''' <summary>
    ''' Occurres when the underlying text changes.
    ''' </summary>
    ''' <param name="source">The source of the event.</param>
    ''' <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnChanged(source As Object, e As System.EventArgs)
        MyBase.OnChanged(source, e)
        If Me._textbox IsNot Nothing AndAlso String.IsNullOrEmpty(Me._textbox.Text) Then
            Me.NullableValue = New Decimal?
        Else
            Me.NullableValue = Me.Value
        End If
        Me.OnValueChanged(e)
    End Sub

    Dim _defaultNullValue As Decimal?
    ''' <summary>
    ''' Gets or sets the default null value.
    ''' </summary>
    ''' <value>
    ''' The default null value.
    ''' </value>
    <Category("Behavior"), Description("When this value is set the control value is set to Null."), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue(0)> _
    Public Property DefaultNullValue As Decimal?
        Get
            Return _defaultNullValue
        End Get
        Set(value As Decimal?)
            _defaultNullValue = value
            If value.HasValue Then
                OnValueChanged(System.EventArgs.Empty)
            End If
        End Set
    End Property

End Class
