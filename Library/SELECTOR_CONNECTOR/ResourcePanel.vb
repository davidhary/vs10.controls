Imports isr.Core.StringExtensions
Imports isr.Core.ControlExtensions
Imports System.ComponentModel

''' <summary>
''' Provides a user interface for a <see cref="isr.Core.IConnectableResource">connectable resource.</see>
''' such as a VISA instrument.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
''' created.
''' </history>
''' <history date="01/21/2011" by="David Hary" revision="1.2.4038.x">
''' Update to using the <see cref="isr.Core.IConnectableResource">connectable resource.</see>
''' </history>
<System.ComponentModel.Description("Connectable Resource Panel - Windows Forms Custom Control")> _
Public Class ResourcePanel
    Implements isr.Core.IMessageObserver

#If False Then
  ' Use inheritable class when editing the derived controls.
  Public MustInherit Class ResourcePanel
  Public Class ResourcePanel
#End If

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Protected Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' onInstantiate()
        Me.Connector.Connectible = True
        Me.Connector.Clearable = True

    End Sub

#End Region

#Region " I MESSAGE PUBLISHER "

    ''' <summary>
    ''' Raises the Message Available event.
    ''' </summary>
    ''' <param name="e">Specifies the event arguments.</param>
    Public Overridable Function OnMessageAvailable(ByVal e As isr.Core.MessageEventArgs) As Boolean Implements isr.Core.IMessageObserver.OnMessageAvailable
        lastMessageDetails = e.Details
        Me._StatusPanel.Text = e.Synopsis
        Me.StatusPanel.SafeTextSetter(e.Synopsis)
        My.Application.Log.WriteEntry(e.Details, e.TraceLevel)
        Return True
    End Function

    ''' <summary>
    ''' Raises a message.
    ''' </summary>
    ''' <param name="traceLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">trace level</see>.</param>
    ''' <param name="synopsis">Specifies the message short synopsis.</param>
    ''' <param name="format">Specifies the format for building the message detains</param>
    ''' <param name="args">Specifies the format arguments.</param>
    ''' <remarks></remarks>
    Public Overridable Function OnMessageAvailable(ByVal traceLevel As Diagnostics.TraceEventType, _
                                  ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return Me.OnMessageAvailable(traceLevel, traceLevel, synopsis, format, args)
    End Function

    ''' <summary>
    ''' Raises the Message Available event.
    ''' </summary>
    ''' <param name="broadcastLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">broadcast level</see>.</param>
    ''' <param name="traceLevel">Specifies the event arguments message
    ''' <see cref="Diagnostics.TraceEventType">trace level</see>.</param>
    ''' <param name="synopsis">Specifies the message Synopsis.</param>
    ''' <param name="format">Specifies the message details.</param>
    ''' <param name="args">Arguments to use in the format statement.</param>
    Public Overridable Function OnMessageAvailable(ByVal broadcastLevel As Diagnostics.TraceEventType, ByVal traceLevel As Diagnostics.TraceEventType, _
                                  ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return OnMessageAvailable(New isr.Core.MessageEventArgs(broadcastLevel, traceLevel, synopsis, format, args))
    End Function

#End Region

#Region " PROPERTIES AND METHODS: DISPLAY "

    ''' <summary>
    ''' Sets the resource name value.
    ''' </summary>
    Public Overridable Sub UpdateResourceName(ByVal resourceName As String)
        Me.IdentityPanel.SafeTextSetter(resourceName)
        Me.IdentityPanel.SafeToolTipTextSetter(resourceName)
    End Sub

#End Region

#Region " CONNECT and DISCONNECT "

    ''' <summary>Gets or sets the connection status.
    ''' Updats controls.
    ''' </summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public Overridable Property IsConnected() As Boolean
        Get
            Return Me.Connector.IsConnected AndAlso Me._connectableResource IsNot Nothing
        End Get
        Set(ByVal value As Boolean)
            Me.Connector.IsConnected = value
            If value Then
                If _connectableResource IsNot Nothing Then
                    Me.UpdateResourceName(_connectableResource.ResourceName)
                End If
            Else
                Me.UpdateResourceName(String.Empty)
            End If
        End Set
    End Property

    Private Sub _connectableInterface_ConnectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _connectableResource.ConnectionChanged
        Me.IsConnected = CType(sender, isr.Core.IConnectableResource).IsConnected
        If Me.IsConnected Then
            Me.OnMessageAvailable(TraceEventType.Information, "CONNECTED", "Connected.")
            Me.OnMessageAvailable(TraceEventType.Information, "LOG INFO", "Logging to: {0}", My.Application.Log.DefaultFileLogWriter.Location)
        Else
            Me.OnMessageAvailable(TraceEventType.Information, "DISCONNECTED", "Disconnected")
        End If
    End Sub

    Dim lastMessageDetails As String
    Private Sub _connectableResource_MessageAvailable(ByVal sender As Object, ByVal e As Core.MessageEventArgs) Handles _connectableResource.MessageAvailable
        If String.IsNullOrEmpty(lastMessageDetails) OrElse Not String.Equals(lastMessageDetails, e.Details) Then
            Me.OnMessageAvailable(e)
        End If
    End Sub

#End Region

#Region " PROPERTIES "

    Private WithEvents _connectableResource As isr.Core.IConnectableResource
    ''' <summary>
    ''' Gets or sets reference to the VISA instrument implementing
    ''' <see cref="isr.Core.IConnectResource">connectible</see> and <see cref="isr.Core.IResettable">resettable</see>
    ''' interfaces.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Protected Property ConnectableResource() As isr.Core.IConnectableResource
        Get
            Return _connectableResource
        End Get
        Set(ByVal value As isr.Core.IConnectableResource)
            _connectableResource = value
        End Set
    End Property

    Private _visible As Boolean = True
    ''' <summary>
    ''' Holds true if the control is visible.  This is used in place
    ''' of the control natural visibilty to control access to the 
    ''' control because it seems in VS 2005 the Visible property
    ''' may be true only if the control is both Visible and Active.
    ''' </summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public Property IsVisible() As Boolean
        Get
            Return _visible
        End Get
        Set(ByVal value As Boolean)
            _visible = value
        End Set
    End Property

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles MyBase.Load

        Me.StatusPanel.Text = "Find and select a resource."
        Me.IdentityPanel.Text = String.Empty

    End Sub

#End Region

#Region " CONNECTOR EVENT HANDLERS "

    ''' <summary>Display the resource names based on the last interface type.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Public Sub DisplayNames()

        Try

            ' get the list of available resources
            Dim names() As String = Me.ConnectableResource.FindResources
            Me.Connector.DisplayNames(names)

            If names Is Nothing Then

                Me.OnMessageAvailable(TraceEventType.Information, "RESOURCE NAMES ARE EMPTY", "Resource names are empty")

            ElseIf names.Length = 0 Then

                ' send a verbose message.
                Me.OnMessageAvailable(TraceEventType.Information, "RESOURCES NOT FOUND", "Resources not found")

                ' check if failure occurred fetching local resources
                If Me.ConnectableResource.FailedFindingResources Then
                    Me.OnMessageAvailable(TraceEventType.Warning, "FAILED FETCHING THE LIST OF LOCAL RESOURCES", _
                                          "Failed fetching the list of local resources. Try entering Resource String in the format 'GPIB[board]::address::INSTR'. Details: {0}", _
                                          Me.ConnectableResource.ResourceFindingFailureSymptom)
                Else
                    Me.OnMessageAvailable(TraceEventType.Warning, "NO LOCAL RESOURCES WERE FOUND", _
                                          "No local resources were found. Details: {0}", _
                                          Me.ConnectableResource.ResourceFindingFailureSymptom)
                End If

            Else

                ' send a verbose message.
                Me.OnMessageAvailable(TraceEventType.Information, "RESOURCE NAMES LISTED", "Resource names listed")

            End If

        Catch ex As System.ArgumentException

            Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED DISPLAYING RESOURCES", _
                                  "Exception occurred finding resources. Connect the resources(s) and click Find. Details: {0}", ex)
        End Try

    End Sub

    ''' <summary>
    ''' Clears the instrument by calling a propagating clear command.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Private Sub connector_Clear(ByVal sender As Object, ByVal e As System.EventArgs) Handles Connector.Clear
        Try
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
            Me.OnMessageAvailable(TraceEventType.Verbose, "CLEARING RESOURCE...", "Clearing the resource {0}", Me.ConnectableResource.ResourceName)
            Me.ConnectableResource.ClearResource()
            Me.OnMessageAvailable(TraceEventType.Verbose, "RESOURCE CLEARED.", "Resource {0} cleared", Me.ConnectableResource.ResourceName)
        Catch ex As Exception
            Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING", _
                                  "Exception occurred clearing. Details: {0}", ex)
        Finally
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Connects the instrument by calling a propagating connect command.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Private Sub connector_Connect(ByVal sender As Object, ByVal e As System.EventArgs) Handles Connector.Connect

        Me.OnMessageAvailable(TraceEventType.Information, "CONNECTING...", "Connecting to {0}", Me.Connector.SelectedName)
        Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
        Try
            If Me.ConnectableResource.Connect(Me.Connector.SelectedName) Then
                Me.OnMessageAvailable(TraceEventType.Information, "CONNECTED", "Connected to {0}", Me.Connector.SelectedName)
            Else
                Me.OnMessageAvailable(TraceEventType.Information, "FAILED CONNECTING", "Failed connecting to {0}", Me.Connector.SelectedName)
                Me.IsConnected = False
            End If
        Catch ex As Exception
            Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED CONNECTING", _
                                  "Exception occurred connecting to {0}. Details: {1}", Me.Connector.SelectedName, ex)
        Finally
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Disconnects the instrument by calling a propagating disconnect command.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Private Sub connector_Disconnect(ByVal sender As Object, ByVal e As System.EventArgs) Handles Connector.Disconnect
        Try
            If Me.ConnectableResource.IsConnected Then
                Me.OnMessageAvailable(TraceEventType.Information, "DISCONNECTING...", "Disconnecting from {0}", Me.ConnectableResource.ResourceName)
            End If
            If Me.ConnectableResource.Disconnect() Then
                Me.OnMessageAvailable(TraceEventType.Information, "DISCONNECTED", "Disconnected from {0}", Me.ConnectableResource.ResourceName)
            Else
                Me.IsConnected = True
                Me.OnMessageAvailable(TraceEventType.Information, "FAILED DISCONNECTING", "Failed disconnecting from {0}", Me.Connector.SelectedName)
            End If
        Catch ex As Exception
            Me.OnMessageAvailable(TraceEventType.Error, "EXCEPTION OCCURRED DISCONNECTING", _
                                  "Exception occurred disconnecting from {0}. Details: {1}", Me.ConnectableResource.ResourceName, ex)
        Finally
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Displays available instrument names.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub connector_FindNames(ByVal sender As Object, ByVal e As System.EventArgs) Handles Connector.FindNames
        Me.DisplayNames()
    End Sub

    ''' <summary>
    ''' Notifies that an instrument name was selected.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub connector_Selected(ByVal sender As Object, ByVal e As System.EventArgs) Handles Connector.Selected
        Me.OnMessageAvailable(TraceEventType.Information, "SELECTED " & Me.Connector.SelectedName, "Selected {0}", Me.Connector.SelectedName)
    End Sub

#End Region

End Class

