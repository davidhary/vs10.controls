Imports System.ComponentModel
Imports isr.Core.StringExtensions
''' <summary>
''' A simple text box and browse button file selector.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/14/2007" by="David Hary" revision="1.2.3405.x">
''' Add default event and description and move bitmap setting from the designer file.
''' </history>
''' <history date="03/14/2007" by="David Hary" revision="1.00.2619.x">
''' created.
''' </history>
<Description("File Selector"), _
    DefaultEvent("SelectedChanged"), _
    Drawing.ToolboxBitmap(GetType(FileSelector))> _
Public Class FileSelector
    Inherits UserControlBase

#Region " CONSTRUCTORS "

    ''' <summary>
    ''' Clears internal properties.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        _textBoxContents = ""
        _filePathName = ""
        _defaultExtension = ".TXT"
        _dialogFilter = "Text Files (*.txt; *.lst)|*.txt;*.lst|Batch Files (*.bat)|*.bat|All Files (*.*)|*.*"
        _dialogTitle = "SELECT A TEXT FILE"
        _extensionName = ""
        _fileName = ""
        _fileTitle = ""
    End Sub

#End Region

#Region " PROPERTIES "

    Private _defaultExtension As String
    ''' <summary>
    ''' Gets or sets the default extension.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Appearance"), Description("Default extension"), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue(".TXT")> _
    Public Property DefaultExtension() As String
        Get
            Return _defaultExtension
        End Get
        Set(ByVal value As String)
            _defaultExtension = value
        End Set
    End Property

    Private _dialogFilter As String
    ''' <summary>
    ''' Gets or sets the filename filter string, which determines the choices of files that appear in the file type selection drop down list.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Appearance"), Description("Filename filter string determing the choices of files that appear in the file type selection drop down list."), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue("Text Files (*.txt; *.lst)|*.txt;*.lst|Batch Files (*.bat)|*.bat|All Files (*.*)|*.*")> _
    Public Property DialogFilter() As String
        Get
            Return _dialogFilter
        End Get
        Set(ByVal value As String)
            _dialogFilter = value
        End Set
    End Property

    Private _dialogTitle As String
    ''' <summary>
    ''' Gets or sets the dialog title.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Category("Appearance"), Description("Dialog caption"), _
        Browsable(True), _
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
        DefaultValue("SELECT A TEXT FILE")> _
    Public Property DialogTitle() As String
        Get
            Return _dialogTitle
        End Get
        Set(ByVal value As String)
            _dialogTitle = value
        End Set
    End Property

    ''' <summary>Gets or sets the file extension.</summary>
    Private _extensionName As String

    ''' <summary>Gets the file extension.</summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public ReadOnly Property ExtensionName() As String
        Get
            Return _extensionName
        End Get
    End Property

    ''' <summary>Gets or sets the file name.</summary>
    Private _fileName As String
    ''' <summary>Gets the file name.</summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public ReadOnly Property FileName() As String
        Get
            FileName = _fileName
        End Get
    End Property

    Private _textBoxContents As String
    Private _filePathName As String
    ''' <summary>
    ''' Gets or sets a new file path name.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public Property FilePathName() As String
        Get
            Return _filePathName
        End Get
        Set(ByVal Value As String)
            Value = Value.Trim
            If _filePathName <> Value OrElse _textBoxContents <> Value.Compact(_filePathNameTextBox) Then
                If String.IsNullOrEmpty(Value) Then
                    _textBoxContents = ""
                    _filePathNameTextBox.Text = _textBoxContents
                    _fileName = _textBoxContents
                    _extensionName = ""
                    _fileTitle = ""
                    Me._toolTip.SetToolTip(_filePathNameTextBox, "Enter or browse for a file name")
                Else
                    If System.IO.File.Exists(Value) Then
                        Dim fileInfo As System.IO.FileInfo = My.Computer.FileSystem.GetFileInfo(Value)
                        Value = fileInfo.FullName
                        _textBoxContents = Value.Compact(_filePathNameTextBox)
                        _filePathNameTextBox.Text = _textBoxContents
                        Me._toolTip.SetToolTip(_filePathNameTextBox, Value)
                        _fileName = fileInfo.Name
                        _extensionName = fileInfo.Extension
                        _fileTitle = isr.Core.IOExtensions.Title(fileInfo)
                    Else
                        _textBoxContents = "<file not found: " & Value & ">"
                        _filePathNameTextBox.Text = _textBoxContents
                        Me._toolTip.SetToolTip(_filePathNameTextBox, "Enter or browse for a file name")
                        _fileName = ""
                        _extensionName = ""
                        _fileTitle = ""
                    End If
                End If
                Me.OnSelectedChanged()
            End If
            _filePathName = Value
            _filePathNameTextBox.Refresh()
        End Set
    End Property

    ''' <summary>Gets or sets the file title.</summary>
    Private _fileTitle As String

    ''' <summary>Gets the file title.</summary>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)> _
    Public ReadOnly Property FileTitle() As String
        Get
            Return _fileTitle
        End Get
    End Property

#End Region

#Region " METHODS "

    ''' <summary>Refreshes the control.</summary>
    Public Overrides Sub Refresh()
        MyBase.Refresh()
    End Sub

    ''' <summary>
    ''' Selects a file.
    ''' </summary>
    Public Function SelectFile() As Boolean

        Dim dialog As New OpenFileDialog()

        ' Use the common dialog class
        dialog.RestoreDirectory = True
        dialog.Multiselect = False
        dialog.CheckFileExists = True
        dialog.CheckPathExists = True
        dialog.ReadOnlyChecked = False
        dialog.DefaultExt = Me._defaultExtension
        dialog.Title = Me._dialogTitle
        dialog.FilterIndex = 0
        If String.IsNullOrEmpty(_filePathName) Then
            dialog.FileName = My.Computer.FileSystem.CurrentDirectory
        Else
            dialog.FileName = Me.FilePathName
        End If
        dialog.Filter = _dialogFilter

        ' Open the Open dialog
        If dialog.ShowDialog(Me) = DialogResult.OK Then

            ' if file selected,
            Me.FilePathName = dialog.FileName

            ' return true for pass
            Return True

        Else

            Return False

        End If

    End Function

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>
    ''' Browses for a file name.
    ''' </summary>
    ''' <param name="eventSender"></param>
    ''' <param name="eventArgs"></param>
    ''' <remarks></remarks>
    Private Sub browseButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _browseButton.Click

        ' Select a file
        SelectFile()

    End Sub

    ''' <summary>
    ''' Enters a file name.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub filePathNameTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) _
        Handles _filePathNameTextBox.Validating

        Me.FilePathName = _filePathNameTextBox.Text

    End Sub

    ''' <summary>Resized the frame and sets the width of the text boxes.
    ''' </summary>
    Private Sub UserControl_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize

        Height = Math.Max(Me._browseButton.Height, Me._filePathNameTextBox.Height) + 2

        _browseButton.Top = (Height - _browseButton.Height) \ 2
        _browseButton.Left = Width - _browseButton.Width - 1
        _browseButton.Refresh()

        _filePathNameTextBox.Top = (Height - _filePathNameTextBox.Height) \ 2
        _filePathNameTextBox.Width = _browseButton.Left - 1
        _filePathNameTextBox.Refresh()

        Me.Refresh()

    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>Occurs when a new file was selected.
    ''' </summary>
    ''' <param name="e">Specifies the
    ''' <see cref="EventArgs">event arguments</see>.
    ''' </param>
    Public Event SelectedChanged As EventHandler(Of EventArgs)

    ''' <summary>Raises the selected changed event.
    ''' </summary>
    Public Sub OnSelectedChanged()

        If SelectedChangedEvent IsNot Nothing Then
            SelectedChangedEvent(Me, EventArgs.Empty)
            System.Windows.Forms.Application.DoEvents()
        End If

    End Sub

#End Region

End Class
