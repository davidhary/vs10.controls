'''   <summary>A flat button.</summary>
''' <history date="09/08/07" by="Microsoft" revision="1.0.2807.x">
''' .Net toogle Button example.
''' </history>
''' <history date="09/08/07" by="David Hary" revision="1.0.2807.x">
''' Add to controls.
''' </history>
<System.ComponentModel.Description("Flat Button"), _
    System.Drawing.ToolboxBitmap(GetType(FlatButton))> _
Public Class FlatButton

#Region " PROPERTIES "

    Private _imageLocation As Point = New Point(7, 5)
    ''' <summary>
    ''' Gets or sets the location of the image.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ImageLocation() As Point
        Get
            Return _imageLocation
        End Get
        Set(ByVal value As Point)
            _imageLocation = value
        End Set
    End Property

    Private _isSelected As Boolean
    ''' <summary>
    ''' Gets or sets to condition telling if the control is selected.
    ''' This determines how we repaint the control.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property IsSelected() As Boolean
        Get
            Return _isSelected
        End Get
        Set(ByVal Value As Boolean)
            If _isSelected <> Value Then
                _isSelected = Value
                ' Selection changed so we need to repaint
                Invalidate()
            End If
        End Set
    End Property

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>
    ''' Selects the button.
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        ' Since the button was clicked our selection has changed
        Me.IsSelected = True
        MyBase.OnClick(e)
    End Sub

    ''' <summary>
    ''' Have our MouseDown act like a mouse click.
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnMouseDown(ByVal e As System.Windows.Forms.MouseEventArgs)
        OnClick(e)
    End Sub

    ''' <summary>
    ''' Paints the control.
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)

        Dim rect As Rectangle = e.ClipRectangle
        Dim textFlags As TextFormatFlags = TextFormatFlags.VerticalCenter Or TextFormatFlags.Right
        Dim buttonGraphics As Graphics = e.Graphics

        If _isSelected Then
            ' If button is selected then highlight the button
            buttonGraphics.FillRectangle(Brushes.White, rect)
            TextRenderer.DrawText(e.Graphics, Me.Text, Me.Font, rect, Me.ForeColor, Color.Transparent, textFlags)
            ' buttonGraphics.DrawString(Me.Text, Me.Font, New SolidBrush(Me.ForeColor),  20, CSng(2.5))
            buttonGraphics.DrawRectangle(New Pen(System.Drawing.SystemColors.Highlight, 2), rect)
        Else
            ' Just draw the border
            buttonGraphics.FillRectangle(New SolidBrush(System.Drawing.SystemColors.Control), rect)
            ' TextRenderer.DrawText(e.Graphics, Me.Text, Me.Font, textRect, Me.ForeColor)
            TextRenderer.DrawText(e.Graphics, Me.Text, Me.Font, rect, Me.ForeColor, Color.Transparent, textFlags)
            ' buttonGraphics.DrawString(Me.Text, Me.Font, New SolidBrush(Me.ForeColor), 20,  CSng(2.5))
        End If

        ' draw the correct image if one is specified
        If MyBase.ImageIndex >= 0 AndAlso Me.ImageList.Images IsNot Nothing _
            AndAlso Me.ImageList.Images.Count > MyBase.ImageIndex Then
            buttonGraphics.DrawImage(Me.ImageList.Images(MyBase.ImageIndex), _imageLocation)
        End If

    End Sub

#End Region

End Class
