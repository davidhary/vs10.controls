﻿namespace ThreadSafeControlsTest {
    partial class ThreadSafeControlsTestForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.StopThreadSafeTestLabel = new System.Windows.Forms.Label();
            this.StartThreadSafeTestLabel = new System.Windows.Forms.Label();
            this.ThreadSafeTestTextBox = new System.Windows.Forms.TextBox();
            this.StopThreadSafeTestButton = new System.Windows.Forms.Button();
            this.StartThreadSafeTestButton = new System.Windows.Forms.Button();
            this.ThreadSafeTestWorker = new System.ComponentModel.BackgroundWorker();
            this.ThreadSafeTestBlinkingLabel = new System.Windows.Forms.Label();
            this.ThreadSafeTestMovingLabel = new System.Windows.Forms.Label();
            this.ThreadSafeTestFontChangingLabel = new System.Windows.Forms.Label();
            this.ThreadSafeTestTable = new System.Windows.Forms.TableLayoutPanel();
            this.ThreadSafeTestTextBoxBox = new System.Windows.Forms.GroupBox();
            this.ThreadSafeTestListBoxBox = new System.Windows.Forms.GroupBox();
            this.ThreadSafeTestListBox = new System.Windows.Forms.ListBox();
            this.ThreadSafeTestListViewBox = new System.Windows.Forms.GroupBox();
            this.ThreadSafeTestListView = new System.Windows.Forms.ListView();
            this.ThreadSafeTestProgressBarBox = new System.Windows.Forms.GroupBox();
            this.ThreadSafeTestProgressBar = new System.Windows.Forms.ProgressBar();
            this.ThreadSafeTestDateTimePickerBox = new System.Windows.Forms.GroupBox();
            this.ThreadSafeTestDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.ThreadSafeTestCheckBox = new System.Windows.Forms.CheckBox();
            this.ThreadSafeTestRadioButton = new System.Windows.Forms.RadioButton();
            this.ThreadSafeTestTable.SuspendLayout();
            this.ThreadSafeTestTextBoxBox.SuspendLayout();
            this.ThreadSafeTestListBoxBox.SuspendLayout();
            this.ThreadSafeTestListViewBox.SuspendLayout();
            this.ThreadSafeTestProgressBarBox.SuspendLayout();
            this.ThreadSafeTestDateTimePickerBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // StopThreadSafeTestLabel
            // 
            this.StopThreadSafeTestLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.StopThreadSafeTestLabel.AutoSize = true;
            this.StopThreadSafeTestLabel.Location = new System.Drawing.Point(291, 317);
            this.StopThreadSafeTestLabel.Name = "StopThreadSafeTestLabel";
            this.StopThreadSafeTestLabel.Size = new System.Drawing.Size(200, 13);
            this.StopThreadSafeTestLabel.TabIndex = 6;
            this.StopThreadSafeTestLabel.Text = "Click here to stop the background thread";
            // 
            // StartThreadSafeTestLabel
            // 
            this.StartThreadSafeTestLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.StartThreadSafeTestLabel.AutoSize = true;
            this.StartThreadSafeTestLabel.Location = new System.Drawing.Point(200, 288);
            this.StartThreadSafeTestLabel.Name = "StartThreadSafeTestLabel";
            this.StartThreadSafeTestLabel.Size = new System.Drawing.Size(291, 13);
            this.StartThreadSafeTestLabel.TabIndex = 7;
            this.StartThreadSafeTestLabel.Text = "Click here to start updating these controls in the background";
            // 
            // ThreadSafeTestTextBox
            // 
            this.ThreadSafeTestTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThreadSafeTestTextBox.Location = new System.Drawing.Point(3, 16);
            this.ThreadSafeTestTextBox.Multiline = true;
            this.ThreadSafeTestTextBox.Name = "ThreadSafeTestTextBox";
            this.ThreadSafeTestTextBox.Size = new System.Drawing.Size(174, 108);
            this.ThreadSafeTestTextBox.TabIndex = 5;
            // 
            // StopThreadSafeTestButton
            // 
            this.StopThreadSafeTestButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.StopThreadSafeTestButton.Location = new System.Drawing.Point(497, 312);
            this.StopThreadSafeTestButton.Name = "StopThreadSafeTestButton";
            this.StopThreadSafeTestButton.Size = new System.Drawing.Size(75, 23);
            this.StopThreadSafeTestButton.TabIndex = 3;
            this.StopThreadSafeTestButton.Text = "Stop";
            this.StopThreadSafeTestButton.UseVisualStyleBackColor = true;
            this.StopThreadSafeTestButton.Click += new System.EventHandler(this.StopThreadSafeTestButton_Click);
            // 
            // StartThreadSafeTestButton
            // 
            this.StartThreadSafeTestButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.StartThreadSafeTestButton.Location = new System.Drawing.Point(497, 283);
            this.StartThreadSafeTestButton.Name = "StartThreadSafeTestButton";
            this.StartThreadSafeTestButton.Size = new System.Drawing.Size(75, 23);
            this.StartThreadSafeTestButton.TabIndex = 4;
            this.StartThreadSafeTestButton.Text = "Start";
            this.StartThreadSafeTestButton.UseVisualStyleBackColor = true;
            this.StartThreadSafeTestButton.Click += new System.EventHandler(this.StartThreadSafeTestButton_Click);
            // 
            // ThreadSafeTestWorker
            // 
            this.ThreadSafeTestWorker.WorkerSupportsCancellation = true;
            this.ThreadSafeTestWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ThreadSafeTestWorker_DoWork);
            // 
            // ThreadSafeTestBlinkingLabel
            // 
            this.ThreadSafeTestBlinkingLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ThreadSafeTestBlinkingLabel.AutoSize = true;
            this.ThreadSafeTestBlinkingLabel.Location = new System.Drawing.Point(12, 230);
            this.ThreadSafeTestBlinkingLabel.Name = "ThreadSafeTestBlinkingLabel";
            this.ThreadSafeTestBlinkingLabel.Size = new System.Drawing.Size(282, 13);
            this.ThreadSafeTestBlinkingLabel.TabIndex = 8;
            this.ThreadSafeTestBlinkingLabel.Text = "This label will alternate between being visible and invisible.";
            // 
            // ThreadSafeTestMovingLabel
            // 
            this.ThreadSafeTestMovingLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ThreadSafeTestMovingLabel.AutoSize = true;
            this.ThreadSafeTestMovingLabel.Location = new System.Drawing.Point(12, 259);
            this.ThreadSafeTestMovingLabel.Name = "ThreadSafeTestMovingLabel";
            this.ThreadSafeTestMovingLabel.Size = new System.Drawing.Size(178, 13);
            this.ThreadSafeTestMovingLabel.TabIndex = 8;
            this.ThreadSafeTestMovingLabel.Text = "This label will move around the form.";
            // 
            // ThreadSafeTestFontChangingLabel
            // 
            this.ThreadSafeTestFontChangingLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ThreadSafeTestFontChangingLabel.AutoSize = true;
            this.ThreadSafeTestFontChangingLabel.Location = new System.Drawing.Point(12, 201);
            this.ThreadSafeTestFontChangingLabel.Name = "ThreadSafeTestFontChangingLabel";
            this.ThreadSafeTestFontChangingLabel.Size = new System.Drawing.Size(182, 13);
            this.ThreadSafeTestFontChangingLabel.TabIndex = 8;
            this.ThreadSafeTestFontChangingLabel.Text = "This label will randomly change fonts.";
            // 
            // ThreadSafeTestTable
            // 
            this.ThreadSafeTestTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ThreadSafeTestTable.ColumnCount = 3;
            this.ThreadSafeTestTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ThreadSafeTestTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ThreadSafeTestTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ThreadSafeTestTable.Controls.Add(this.ThreadSafeTestTextBoxBox, 0, 0);
            this.ThreadSafeTestTable.Controls.Add(this.ThreadSafeTestListBoxBox, 1, 0);
            this.ThreadSafeTestTable.Controls.Add(this.ThreadSafeTestListViewBox, 2, 0);
            this.ThreadSafeTestTable.Controls.Add(this.ThreadSafeTestProgressBarBox, 0, 1);
            this.ThreadSafeTestTable.Controls.Add(this.ThreadSafeTestDateTimePickerBox, 1, 1);
            this.ThreadSafeTestTable.Location = new System.Drawing.Point(12, 12);
            this.ThreadSafeTestTable.Name = "ThreadSafeTestTable";
            this.ThreadSafeTestTable.RowCount = 2;
            this.ThreadSafeTestTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ThreadSafeTestTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.ThreadSafeTestTable.Size = new System.Drawing.Size(560, 178);
            this.ThreadSafeTestTable.TabIndex = 9;
            // 
            // ThreadSafeTestTextBoxBox
            // 
            this.ThreadSafeTestTextBoxBox.Controls.Add(this.ThreadSafeTestTextBox);
            this.ThreadSafeTestTextBoxBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThreadSafeTestTextBoxBox.Location = new System.Drawing.Point(3, 3);
            this.ThreadSafeTestTextBoxBox.Name = "ThreadSafeTestTextBoxBox";
            this.ThreadSafeTestTextBoxBox.Size = new System.Drawing.Size(180, 127);
            this.ThreadSafeTestTextBoxBox.TabIndex = 6;
            this.ThreadSafeTestTextBoxBox.TabStop = false;
            this.ThreadSafeTestTextBoxBox.Text = "Randomly changing textbox";
            // 
            // ThreadSafeTestListBoxBox
            // 
            this.ThreadSafeTestListBoxBox.Controls.Add(this.ThreadSafeTestListBox);
            this.ThreadSafeTestListBoxBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThreadSafeTestListBoxBox.Location = new System.Drawing.Point(189, 3);
            this.ThreadSafeTestListBoxBox.Name = "ThreadSafeTestListBoxBox";
            this.ThreadSafeTestListBoxBox.Size = new System.Drawing.Size(180, 127);
            this.ThreadSafeTestListBoxBox.TabIndex = 7;
            this.ThreadSafeTestListBoxBox.TabStop = false;
            this.ThreadSafeTestListBoxBox.Text = "Randomly changing listbox";
            // 
            // ThreadSafeTestListBox
            // 
            this.ThreadSafeTestListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThreadSafeTestListBox.FormattingEnabled = true;
            this.ThreadSafeTestListBox.Location = new System.Drawing.Point(3, 16);
            this.ThreadSafeTestListBox.Name = "ThreadSafeTestListBox";
            this.ThreadSafeTestListBox.Size = new System.Drawing.Size(174, 108);
            this.ThreadSafeTestListBox.TabIndex = 0;
            // 
            // ThreadSafeTestListViewBox
            // 
            this.ThreadSafeTestListViewBox.Controls.Add(this.ThreadSafeTestListView);
            this.ThreadSafeTestListViewBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThreadSafeTestListViewBox.Location = new System.Drawing.Point(375, 3);
            this.ThreadSafeTestListViewBox.Name = "ThreadSafeTestListViewBox";
            this.ThreadSafeTestListViewBox.Size = new System.Drawing.Size(182, 127);
            this.ThreadSafeTestListViewBox.TabIndex = 8;
            this.ThreadSafeTestListViewBox.TabStop = false;
            this.ThreadSafeTestListViewBox.Text = "Randomly changing listview";
            // 
            // ThreadSafeTestListView
            // 
            this.ThreadSafeTestListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThreadSafeTestListView.Location = new System.Drawing.Point(3, 16);
            this.ThreadSafeTestListView.Name = "ThreadSafeTestListView";
            this.ThreadSafeTestListView.Size = new System.Drawing.Size(176, 108);
            this.ThreadSafeTestListView.TabIndex = 0;
            this.ThreadSafeTestListView.UseCompatibleStateImageBehavior = false;
            this.ThreadSafeTestListView.View = System.Windows.Forms.View.Details;
            // 
            // ThreadSafeTestProgressBarBox
            // 
            this.ThreadSafeTestProgressBarBox.Controls.Add(this.ThreadSafeTestProgressBar);
            this.ThreadSafeTestProgressBarBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThreadSafeTestProgressBarBox.Location = new System.Drawing.Point(3, 136);
            this.ThreadSafeTestProgressBarBox.Name = "ThreadSafeTestProgressBarBox";
            this.ThreadSafeTestProgressBarBox.Size = new System.Drawing.Size(180, 39);
            this.ThreadSafeTestProgressBarBox.TabIndex = 11;
            this.ThreadSafeTestProgressBarBox.TabStop = false;
            this.ThreadSafeTestProgressBarBox.Text = "Randomly changing progressbar";
            // 
            // ThreadSafeTestProgressBar
            // 
            this.ThreadSafeTestProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThreadSafeTestProgressBar.Location = new System.Drawing.Point(3, 16);
            this.ThreadSafeTestProgressBar.Name = "ThreadSafeTestProgressBar";
            this.ThreadSafeTestProgressBar.Size = new System.Drawing.Size(174, 20);
            this.ThreadSafeTestProgressBar.TabIndex = 10;
            // 
            // ThreadSafeTestDateTimePickerBox
            // 
            this.ThreadSafeTestDateTimePickerBox.Controls.Add(this.ThreadSafeTestDateTimePicker);
            this.ThreadSafeTestDateTimePickerBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThreadSafeTestDateTimePickerBox.Location = new System.Drawing.Point(189, 136);
            this.ThreadSafeTestDateTimePickerBox.Name = "ThreadSafeTestDateTimePickerBox";
            this.ThreadSafeTestDateTimePickerBox.Size = new System.Drawing.Size(180, 39);
            this.ThreadSafeTestDateTimePickerBox.TabIndex = 12;
            this.ThreadSafeTestDateTimePickerBox.TabStop = false;
            this.ThreadSafeTestDateTimePickerBox.Text = "Randomly changing date/time";
            // 
            // ThreadSafeTestDateTimePicker
            // 
            this.ThreadSafeTestDateTimePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThreadSafeTestDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ThreadSafeTestDateTimePicker.Location = new System.Drawing.Point(3, 16);
            this.ThreadSafeTestDateTimePicker.Name = "ThreadSafeTestDateTimePicker";
            this.ThreadSafeTestDateTimePicker.Size = new System.Drawing.Size(174, 20);
            this.ThreadSafeTestDateTimePicker.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(497, 283);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.StartThreadSafeTestButton_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(497, 312);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Stop";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.StopThreadSafeTestButton_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(200, 288);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(291, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Click here to start updating these controls in the background";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(291, 317);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(200, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Click here to stop the background thread";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 230);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(282, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "This label will alternate between being visible and invisible.";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 201);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(182, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "This label will randomly change fonts.";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 259);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "This label will move around the form.";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(60, 94);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Randomly changing textbox";
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(3, 16);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(54, 75);
            this.textBox1.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listBox1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(69, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(60, 127);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Randomly changing listbox";
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(3, 16);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(54, 108);
            this.listBox1.TabIndex = 0;
            // 
            // ThreadSafeTestCheckBox
            // 
            this.ThreadSafeTestCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ThreadSafeTestCheckBox.AutoSize = true;
            this.ThreadSafeTestCheckBox.Location = new System.Drawing.Point(12, 287);
            this.ThreadSafeTestCheckBox.Name = "ThreadSafeTestCheckBox";
            this.ThreadSafeTestCheckBox.Size = new System.Drawing.Size(170, 17);
            this.ThreadSafeTestCheckBox.TabIndex = 10;
            this.ThreadSafeTestCheckBox.Text = "Randomly changing checkbox";
            this.ThreadSafeTestCheckBox.UseVisualStyleBackColor = true;
            // 
            // ThreadSafeTestRadioButton
            // 
            this.ThreadSafeTestRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ThreadSafeTestRadioButton.AutoSize = true;
            this.ThreadSafeTestRadioButton.Location = new System.Drawing.Point(12, 315);
            this.ThreadSafeTestRadioButton.Name = "ThreadSafeTestRadioButton";
            this.ThreadSafeTestRadioButton.Size = new System.Drawing.Size(175, 17);
            this.ThreadSafeTestRadioButton.TabIndex = 11;
            this.ThreadSafeTestRadioButton.TabStop = true;
            this.ThreadSafeTestRadioButton.Text = "Randomly changing radiobutton";
            this.ThreadSafeTestRadioButton.UseVisualStyleBackColor = true;
            // 
            // ThreadSafeControlsTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 347);
            this.Controls.Add(this.ThreadSafeTestRadioButton);
            this.Controls.Add(this.ThreadSafeTestCheckBox);
            this.Controls.Add(this.ThreadSafeTestTable);
            this.Controls.Add(this.ThreadSafeTestMovingLabel);
            this.Controls.Add(this.ThreadSafeTestFontChangingLabel);
            this.Controls.Add(this.ThreadSafeTestBlinkingLabel);
            this.Controls.Add(this.StopThreadSafeTestLabel);
            this.Controls.Add(this.StartThreadSafeTestLabel);
            this.Controls.Add(this.StopThreadSafeTestButton);
            this.Controls.Add(this.StartThreadSafeTestButton);
            this.MinimumSize = new System.Drawing.Size(600, 385);
            this.Name = "ThreadSafeControlsTestForm";
            this.ShowIcon = false;
            this.Text = "ThreadSafeControls Test";
            this.ThreadSafeTestTable.ResumeLayout(false);
            this.ThreadSafeTestTextBoxBox.ResumeLayout(false);
            this.ThreadSafeTestTextBoxBox.PerformLayout();
            this.ThreadSafeTestListBoxBox.ResumeLayout(false);
            this.ThreadSafeTestListViewBox.ResumeLayout(false);
            this.ThreadSafeTestProgressBarBox.ResumeLayout(false);
            this.ThreadSafeTestDateTimePickerBox.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label StopThreadSafeTestLabel;
        private System.Windows.Forms.Label StartThreadSafeTestLabel;
        private System.Windows.Forms.TextBox ThreadSafeTestTextBox;
        private System.Windows.Forms.Button StopThreadSafeTestButton;
        private System.Windows.Forms.Button StartThreadSafeTestButton;
        private System.ComponentModel.BackgroundWorker ThreadSafeTestWorker;
        private System.Windows.Forms.Label ThreadSafeTestBlinkingLabel;
        private System.Windows.Forms.Label ThreadSafeTestMovingLabel;
        private System.Windows.Forms.Label ThreadSafeTestFontChangingLabel;
        private System.Windows.Forms.TableLayoutPanel ThreadSafeTestTable;
        private System.Windows.Forms.GroupBox ThreadSafeTestTextBoxBox;
        private System.Windows.Forms.GroupBox ThreadSafeTestListBoxBox;
        private System.Windows.Forms.ListBox ThreadSafeTestListBox;
        private System.Windows.Forms.GroupBox ThreadSafeTestListViewBox;
        private System.Windows.Forms.ListView ThreadSafeTestListView;
        private System.Windows.Forms.GroupBox ThreadSafeTestProgressBarBox;
        private System.Windows.Forms.ProgressBar ThreadSafeTestProgressBar;
        private System.Windows.Forms.GroupBox ThreadSafeTestDateTimePickerBox;
        private System.Windows.Forms.DateTimePicker ThreadSafeTestDateTimePicker;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.CheckBox ThreadSafeTestCheckBox;
        private System.Windows.Forms.RadioButton ThreadSafeTestRadioButton;
    }
}

