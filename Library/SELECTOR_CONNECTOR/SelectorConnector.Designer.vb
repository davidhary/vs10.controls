<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelectorConnector
    Inherits UserControlBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SelectorConnector))
        Me._toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._namesComboBox = New System.Windows.Forms.ComboBox
        Me._findButton = New System.Windows.Forms.Button
        Me._imageList = New System.Windows.Forms.ImageList(Me.components)
        Me._connectToggleButton = New System.Windows.Forms.CheckBox
        Me._clearButton = New System.Windows.Forms.Button
        Me._mainTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel
        Me._selectorTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel
        Me._mainTableLayoutPanel.SuspendLayout()
        Me._selectorTableLayoutPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_namesComboBox
        '
        Me._namesComboBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._namesComboBox.Location = New System.Drawing.Point(0, 3)
        Me._namesComboBox.Margin = New System.Windows.Forms.Padding(0)
        Me._namesComboBox.Name = "_namesComboBox"
        Me._namesComboBox.Size = New System.Drawing.Size(201, 21)
        Me._namesComboBox.TabIndex = 5
        Me._toolTip.SetToolTip(Me._namesComboBox, "Select a Names Item")
        '
        '_findButton
        '
        Me._findButton.ImageIndex = 3
        Me._findButton.ImageList = Me._imageList
        Me._findButton.Location = New System.Drawing.Point(241, 5)
        Me._findButton.Name = "_findButton"
        Me._findButton.Size = New System.Drawing.Size(25, 26)
        Me._findButton.TabIndex = 6
        Me._toolTip.SetToolTip(Me._findButton, "Find Named Items")
        Me._findButton.UseVisualStyleBackColor = True
        '
        '_imageList
        '
        Me._imageList.ImageStream = CType(resources.GetObject("_imageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me._imageList.TransparentColor = System.Drawing.SystemColors.Control
        Me._imageList.Images.SetKeyName(0, "")
        Me._imageList.Images.SetKeyName(1, "")
        Me._imageList.Images.SetKeyName(2, "")
        Me._imageList.Images.SetKeyName(3, "")
        '
        '_connectToggleButton
        '
        Me._connectToggleButton.Appearance = System.Windows.Forms.Appearance.Button
        Me._connectToggleButton.Enabled = False
        Me._connectToggleButton.ImageIndex = 1
        Me._connectToggleButton.ImageList = Me._imageList
        Me._connectToggleButton.Location = New System.Drawing.Point(272, 5)
        Me._connectToggleButton.Name = "_connectToggleButton"
        Me._connectToggleButton.Size = New System.Drawing.Size(25, 26)
        Me._connectToggleButton.TabIndex = 7
        Me._toolTip.SetToolTip(Me._connectToggleButton, "Depress to connect or release to disconnect")
        Me._connectToggleButton.UseVisualStyleBackColor = True
        '
        '_clearButton
        '
        Me._clearButton.Enabled = False
        Me._clearButton.ImageIndex = 0
        Me._clearButton.ImageList = Me._imageList
        Me._clearButton.Location = New System.Drawing.Point(3, 5)
        Me._clearButton.Name = "_clearButton"
        Me._clearButton.Size = New System.Drawing.Size(25, 26)
        Me._clearButton.TabIndex = 8
        Me._toolTip.SetToolTip(Me._clearButton, "Clear Selected Named Item")
        Me._clearButton.UseVisualStyleBackColor = True
        '
        '_mainTableLayoutPanel
        '
        Me._mainTableLayoutPanel.ColumnCount = 4
        Me._mainTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._mainTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._mainTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._mainTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._mainTableLayoutPanel.Controls.Add(Me._selectorTableLayoutPanel, 1, 1)
        Me._mainTableLayoutPanel.Controls.Add(Me._clearButton, 0, 1)
        Me._mainTableLayoutPanel.Controls.Add(Me._connectToggleButton, 3, 1)
        Me._mainTableLayoutPanel.Controls.Add(Me._findButton, 2, 1)
        Me._mainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._mainTableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me._mainTableLayoutPanel.Margin = New System.Windows.Forms.Padding(0)
        Me._mainTableLayoutPanel.Name = "_mainTableLayoutPanel"
        Me._mainTableLayoutPanel.RowCount = 3
        Me._mainTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._mainTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me._mainTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._mainTableLayoutPanel.Size = New System.Drawing.Size(300, 37)
        Me._mainTableLayoutPanel.TabIndex = 10
        '
        '_selectorTableLayoutPanel
        '
        Me._selectorTableLayoutPanel.ColumnCount = 1
        Me._selectorTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._selectorTableLayoutPanel.Controls.Add(Me._namesComboBox, 0, 1)
        Me._selectorTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._selectorTableLayoutPanel.Location = New System.Drawing.Point(34, 5)
        Me._selectorTableLayoutPanel.Name = "_selectorTableLayoutPanel"
        Me._selectorTableLayoutPanel.RowCount = 3
        Me._selectorTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._selectorTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._selectorTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2.0!))
        Me._selectorTableLayoutPanel.Size = New System.Drawing.Size(201, 26)
        Me._selectorTableLayoutPanel.TabIndex = 11
        '
        'SelectorConnector
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me._mainTableLayoutPanel)
        Me.Name = "SelectorConnector"
        Me.Size = New System.Drawing.Size(300, 37)
        Me._mainTableLayoutPanel.ResumeLayout(False)
        Me._selectorTableLayoutPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _toolTip As System.Windows.Forms.ToolTip
    Private WithEvents _imageList As System.Windows.Forms.ImageList
    Private WithEvents _namesComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _findButton As System.Windows.Forms.Button
    Private WithEvents _connectToggleButton As System.Windows.Forms.CheckBox
    Private WithEvents _clearButton As System.Windows.Forms.Button
    Friend WithEvents _mainTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents _selectorTableLayoutPanel As System.Windows.Forms.TableLayoutPanel

End Class
